/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.reader.*;
import com.cncsoftwaretools.autoleveller.reader.processor.GCodeInterpreter;
import com.cncsoftwaretools.autoleveller.reader.processor.LevelerProcessor;
import com.cncsoftwaretools.autoleveller.reader.processor.ReplacerPreProcessor;
import com.cncsoftwaretools.autoleveller.util.Arc;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import javafx.scene.shape.Rectangle;

import javax.annotation.Nonnull;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Holds information about the gcode design to be levelled, i.e the original file created by uor CAM program
 * Created by James Hawthorne on 09/07/2015.
 */
public class OGF
{
    private final String filename;
    private final Rectangle size;
    private final Units units;

    private OGF(String filename, Rectangle size, @Nonnull Units units)
    {
        this.filename = filename;
        this.size = size;
        this.units = units;
    }

    public static Optional<OGF> newInstance(String filename)
    {
        try {
            GCodeReader reader = PlainGCodeReader.newInstance(new FileReader(filename));
            GCodeFileCollector gCodeFileCollector = new GCodeFileCollector();
            reader.readFile(gCodeFileCollector);

            Rectangle size = gCodeFileCollector.getSize();
            Units units = gCodeFileCollector.getUnits();

            return Optional.of(new OGF(filename, size, units));
        }
        catch (Exception e){return Optional.empty();}
    }

    public Rectangle getSize()
    {
        return size;
    }

    public String getFilename()
    {
        return filename;
    }

    public Units getUnits()
    {
        return units;
    }

    @Override
    public String toString()
    {
        return "OGF{ size=" + size + '}';
    }

    /**
     * A minimal set of details. A custom version of toString()
     *
     * @return a String
     */
    public String displayDetails()
    {
        DecimalFormat df = Autoleveller.DF;

        return "Units=" + units + " X=" + df.format(size.getX()) + " Y=" + df.format(size.getY()) +
        		" Width=" + df.format(size.getWidth()) + " Height=" + df.format(size.getHeight());
    }

    /**
     * The OGF class uses this private class to collect information about the OGF such as size and bounds
     */
    private static class GCodeFileCollector extends GCodeInterpreter
    {
        private Rectangle size;
        private Units units = Units.MM;

        private BigDecimal largestX = null;
        private BigDecimal smallestX = null;
        private BigDecimal largestY = null;
        private BigDecimal smallestY = null;
        private GCodeState latestState = null;

        @Override
        public List<GCodeState> processState(GCodeState currentState)
        {
            latestState = currentState;

            Optional<Arc> arcOpt = latestState.getCurrentWords().contains(Word.createPair('G', new BigDecimal("2"))) ||
                    latestState.getCurrentWords().contains(Word.createPair('G', new BigDecimal("3"))) ?
                    Arc.createArcFromState(latestState) : Optional.empty();
            Optional<BigDecimal> z = latestState.getModalWordByGroup(WordGroup.ZAXISGROUP)
                    .map(word -> word.getWordPair().getSecond());

            if (z.isPresent() && z.get().compareTo(LevelerProcessor.SURFACEHEIGHT) == -1) {
                if (arcOpt.isPresent()) {
                    setLargestSmallest(arcOpt.orElseGet(() -> Arc.newInstance(Point2D.ZERO, Point2D.ZERO, Point2D.ZERO, Arc.Direction.CW)).getExtremities());
                } else {
                    setLargestSmallest(convertStateToMinMaxMap(latestState));
                }
            }

            return ImmutableList.of();
        }

        private Map<Arc.Coords, BigDecimal> convertStateToMinMaxMap(GCodeState state)
        {
            Map<Arc.Coords, BigDecimal> minMaxMap = new HashMap<>();
            Optional<BigDecimal> x = state.getModalWordByGroup(WordGroup.XAXISGROUP)
                    .map(word -> word.getWordPair().getSecond());
            Optional<BigDecimal> y = state.getModalWordByGroup(WordGroup.YAXISGROUP)
                    .map(word -> word.getWordPair().getSecond());

            if (x.isPresent()){
                minMaxMap.put(Arc.Coords.MAXX, x.get());
                minMaxMap.put(Arc.Coords.MINX, x.get());
            }

            if (y.isPresent()){
                minMaxMap.put(Arc.Coords.MAXY, y.get());
                minMaxMap.put(Arc.Coords.MINY, y.get());
            }

            return minMaxMap;
        }

        private void setLargestSmallest(Map<Arc.Coords, BigDecimal> extremities)
        {
            BigDecimal maxX = extremities.get(Arc.Coords.MAXX);
            BigDecimal minX = extremities.get(Arc.Coords.MINX);
            BigDecimal maxY = extremities.get(Arc.Coords.MAXY);
            BigDecimal minY = extremities.get(Arc.Coords.MINY);

            largestX = (largestX == null || maxX.compareTo(largestX) == 1) ? maxX : largestX;
            smallestX = (smallestX == null || minX.compareTo(smallestX) == -1) ? minX : smallestX;
            largestY = (largestY == null || maxY.compareTo(largestY) == 1) ? maxY : largestY;
            smallestY = (smallestY == null || minY.compareTo(smallestY) == -1) ? minY : smallestY;
        }

        public void finish()
        {
            if (smallestX != null &&
                    smallestY != null &&
                    largestX != null &&
                    largestY != null) {

                size = new Rectangle(smallestX.doubleValue(), smallestY.doubleValue(),
                        largestX.subtract(smallestX).doubleValue(), largestY.subtract(smallestY).doubleValue());
            }
            else {size = new Rectangle();}

            latestState.getModalWordByGroup(WordGroup.UNITSGROUP)
                    .ifPresent(word -> units = word.asString().equals("G20") ? Units.INCHES : Units.MM);
        }

        public Rectangle getSize(){return size;}

        public Units getUnits(){return units;}
    }
}

