/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.util;

import com.cncsoftwaretools.autoleveller.ALTextBlockFunctions;
import com.cncsoftwaretools.autoleveller.Controller;
import com.cncsoftwaretools.autoleveller.Pair;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.shape.Rectangle;
import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.cncsoftwaretools.autoleveller.Autoleveller.getLogger;
import static com.google.common.collect.ImmutableList.of;
import static java.util.stream.Collectors.toList;

/**
 * Util class to contain static functions for common Autolevelling tasks.
 * Created by James Hawthorne on 12/05/2015.
 */
public class AutolevellerUtil
{
    private static final Logger LOGGER = getLogger(AutolevellerUtil.class.getName()).orElse(Logger.getAnonymousLogger());

    private AutolevellerUtil(){}

    /**
     * Returns a filename given the original filename and then replaces the existing extension with the extension
     * most appropriate as defined in the cnc {@link Controller} given
     * @param originalName The OGF filename
     * @param controller The {@link Controller}
     * @return A String representing the new output filename
     */
    public static String generateOutputFilename(String originalName, Controller controller)
    {
        String nameWithoutExt = FilenameUtils.removeExtension(originalName);

        return "AL".concat(nameWithoutExt).concat(".").concat(controller.getDefaultExt());
    }

    /**
     * Gets the scaled centre point of the node
     * @param unscaledNode The unscaled Node to find the centre point of after scaling
     * @param scaledPercent The percentage value to use to scale the node
     * @return The scaled centre point of the input node
     */
    public static Point2D getScaledClickPointForNode(Node unscaledNode, double scaledPercent)
    {
        double scaledValue = scaledPercent / 100;
        Point2D topLeftPoint = unscaledNode.localToScreen(0, 0);
        Point2D scaledPos = getScaledPoint(topLeftPoint, scaledPercent);
        Rectangle scaledNode = new Rectangle(scaledPos.getX(), scaledPos.getY(),
                unscaledNode.getBoundsInLocal().getWidth() * scaledValue,
                unscaledNode.getBoundsInLocal().getHeight() * scaledValue);

        return new Point2D(scaledNode.getX() + scaledNode.getWidth() / 2, scaledNode.getY() + scaledNode.getHeight() / 2);
    }

    /**
     * Scales the input point according to the screen scaling
     * @param unscaledPoint The point unscaled or at 100% scaling
     * @param scaledPercent The pecentage used to scale the point
     * @return A Point2D after scaling has been applied
     */
    public static Point2D getScaledPoint(Point2D unscaledPoint, double scaledPercent)
    {
        double scaledValue = scaledPercent / 100;
        return new Point2D(unscaledPoint.getX() * scaledValue, unscaledPoint.getY() * scaledValue);
    }

    public static double normalize(double value, double baseMin, double baseMax)
    {
        double scaledVal = 1 * (value - baseMin) / (baseMax - baseMin);
        return Math.max(0, Math.min(scaledVal, 1)); //Clamp value between 0 and 1
    }

    /**
     * <br>Gets an interpolated value based on the two input pairs of numbers
     * <br>Usage: {@code linearInterpolate(pair1, pair2).apply(new BigDecimal("38"));}
     * <br>Interpolation formula used:
     * <br>      XorY - XorY
     * z = z1 + ------------- (z2 - z1)
     *          XorY2 - XorY1
     * @param pair1 First number and its value
     * @param pair2 First number and its value
     * @return The Pair consisting of the original number and its interpolated value
     */
    public static Function<BigDecimal, Pair<BigDecimal, BigDecimal>> linearInterpolate(
            Pair<BigDecimal, BigDecimal> pair1, Pair<BigDecimal, BigDecimal> pair2)
    {
        return number -> {
            BigDecimal nominator = number.subtract(pair1.getFirst());
            BigDecimal denominator = pair2.getFirst().subtract(pair1.getFirst());
            //avoid dividing by zero
            BigDecimal mult = denominator.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO :
                    nominator.divide(denominator, ALTextBlockFunctions.SCALE, BigDecimal.ROUND_UP);
            BigDecimal term = mult.multiply(pair2.getSecond().subtract(pair1.getSecond()));
            BigDecimal intValue = pair1.getSecond().add(term);

            return Pair.createPair(number, intValue);
        };
    }

    /**
     * <br>Given a List of surrounding 3D points get the Point3D for the applied Point2D
     * <br>where the returned Z point is interpolated
     * <br>Usage: {@code biLinearInterpolate(surroundingPoints).apply(point2D);}
     * @param surroundingPoints <br>A List of Point3D object where the order is important
     *                          <br>index values: 0 = Top Left, 1 = Top Right, 2 = Bottom Left, 3 = Bottom Right
     * @return The Point3D where Z has been interpolated
     */
    public static Function<Point2D, Point3D> biLinearInterpolate(List<Point3D> surroundingPoints)
    {
        return point2D -> {
            Pair<BigDecimal, BigDecimal> topLeftY = Pair.createPair(new BigDecimal(surroundingPoints.get(0).getY()),
                    new BigDecimal(surroundingPoints.get(0).getZ()));
            Pair<BigDecimal, BigDecimal> bottomLeftY = Pair.createPair(new BigDecimal(surroundingPoints.get(2).getY()),
                    new BigDecimal(surroundingPoints.get(2).getZ()));
            Pair<BigDecimal, BigDecimal> topRightY = Pair.createPair(new BigDecimal(surroundingPoints.get(1).getY()),
                    new BigDecimal(surroundingPoints.get(1).getZ()));
            Pair<BigDecimal, BigDecimal> bottomRightY = Pair.createPair(new BigDecimal(surroundingPoints.get(3).getY()),
                    new BigDecimal(surroundingPoints.get(3).getZ()));

            Pair<BigDecimal, BigDecimal> leftY = linearInterpolate(topLeftY, bottomLeftY).apply(new BigDecimal(point2D.getY()));
            Pair<BigDecimal, BigDecimal> rightY = linearInterpolate(topRightY, bottomRightY).apply(new BigDecimal(point2D.getY()));

            Pair<BigDecimal, BigDecimal> leftX = Pair.createPair(new BigDecimal(surroundingPoints.get(0).getX()), leftY.getSecond());
            Pair<BigDecimal, BigDecimal> rightX = Pair.createPair(new BigDecimal(surroundingPoints.get(1).getX()), rightY.getSecond());

            Pair<BigDecimal, BigDecimal> value = linearInterpolate(leftX, rightX).apply(new BigDecimal(point2D.getX()));

            return new Point3D(point2D.getX(), point2D.getY(), value.getSecond().setScale(ALTextBlockFunctions.SCALE, BigDecimal.ROUND_HALF_UP).doubleValue());
        };
    }

    public static <T> List<List<T>> partitionList(List<T> fullList, Predicate<T> pred)
    {
        List<Integer> indexes = IntStream.range(0, fullList.size())
                .filter(i -> pred.test(fullList.get(i)))
                .boxed()
                .collect(Collectors.toList());
        //add -1 to the front of the list so that the first partition starts from index 0
        indexes.add(0, -1);
        //add the last index of full list to index list if not already present so that we capture the entire list
        if (indexes.get(indexes.size()-1) != fullList.size()-1){indexes.add(fullList.size()-1);}

        return IntStream.range(0, indexes.size() - 1)
                .mapToObj(i -> fullList.subList(indexes.get(i) + 1, indexes.get(i + 1) + 1))
                .collect(toList());
    }

    public static List<String> getLinesMatchingRegex(List<String> strings, String regexStr)
    {
        return strings.stream()
                .filter(string -> string.matches(regexStr))
                .collect(toList());
    }

    public static List<String> getFileLinesFromTo(String filename, String from, String to)
    {
        try (Stream<String> lineStream = Files.lines(Paths.get(filename))) {
            List<String> allLines = lineStream.collect(toList());
            int fromIndex = allLines.indexOf(from);
            int toIndex = allLines.indexOf(to);

            return allLines.subList(fromIndex, toIndex);
        }catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error reading file: " + filename);
            throw new RuntimeException(e);
        }
    }
}
