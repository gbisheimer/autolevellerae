/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/

package com.cncsoftwaretools.autoleveller.util;

import com.cncsoftwaretools.autoleveller.Pair;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.reader.Word;
import com.cncsoftwaretools.autoleveller.reader.WordGroup;
import com.cncsoftwaretools.autoleveller.reader.processor.LevelerProcessor;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import javafx.geometry.Point2D;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A partial circle is an Arc. The curved line between 2 points.
 */
public class Arc
{
    private final Point2D startPoint;
    private final Point2D endPoint;
    private final Point2D centrePoint;
    private final Direction direction;
    private final BigDecimal radius;
    private final BigDecimal circleCircumference;
    private static final double fullCircle = 2 * Math.PI; //360 deg. = 2*PI rad.

    private Arc(Point2D startPoint, Point2D endPoint, Point2D centrePoint, Direction direction)
    {
        this.startPoint = roundPoint2D(startPoint);
        this.endPoint = roundPoint2D(endPoint);
        this.centrePoint = roundPoint2D(centrePoint);
        this.direction = direction;
        this.radius = new BigDecimal(startPoint.distance(centrePoint));
        this.circleCircumference = new BigDecimal(fullCircle * radius.doubleValue());
    }

    /**
     * Creates a representation of an arc, i.e a segment of a circle
     * @param startPoint The starting point of the Arc. This {@code Point2D} should be on the circumference of the circle
     * @param endPoint The ending point of the Arc. This {@code Point2D} should be on the circumference of the circle
     * @param centrePoint The centre point of the Arc. This {@code Point2D} should be at the centre of the arc/circle
     * and is the point where the start and end points diverge
     * @param direction Determines if the start point meets the end point in a clockwise or counter-clockwise direction
     * @return An Arc instance
     */
    public static Arc newInstance(Point2D startPoint, Point2D endPoint, Point2D centrePoint, Direction direction)
    {
        return new Arc(startPoint, endPoint, centrePoint, direction);
    }

    public static Optional<Arc> createArcFromState(GCodeState state)
    {
        if (!isValidArcState(state)){return Optional.empty();}

        Arc.Direction direction = state.getModalWords().contains(Word.createPair('G', new BigDecimal(2))) ?
                Arc.Direction.CW : Arc.Direction.CCW;
        Point2D startPoint = state.getPreviousState().get().get2DPoint().get();
        Point2D endPoint = state.get2DPoint().get();
        Point2D centrePoint = state.wordBeginningOnCurrentLine('R').isPresent() ?
                getCentreFromRadius(startPoint, endPoint, direction, state.wordValueOnCurrentLineGetter()).get() :
                getCentreFromOffsets(state, startPoint, state.wordValueOnCurrentLineGetter()).get();

        return Optional.of(Arc.newInstance(startPoint, endPoint, centrePoint, direction));
    }

    @SuppressWarnings("RedundantIfStatement")
    public static boolean isValidArcState(GCodeState state)
    {
        Optional<GCodeState> previousState = state.getPreviousState();
        Optional<Word> zDepth = state.getModalWordByGroup(WordGroup.ZAXISGROUP);

        if (!previousState.isPresent() || !previousState.get().get2DPoint().isPresent()){return false;}
        if ((!state.wordBeginningOnCurrentLine('I').isPresent() && !state.wordBeginningOnCurrentLine('J').isPresent()) &&
                !state.wordBeginningOnCurrentLine('R').isPresent()) {return false;}
        if (!state.getModalWordByGroup(WordGroup.FEEDGROUP).isPresent()){return false;}
        if (!zDepth.isPresent() || zDepth.get().getWordPair().getSecond().doubleValue() > LevelerProcessor.SURFACEHEIGHT.doubleValue()){return false;}

        return true;
    }

    public static Optional<Point2D> getCentreFromRadius(Point2D startPoint, Point2D endPoint, Arc.Direction direction,
                                                  Function<Character, Optional<BigDecimal>> wordValueGetter)
    {
        Optional<BigDecimal> radiusOpt = wordValueGetter.apply('R');
        if (!radiusOpt.isPresent()){return Optional.empty();}

        double radius = radiusOpt.get().doubleValue();
        Pair<Point2D, Point2D> centres = Arc.getCentreFromRadius(startPoint, endPoint, radius);

        Arc arcMadeWithCenre1 = Arc.newInstance(startPoint, endPoint, centres.getFirst(), direction);
        BigDecimal angleWithCentre1 = arcMadeWithCenre1.angleBetweenStartAndGivenPoint(endPoint);

        if (radius < 0){ //Pi = 180deg. in radians
            return Optional.of(angleWithCentre1.doubleValue() < Math.PI ? centres.getSecond() : centres.getFirst());
        }
        else{
            return Optional.of(angleWithCentre1.doubleValue() < Math.PI ? centres.getFirst() : centres.getSecond());
        }
    }

    public static Optional<Point2D> getCentreFromOffsets(GCodeState state, Point2D startPoint, Function<Character, Optional<BigDecimal>> wordValueGetter)
    {
        Optional<Word> absArcWordOpt = state.getModalWordByGroup(WordGroup.ARCMODE);
        double iValue = wordValueGetter.apply('I').orElse(BigDecimal.ZERO).doubleValue();
        double jValue = wordValueGetter.apply('J').orElse(BigDecimal.ZERO).doubleValue();

        return absArcWordOpt.isPresent() && absArcWordOpt.get().getWordPair().getSecond().equals(BigDecimal.valueOf(90.1)) ?
                Optional.of(new Point2D(iValue, jValue)) :
                Optional.of(new Point2D(startPoint.getX() + iValue, startPoint.getY() + jValue));
    }

    private Point2D roundPoint2D(Point2D pointToRound)
    {
        BigDecimal roundedPointX = new BigDecimal(pointToRound.getX());
        BigDecimal roundedPointY =  new BigDecimal(pointToRound.getY());
        roundedPointX = roundedPointX.setScale(6, BigDecimal.ROUND_HALF_UP);
        roundedPointY = roundedPointY.setScale(6, BigDecimal.ROUND_HALF_UP);

        return new Point2D(roundedPointX.doubleValue(), roundedPointY.doubleValue());
    }

    public Point2D getStartPoint(){return startPoint;}

    public Point2D getEndPoint(){return endPoint;}

    public Point2D getCentrePoint(){return centrePoint;}

    public Direction getDirection(){return direction;}

    public BigDecimal getRadius(){return radius;}

    public BigDecimal getCircleCircumference(){return circleCircumference;}

    public Map<Coords, BigDecimal> getExtremities()
    {
        List<Point2D> allContributingPoints = getSignificantPointsOnArcCircumference();
        Supplier<Stream<Double>> allContributingXsSup = () -> allContributingPoints.stream().map(Point2D::getX);
        Supplier<Stream<Double>> allContributingYsSup = () -> allContributingPoints.stream().map(Point2D::getY);

        return ImmutableMap.of(
                Coords.MAXX, new BigDecimal(allContributingXsSup.get().max(Double::compareTo).orElse(endPoint.getX())),
                Coords.MINX, new BigDecimal(allContributingXsSup.get().min(Double::compareTo).orElse(endPoint.getX())),
                Coords.MAXY, new BigDecimal(allContributingYsSup.get().max(Double::compareTo).orElse(endPoint.getY())),
                Coords.MINY, new BigDecimal(allContributingYsSup.get().min(Double::compareTo).orElse(endPoint.getY())));
    }

    /**
     * A list of points on the circumference of the Arc, which includes...
     * the start and end points plus all the limit points which the arc circumference passed through
     */
    private List<Point2D> getSignificantPointsOnArcCircumference()
    {
        BigDecimal angularDistanceToEndPoint = angleBetweenStartAndGivenPoint(endPoint);
        Map<Point2D, BigDecimal> circleLimitDistances = getAngularDistanceToLimitPointsFromStart();
        ImmutableList.Builder<Point2D> allContributingPointsBld = ImmutableList.builder();
        allContributingPointsBld.add(startPoint);
        allContributingPointsBld.add(endPoint);
        allContributingPointsBld.addAll(circleLimitDistances.keySet().stream()
                .filter(limitPoint -> angularDistanceToEndPoint.compareTo(circleLimitDistances.get(limitPoint)) == 1)
                .collect(Collectors.toList()));

        return allContributingPointsBld.build();
    }

    /**
     * The angle from start to given point in radians
     */
    public BigDecimal angleBetweenStartAndGivenPoint(Point2D point)
    {
        BigDecimal startPointAngle = angleAtPoint(startPoint, centrePoint);
        BigDecimal endPointAngle = angleAtPoint(point, centrePoint);
        BigDecimal diff = direction.equals(Direction.CW) ?
                startPointAngle.subtract(endPointAngle) : endPointAngle.subtract(startPointAngle);

        //if diff is negative then we are going in the opposite direction to the stated direction
        //if this is the case return the opposing angle
        return new BigDecimal(diff.doubleValue() < 0 ? fullCircle + diff.doubleValue() : diff.doubleValue());
    }

    @VisibleForTesting
    Map<Point2D, BigDecimal> getAngularDistanceToLimitPointsFromStart()
    {
        List<Point2D> circleLimits = getCircleLimits();

        return ImmutableMap.of(
                circleLimits.get(0), angleBetweenStartAndGivenPoint(circleLimits.get(0)),
                circleLimits.get(1), angleBetweenStartAndGivenPoint(circleLimits.get(1)),
                circleLimits.get(2), angleBetweenStartAndGivenPoint(circleLimits.get(2)),
                circleLimits.get(3), angleBetweenStartAndGivenPoint(circleLimits.get(3)));
    }

    private List<Point2D> getCircleLimits()
    {
        return ImmutableList.of(
                new Point2D(centrePoint.getX(), centrePoint.getY() + radius.doubleValue()),
                new Point2D(centrePoint.getX() - radius.doubleValue(), centrePoint.getY()),
                new Point2D(centrePoint.getX(), centrePoint.getY() - radius.doubleValue()),
                new Point2D(centrePoint.getX() + radius.doubleValue(), centrePoint.getY()));
    }

    /**
     * Gets the angle from the vector pointing directly east from the centre point to the vector
     * represented by {@code point} in a counter-clockwise direction
     * e.g. if {@code point} were pointing directly north from the centre, the angle would be 90 deg.
     */
    public static BigDecimal angleAtPoint(Point2D point, Point2D centre)
    {
        double adjacent = point.getX() - centre.getX();
        double opposite = point.getY() - centre.getY();

        double angle = Math.atan2(opposite, adjacent);

        //if angle is negative return full circle + the negative angle
        return new BigDecimal(angle < 0 ? fullCircle + angle : angle);
    }

    /**
     * Gets the length of this arc, i.e the arc length between start and end points, not just the direct distance
     */
	public BigDecimal arcLength()
	{
		BigDecimal angle = angleBetweenStartAndGivenPoint(endPoint);
		BigDecimal proportionOfCircumference = angle.divide(new BigDecimal(fullCircle), BigDecimal.ROUND_HALF_UP);
		
		return proportionOfCircumference.multiply(circleCircumference);
	}

    /**
     * Returns the angle at the centre point made between the given point on the circumference and desired arc length.
     * @param arcLength The desired segment length
     */
    @VisibleForTesting
    static BigDecimal angleForArcLength(Point2D startPoint, Point2D centrePoint, double arcLength, Direction direction)
    {
        double absArcLength = Math.abs(arcLength);
        double radius = startPoint.distance(centrePoint);
        BigDecimal startPointAngle = angleAtPoint(startPoint, centrePoint);
        BigDecimal angleChange = new BigDecimal((absArcLength / (2 * Math.PI * radius)) * fullCircle);
        if(direction.equals(Direction.CCW)){angleChange = angleChange.negate();}
        BigDecimal endPointAngle = startPointAngle.subtract(angleChange);

        //make sure the range is 0 - 360deg
        return endPointAngle.compareTo(BigDecimal.ZERO) < 0 ? new BigDecimal(fullCircle).add(endPointAngle) :
                endPointAngle.compareTo(BigDecimal.ZERO) >= fullCircle ? new BigDecimal(fullCircle).subtract(endPointAngle) :
                        endPointAngle;
    }
	
    /**
     * Returns the point at the given startPoint + desired segment length of the arc created on the circumference.
     * NOTE: If segment length is negative the returned point will be counter-clockwise from the startPoint
     * @param arcLength The desired arc length
     * @param direction The direction from the start point
     * @return the end point
     */
    public static Point2D endPointForArcLength(Point2D startPoint, Point2D centrePoint, double arcLength, Direction direction)
    {
        double radius = startPoint.distance(centrePoint);
        BigDecimal angleForNewSegment = angleForArcLength(startPoint, centrePoint, arcLength, direction);

        double opposite = Math.sin(angleForNewSegment.doubleValue()) * radius;
        double adjacent = Math.cos(angleForNewSegment.doubleValue()) * radius;

        return new Point2D(centrePoint.getX() + adjacent, centrePoint.getY() + opposite);
	}
    
	/**
	 * Returns the 2 candidates for the centre point of an arc given the radius
     * @param startPoint The starting point of the arc
     * @param endPoint The ending point of the arc
	 * @param radius A known radius
	 * @return A Pair instance containing two {@code Point2D} objects that represent the 2 possible centre points
	 */
	public static Pair<Point2D, Point2D> getCentreFromRadius(Point2D startPoint, Point2D endPoint, double radius)
	{
		double chordDistance = startPoint.distance(endPoint);
		Point2D chordCentre = startPoint.midpoint(endPoint);
		double distance = Math.sqrt(Math.pow(radius, 2) - Math.pow(chordDistance / 2, 2));
		double xDistance = distance * (startPoint.getY() - endPoint.getY()) / chordDistance;
		double yDistance = distance * (endPoint.getX() - startPoint.getX()) / chordDistance;
		
		//these are the two candidates for the centre point
		Point2D centre1 = new Point2D(chordCentre.getX() - xDistance, chordCentre.getY() - yDistance);
		Point2D centre2 = new Point2D(chordCentre.getX() + xDistance, chordCentre.getY() + yDistance);

 		return Pair.createPair(centre1, centre2);
	}

    @Override
    public String toString()
    {
        return "Arc{startPoint=" + startPoint + ", endPoint=" + endPoint + ", centrePoint=" + centrePoint +
                ", direction=" + direction + ", circleCircumference=" + circleCircumference + ", radius=" + radius + '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Arc arc = (Arc) o;

        if (startPoint != null ? !startPoint.equals(arc.startPoint) : arc.startPoint != null) return false;
        if (endPoint != null ? !endPoint.equals(arc.endPoint) : arc.endPoint != null) return false;
        //noinspection SimplifiableIfStatement
        if (centrePoint != null ? !centrePoint.equals(arc.centrePoint) : arc.centrePoint != null) return false;
        return direction == arc.direction;

    }

    @Override
    public int hashCode()
    {
        int result = startPoint != null ? startPoint.hashCode() : 0;
        result = 31 * result + (endPoint != null ? endPoint.hashCode() : 0);
        result = 31 * result + (centrePoint != null ? centrePoint.hashCode() : 0);
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        return result;
    }

    public enum Direction {CW, CCW}
    public enum Coords {MAXX, MINX, MAXY, MINY}
}
