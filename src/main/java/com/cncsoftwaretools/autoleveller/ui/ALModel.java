/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui;

import com.cncsoftwaretools.autoleveller.*;
import com.google.common.collect.ImmutableList;
import javafx.application.HostServices;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The model holding data to be shared among all controllers
 * A Singleton with each element as a property so that events can be triggered if changed
 * Created by James Hawthorne on 06/07/2015.
 */
public class ALModel
{
    private static final String DEFAULT_DIR_KEY = "defaultDir";
    private static final String UNITS_KEY = "preferredUnits";
    private static final String CONTROLLER_KEY = "preferredController";
    public static final String CST_PROBE_WORD = "customProbeWord";
    public static final String CST_PROBE_CMD = "customProbeCommand";
    public static final String CST_ZERO_WORD = "customZeroWord";
    public static final String CST_ZERO_CMD = "customZeroCommand";
    public static final String CST_CURRENT_Z = "customZParameter";
    public static final String CST_STARTING_PARM = "customStartingParameter";
    public static final String CST_OPEN_LOG = "customOpenLog";
    public static final String CST_CLOSE_LOG = "customCloseLog";
    public static final String CST_FILE_EXT = "customFileExtension";
    public static final String USE_CST_PFG_INIT = "usePFGInit";
    public static final String USE_CST_LVL_INIT = "useLvlInit";
    public static final String CST_PFG_INIT_BLOCK = "customPFGInitBlock";
    public static final String CST_LVL_INIT_BLOCK = "customLvlInitBlock";

    public static final String XY_FEED_KEY = "preferredXYFeed";
    public static final String Z_FEED_KEY = "preferredZFeed";
    public static final String PROBE_DEPTH = "preferredProbeDepth";
    public static final String PROBE_CLEARANCE = "preferredProbeClearance";
    public static final String PROBE_SPACING = "preferredProbeSpacing";
    public static final String SAFE_HEIGHT = "preferredSafeHeight";
    public static final String MAX_SEG_LTH = "preferredMaxSegLth";
    public static final String LEFT_INSET = "preferredLeftInset";
    public static final String RIGHT_INSET = "preferredRightInset";
    public static final String TOP_INSET = "preferredTopInset";
    public static final String BOTTOM_INSET = "preferredBottomInset";
    public static final String WIDTH = "preferredWidth";
    public static final String HEIGHT = "preferredHeight";
    private static final List<String> mainPrefsLst = ImmutableList.of(DEFAULT_DIR_KEY, UNITS_KEY, CONTROLLER_KEY,
            CST_PROBE_WORD, CST_PROBE_CMD, CST_ZERO_WORD, CST_ZERO_CMD, CST_CURRENT_Z, CST_STARTING_PARM,
            CST_OPEN_LOG, CST_CLOSE_LOG, CST_FILE_EXT, WIDTH, HEIGHT, USE_CST_PFG_INIT, USE_CST_LVL_INIT,
            CST_PFG_INIT_BLOCK, CST_LVL_INIT_BLOCK);
    private static final List<String> unitsPrefsLst = ImmutableList.of(XY_FEED_KEY, Z_FEED_KEY, PROBE_DEPTH,
            PROBE_CLEARANCE, PROBE_SPACING, SAFE_HEIGHT, MAX_SEG_LTH, LEFT_INSET, RIGHT_INSET, TOP_INSET, BOTTOM_INSET);
    private static ALModel MODEL;

    private final Preferences prefs;
    private final Preferences mmPrefs;
    private final Preferences inPrefs;

    private final ObjectProperty<OGF> ogf = new SimpleObjectProperty<>(this, "ogf");
    private final ObjectProperty<Mesh> mesh = new SimpleObjectProperty<>(this, "mesh");
    private final ObjectProperty<RPF> rpf = new SimpleObjectProperty<>(this, "rpf");

    private final List<ALModelListener> listeners = new ArrayList<>();
    private HostServices hostServices;

    private ALModel()
    {
        try {
            Files.createDirectories(Paths.get(System.getProperty("user.home"), ".AE"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Path prefsPath = Paths.get(System.getProperty("user.home"), ".AE", "settings.xml");
        Path mmPrefsPath = Paths.get(System.getProperty("user.home"), ".AE", "mmOptions.xml");
        Path inPrefsPath = Paths.get(System.getProperty("user.home"), ".AE", "inOptions.xml");

        prefs = PreferenceImpl.newInstance(prefsPath);
        mmPrefs = PreferenceImpl.newInstance(mmPrefsPath);
        inPrefs = PreferenceImpl.newInstance(inPrefsPath);

        setDefaultPrefs();
    }

    private void setDefaultPrefs()
    {
        DecimalFormat df = Autoleveller.DF;
        DefaultMeshValues dmv;

        if (prefs.get(UNITS_KEY).equals(Optional.empty())){prefs.set(UNITS_KEY, Units.MM.toString());}
        if (prefs.get(DEFAULT_DIR_KEY).equals(Optional.empty())){prefs.set(DEFAULT_DIR_KEY, System.getProperty("user.home"));}
        if (prefs.get(CONTROLLER_KEY).equals(Optional.empty())){prefs.set(CONTROLLER_KEY, Controller.LINUXCNC.toString());}
        if (prefs.get(CST_PROBE_WORD).equals(Optional.empty())){prefs.set(CST_PROBE_WORD, " ");}
        if (prefs.get(CST_PROBE_CMD).equals(Optional.empty())){prefs.set(CST_PROBE_CMD, " ");}
        if (prefs.get(CST_ZERO_WORD).equals(Optional.empty())){prefs.set(CST_ZERO_WORD, " ");}
        if (prefs.get(CST_ZERO_CMD).equals(Optional.empty())){prefs.set(CST_ZERO_CMD, " ");}
        if (prefs.get(CST_CURRENT_Z).equals(Optional.empty())){prefs.set(CST_CURRENT_Z, " ");}
        if (prefs.get(CST_STARTING_PARM).equals(Optional.empty())){prefs.set(CST_STARTING_PARM, " ");}
        if (prefs.get(CST_OPEN_LOG).equals(Optional.empty())){prefs.set(CST_OPEN_LOG, " ");}
        if (prefs.get(CST_CLOSE_LOG).equals(Optional.empty())){prefs.set(CST_CLOSE_LOG, " ");}
        if (prefs.get(CST_FILE_EXT).equals(Optional.empty())){prefs.set(CST_FILE_EXT, " ");}
        if (prefs.get(WIDTH).equals(Optional.empty())){prefs.set(WIDTH, "1024");}
        if (prefs.get(HEIGHT).equals(Optional.empty())){prefs.set(HEIGHT, "730");}
        if (prefs.get(USE_CST_PFG_INIT).equals(Optional.empty())){prefs.set(USE_CST_PFG_INIT, "false");}
        if (prefs.get(USE_CST_LVL_INIT).equals(Optional.empty())){prefs.set(USE_CST_LVL_INIT, "false");}
        if (prefs.get(CST_PFG_INIT_BLOCK).equals(Optional.empty())){prefs.set(CST_PFG_INIT_BLOCK, " ");}
        if (prefs.get(CST_LVL_INIT_BLOCK).equals(Optional.empty())){prefs.set(CST_LVL_INIT_BLOCK, " ");}

        dmv = DefaultMeshValues.MM;
        if (mmPrefs.get(MAX_SEG_LTH).equals(Optional.empty())){mmPrefs.set(MAX_SEG_LTH, "5");}
        if (mmPrefs.get(XY_FEED_KEY).equals(Optional.empty())){mmPrefs.set(XY_FEED_KEY, df.format(dmv.getXYFeed()));}
        if (mmPrefs.get(Z_FEED_KEY).equals(Optional.empty())){mmPrefs.set(Z_FEED_KEY, df.format(dmv.getZFeed()));}
        if (mmPrefs.get(PROBE_DEPTH).equals(Optional.empty())){mmPrefs.set(PROBE_DEPTH, df.format(dmv.getProbeDepth()));}
        if (mmPrefs.get(PROBE_CLEARANCE).equals(Optional.empty())){mmPrefs.set(PROBE_CLEARANCE, df.format(dmv.getProbeClearance()));}
        if (mmPrefs.get(PROBE_SPACING).equals(Optional.empty())){mmPrefs.set(PROBE_SPACING, df.format(dmv.getPointSpacing()));}
        if (mmPrefs.get(SAFE_HEIGHT).equals(Optional.empty())){mmPrefs.set(SAFE_HEIGHT, df.format(dmv.getSafeHeight()));}
        if (mmPrefs.get(LEFT_INSET).equals(Optional.empty())){mmPrefs.set(LEFT_INSET, df.format(dmv.getInset(Mesh.Inset.LEFT)));}
        if (mmPrefs.get(RIGHT_INSET).equals(Optional.empty())){mmPrefs.set(RIGHT_INSET, df.format(dmv.getInset(Mesh.Inset.RIGHT)));}
        if (mmPrefs.get(TOP_INSET).equals(Optional.empty())){mmPrefs.set(TOP_INSET, df.format(dmv.getInset(Mesh.Inset.TOP)));}
        if (mmPrefs.get(BOTTOM_INSET).equals(Optional.empty())){mmPrefs.set(BOTTOM_INSET, df.format(dmv.getInset(Mesh.Inset.BOTTOM)));}

        dmv = DefaultMeshValues.INCHES;
        if (inPrefs.get(MAX_SEG_LTH).equals(Optional.empty())){inPrefs.set(MAX_SEG_LTH, "0.187");}
        if (inPrefs.get(XY_FEED_KEY).equals(Optional.empty())){inPrefs.set(XY_FEED_KEY, df.format(dmv.getXYFeed()));}
        if (inPrefs.get(Z_FEED_KEY).equals(Optional.empty())){inPrefs.set(Z_FEED_KEY, df.format(dmv.getZFeed()));}
        if (inPrefs.get(PROBE_DEPTH).equals(Optional.empty())){inPrefs.set(PROBE_DEPTH, df.format(dmv.getProbeDepth()));}
        if (inPrefs.get(PROBE_CLEARANCE).equals(Optional.empty())){inPrefs.set(PROBE_CLEARANCE, df.format(dmv.getProbeClearance()));}
        if (inPrefs.get(PROBE_SPACING).equals(Optional.empty())){inPrefs.set(PROBE_SPACING, df.format(dmv.getPointSpacing()));}
        if (inPrefs.get(SAFE_HEIGHT).equals(Optional.empty())){inPrefs.set(SAFE_HEIGHT, df.format(dmv.getSafeHeight()));}
        if (inPrefs.get(LEFT_INSET).equals(Optional.empty())){inPrefs.set(LEFT_INSET, df.format(dmv.getInset(Mesh.Inset.LEFT)));}
        if (inPrefs.get(RIGHT_INSET).equals(Optional.empty())){inPrefs.set(RIGHT_INSET, df.format(dmv.getInset(Mesh.Inset.RIGHT)));}
        if (inPrefs.get(TOP_INSET).equals(Optional.empty())){inPrefs.set(TOP_INSET, df.format(dmv.getInset(Mesh.Inset.TOP)));}
        if (inPrefs.get(BOTTOM_INSET).equals(Optional.empty())){inPrefs.set(BOTTOM_INSET, df.format(dmv.getInset(Mesh.Inset.BOTTOM)));}
    }

    public static ALModel getInstance()
    {
        if (MODEL == null){MODEL = new ALModel();}

        return MODEL;
    }

    static void clearModel()
    {
        MODEL = null;
    }

    //HostServices
    public HostServices getHostServices(){return hostServices;}

    public void setHostServices(HostServices hostServices){this.hostServices = hostServices;}

    //RPF
    public RPF getRPF(){return rpf.get();}

    public void setRPF(RPF rpf) {this.rpf.set(rpf);}

    public ObjectProperty<RPF> rpfProperty(){return rpf;}

    //OGF
    public OGF getOGF()
    {
        return ogf.get();
    }

    public void setOGF(OGF ogf) {this.ogf.set(ogf);}

    public ObjectProperty<OGF> ogfProperty() {return ogf;}

    //Mesh
    public Mesh getMesh()
    {
        return mesh.get();
    }

    public void setMesh(Mesh mesh) {this.mesh.set(mesh);}

    public ObjectProperty<Mesh> meshProperty() {return mesh;}

    public void setUnits(Units units){
        notifyListeners(ALModel.UNITS_KEY);
        prefs.set(ALModel.UNITS_KEY, units.toString());
    }

    public Units readUnits()
    {
        Optional<Units> unitsOpt = Units.fromString(prefs.get(ALModel.UNITS_KEY).orElseThrow(IllegalArgumentException::new));

        return unitsOpt.orElseThrow(IllegalArgumentException::new);
    }

    public void writeController(Controller controller){
        notifyListeners(ALModel.CONTROLLER_KEY);
        prefs.set(ALModel.CONTROLLER_KEY, controller.toString());
    }

    public Controller readController()
    {
        Optional<Controller> controllerOpt = Controller.fromString((prefs.get(ALModel.CONTROLLER_KEY).orElseThrow(IllegalArgumentException::new)));

        return controllerOpt.orElseThrow(IllegalArgumentException::new);
    }

    public void writeDirectory(String dir) {
        notifyListeners(ALModel.DEFAULT_DIR_KEY);
        prefs.set(DEFAULT_DIR_KEY, dir);
    }

    public String readDirectory()
    {
        return prefs.get(DEFAULT_DIR_KEY).orElse(System.getProperty("user.home"));
    }

    public boolean readUsePFGZInit(){return Boolean.parseBoolean(readString(USE_CST_PFG_INIT));}

    public boolean readUseLvlZInit(){return Boolean.parseBoolean(readString(USE_CST_LVL_INIT));}

    public void writeUsePFGZInit(boolean usePFGInit){writeString(USE_CST_PFG_INIT, String.valueOf(usePFGInit));}

    public void writeUseLvlZInit(boolean useLvlInit){writeString(USE_CST_LVL_INIT, String.valueOf(useLvlInit));}

    public void writeDouble(String key, double value)
    {
        writeString(key, Autoleveller.DF.format(value));
    }

    public double readDouble(String key)
    {
        return Double.parseDouble(readString(key));
    }

    public void writeString(String key, String value)
    {
        if (!mainPrefsLst.contains(key) && !unitsPrefsLst.contains(key)) {
            throw new IllegalArgumentException("key: " + key + " not known to ALModel");
        } else if (mainPrefsLst.contains(key)) {
            prefs.set(key, value);
            notifyListeners(key);
        } else { //key must be in unitsPrefsLst
            Preferences unitsPrefs = readUnits().equals(Units.MM) ? mmPrefs : inPrefs;
            unitsPrefs.set(key, value);
            notifyListeners(key);
        }
    }

    public String readString(String key)
    {
        if (!mainPrefsLst.contains(key) && !unitsPrefsLst.contains(key)) {
            throw new IllegalArgumentException("key: " + key + " not known to ALModel");
        } else if (mainPrefsLst.contains(key)) {
            return prefs.get(key).orElseThrow(IllegalArgumentException::new);
        } else { //key must be in unitsPrefsLst
            Preferences unitsPrefs = readUnits().equals(Units.MM) ? mmPrefs : inPrefs;
            return unitsPrefs.get(key).orElseThrow(IllegalArgumentException::new);
        }
    }

    public void addChangeListener(ALModelListener alModelListener){listeners.add(alModelListener);}

    private void notifyListeners(String changedProperty)
    {
        listeners.forEach(alModelListener -> alModelListener.modelChanged(changedProperty));
    }
}