/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.Controller;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by James Hawthorne on 09/05/2017.
 *
 */
public class CustomControllerDialog extends Stage implements Initializable
{
    @FXML private TextField pWordTxt;
    @FXML private TextField pCommandTxt;
    @FXML private TextField zeroWordTxt;
    @FXML private TextField zeroCommandTxt;
    @FXML private TextField currentZTxt;
    @FXML private TextField startingParmTxt;
    @FXML private TextField openLogTxt;
    @FXML private TextField closeLogTxt;
    @FXML private TextField fileExtTxt;
    @FXML private TextArea exampleGCodeTxt;

    private CustomControllerDialog()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ALGUI/CstCntDlg.fxml"));
        fxmlLoader.setController(this);

        try {
            setScene(new Scene(fxmlLoader.load()));
            Stage stage = (Stage) getScene().getWindow();
            stage.getIcons().add(new Image("alicon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static CustomControllerDialog newInstance()
    {
        return new CustomControllerDialog();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        setResizable(false);
        setTitle("Custom Controller Options");
        setFieldsFromModel();
        setExampleGCodeTxt();
    }

    @FXML
    private void textFieldKeyPressed(KeyEvent event)
    {
        if (event.getCode().equals(KeyCode.ENTER) || event.getCode().equals(KeyCode.TAB)){
            setExampleGCodeTxt();
        }
    }

    @FXML
    private void onApply()
    {
        setExampleGCodeTxt();
        setModelFromFields();
    }

    private void setModelFromFields()
    {
        ALModel model = ALModel.getInstance();

        model.writeString(ALModel.CST_PROBE_WORD, pWordTxt.getText());
        model.writeString(ALModel.CST_PROBE_CMD, pCommandTxt.getText());
        model.writeString(ALModel.CST_ZERO_WORD, zeroWordTxt.getText());
        model.writeString(ALModel.CST_ZERO_CMD, zeroCommandTxt.getText());
        model.writeString(ALModel.CST_CURRENT_Z, currentZTxt.getText());
        model.writeString(ALModel.CST_STARTING_PARM, startingParmTxt.getText());
        model.writeString(ALModel.CST_OPEN_LOG, openLogTxt.getText());
        model.writeString(ALModel.CST_CLOSE_LOG, closeLogTxt.getText());
        model.writeString(ALModel.CST_FILE_EXT, fileExtTxt.getText());
    }

    private void setFieldsFromModel()
    {
        Controller cstCnt = Controller.CUSTOM;

        pWordTxt.setText(cstCnt.getProbeWord());
        pCommandTxt.setText(cstCnt.getProbeCommand());
        zeroWordTxt.setText(cstCnt.getZeroWord());
        zeroCommandTxt.setText(cstCnt.getZeroCommand());
        currentZTxt.setText(cstCnt.getCurrentZparameter());
        startingParmTxt.setText(cstCnt.getStartingParameter());
        openLogTxt.setText(cstCnt.getOpenLogCommand());
        closeLogTxt.setText(cstCnt.getCloseLogCommand());
        fileExtTxt.setText(cstCnt.getDefaultExt());
    }

    @FXML
    private void onCancel()
    {
        close();
    }

    @FXML
    private void onOK()
    {
        onApply();
        close();
    }

    private void setExampleGCodeTxt()
    {
        int startingParmNum = startingParmTxt.getText().matches("^#\\d+$") ?
                Integer.parseInt(startingParmTxt.getText().substring(1)) : -1;
        if (startingParmNum == -1){startingParmTxt.setText("");}

        exampleGCodeTxt.setText("M0 (Attach probe wires and clips that need attaching)\n" +
                "(Initialize probe routine)\n" +
                "G0 Z25 (Move clear of the board first)\n" +
                "G1 X12.042 Y10.1 F600 (Move to bottom left corner)\n" +
                "G0 Z1 (Quick move to probe clearance height)\n" +
                (!pWordTxt.getText().isEmpty() ? pWordTxt.getText() + " Z-1 F100\n" : "") +
                pCommandTxt.getText() + (!pCommandTxt.getText().isEmpty() ? "\n" : "") +
                (!zeroWordTxt.getText().isEmpty() ? zeroWordTxt.getText() + " Z0\n" : "") +
                zeroCommandTxt.getText() + (!zeroCommandTxt.getText().isEmpty() ? "\n" : "") +
                "G0 Z1 (Move Z to above the contact point)\n" +
                (!pWordTxt.getText().isEmpty() ? pWordTxt.getText() + " Z-1 F100\n" : "") +
                pCommandTxt.getText() + (!pCommandTxt.getText().isEmpty() ? "\n" : "") +
                (!zeroWordTxt.getText().isEmpty() ? zeroWordTxt.getText() + " Z0\n" : "") +
                zeroCommandTxt.getText() + (!zeroCommandTxt.getText().isEmpty() ? "\n" : "") +
                "G0 Z1\n" +
                "\n" +
                openLogTxt.getText() + (!openLogTxt.getText().isEmpty() ? "\n" : "") +
                "G0 Z1\n" +
                "G1 X12.042 Y10.1 F600\n" +
                (!pWordTxt.getText().isEmpty() ? pWordTxt.getText() + " Z-1 F100\n" : "") +
                pCommandTxt.getText() + (!pCommandTxt.getText().isEmpty() ? "\n" : "") +
                (startingParmNum != -1 ? "#" + startingParmNum++ + "=" : "") + //print line1 if starting parm specified
                currentZTxt.getText() + //print line2 (blank if current Z not specified)
                (startingParmNum > -1 || !currentZTxt.getText().isEmpty() ? "\n" : "") + //print new line if either line1 or line 2 not blank
                "G0 Z1\n" +
                "G1 X32.64083 Y10.1 F600\n" +
                (!pWordTxt.getText().isEmpty() ? pWordTxt.getText() + " Z-1 F100\n" : "") +
                pCommandTxt.getText() + (!pCommandTxt.getText().isEmpty() ? "\n" : "") +
                (startingParmNum != -1 ? "#" + startingParmNum++ + "=" : "") + //print line1 if starting parm specified
                currentZTxt.getText() + //print line2 (blank if current Z not specified)
                (startingParmNum > -1 || !currentZTxt.getText().isEmpty() ? "\n" : "") + //print new line if either line1 or line 2 not blank
                "G0 Z1\n" +
                "G1 X53.23966 Y10.1 F600\n" +
                (!pWordTxt.getText().isEmpty() ? pWordTxt.getText() + " Z-1 F100\n" : "") +
                pCommandTxt.getText() + (!pCommandTxt.getText().isEmpty() ? "\n" : "") +
                (startingParmNum != -1 ? "#" + startingParmNum++ + "=" : "") + //print line1 if starting parm specified
                currentZTxt.getText() + //print line2 (blank if current Z not specified)
                (startingParmNum > -1 || !currentZTxt.getText().isEmpty() ? "\n" : "") + //print new line if either line1 or line 2 not blank
                "G0 Z1\n" +
                "G1 X73.83849 Y10.1 F600\n" +
                (!pWordTxt.getText().isEmpty() ? pWordTxt.getText() + " Z-1 F100\n" : "") +
                pCommandTxt.getText() + (!pCommandTxt.getText().isEmpty() ? "\n" : "") +
                (startingParmNum != -1 ? "#" + startingParmNum++ + "=" : "") + //print line1 if starting parm specified
                currentZTxt.getText() + //print line2 (blank if current Z not specified)
                (startingParmNum > -1 || !currentZTxt.getText().isEmpty() ? "\n" : "") + //print new line if either line1 or line 2 not blank
                "G0 Z1\n" +
                "...\n" +
                closeLogTxt.getText());
    }

    void popupAndWait()
    {
        showAndWait();
    }
}
