/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.ALWriter;
import com.cncsoftwaretools.autoleveller.Controller;
import com.cncsoftwaretools.autoleveller.RPF;
import com.cncsoftwaretools.autoleveller.reader.Word;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.cncsoftwaretools.autoleveller.util.AutolevellerUtil;
import com.google.common.io.Files;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * Created by James Hawthorne on 26/03/2017.
 *
 */
public class RPFFixer extends Stage implements Initializable
{
    private final String regexStr = ".*X(" + Word.DECIMALPATTERN.pattern() + ")\\s{0,3}Y(" + Word.DECIMALPATTERN.pattern() + ").*";
    private CommonFXML commonFXML = new CommonFXML();
    private List<Point2D> pfgList = new ArrayList<>();
    private List<Point3D> rpfList = new ArrayList<>();
    private FileChooser fc = commonFXML.createFileChooser("Browse...");
    private ALModel model = ALModel.getInstance();

    @FXML private TextField pfgPathTxt;
    @FXML private Label pfgPointsLbl;
    @FXML private Label pfgXsLbl;
    @FXML private Label pfgYsLbl;
    @FXML private TextField rpfPathTxt;
    @FXML private Label rpfPointsLbl;
    @FXML private Label rpfXsLbl;
    @FXML private Label rpfYsLbl;
    @FXML private ImageView matchImg;

    private RPFFixer()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ALGUI/RPFAlign.fxml"));
        fxmlLoader.setController(this);

        try {
            setScene(new Scene(fxmlLoader.load()));
            Stage stage = (Stage) getScene().getWindow();
            stage.getIcons().add(new Image("alicon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onPFGBrowse()
    {
        Optional<File> fileOpt = Optional.ofNullable(fc.showOpenDialog(null));

        try {
            if (fileOpt.isPresent()) {
                Controller c = model.readController();
                List<String> probeLines = AutolevellerUtil.getFileLinesFromTo(
                        fileOpt.get().getAbsolutePath(), c.getOpenLogCommand(), c.getCloseLogCommand());
                List<String> xyLines = AutolevellerUtil.getLinesMatchingRegex(probeLines, regexStr);
                pfgList = xyLines.stream().map(mapTextToPoint2D).collect(toList());
                displayPointInfo(pfgList, fileOpt.get(), pfgPathTxt, pfgPointsLbl, pfgXsLbl, pfgYsLbl);

                matchImg.setImage(getMatchImg());
            }
        }
        catch (RuntimeException e){
            clearDisplayPointInfo(pfgList, pfgPathTxt, pfgPointsLbl, pfgXsLbl, pfgYsLbl);

            new Alert(Alert.AlertType.ERROR, "There was an error reading the PFG file. " +
                    "The likely cause is that the chosen file is of an incorrect format. " +
                    "Check that you have the correct controller selected in the Mesh tab " +
                    "and that the chosen file contains probe lines preceded by X and Y lines", ButtonType.OK).showAndWait();
        }
    }

    @FXML
    private void onRPFBrowse()
    {
        Optional<File> fileOpt = Optional.ofNullable(fc.showOpenDialog(null));

        try {
            if (fileOpt.isPresent()) {
                rpfList = RPF.createOrderedPointList(fileOpt.get().getAbsolutePath());
                List<Point2D> rpfList2D = rpfList.stream().map(p3D -> new Point2D(p3D.getX(), p3D.getY())).collect(toList());
                displayPointInfo(rpfList2D, fileOpt.get(), rpfPathTxt, rpfPointsLbl, rpfXsLbl, rpfYsLbl);

                matchImg.setImage(getMatchImg());
            }
        }
        catch (RuntimeException e){
            clearDisplayPointInfo(rpfList, rpfPathTxt, rpfPointsLbl, rpfXsLbl, rpfYsLbl);

            new Alert(Alert.AlertType.ERROR, "There was an error reading the RPF file. " +
                    "The likely cause is that the chosen file is of an incorrect format", ButtonType.OK).showAndWait();
        }
    }

    private Function<String, Point2D> mapTextToPoint2D = line -> {
        Matcher m = Pattern.compile(regexStr).matcher(line);
        //noinspection ResultOfMethodCallIgnored
        m.find();

        return new Point2D(Double.valueOf(m.group(1)), Double.valueOf(m.group(3)));
    };

    private Image getMatchImg()
    {
        List<Point2D> rpfList2D = rpfList.stream().map(p3D -> new Point2D(p3D.getX(), p3D.getY())).collect(toList());

        if (pfgList.isEmpty() || rpfList2D.isEmpty()){
            return new Image("unknown-blue-300px.png");
        }
        else if (pfgList.equals(rpfList2D)){
            return new Image("green-tick-300px.png");
        }
        else{
            return new Image("red-cross-300px.png");
        }
    }

    private void displayPointInfo(List<Point2D> ps, File path, TextField pathTxt, Label pLbl, Label xLbl, Label yLbl)
    {
        pathTxt.setText(path.getAbsolutePath());
        pLbl.setText("Points: " + Integer.toString(ps.size()));
        xLbl.setText("Distinct X's: " + Long.toString(ps.stream().map(Point2D::getX).distinct().count()));
        yLbl.setText("Distinct Y's: " + Long.toString(ps.stream().map(Point2D::getY).distinct().count()));
    }

    private void clearDisplayPointInfo(List points, TextField pathTxt, Label pLbl, Label xLbl, Label yLbl)
    {
        points.clear();
        pathTxt.setText("");
        pLbl.setText("Points:");
        xLbl.setText("Distinct X's:");
        yLbl.setText("Distinct Y's:");
        matchImg.setImage(new Image("unknown-blue-300px.png"));
    }

    @FXML
    private void onCombine()
    {
        if ((!pfgList.isEmpty() && !rpfList.isEmpty()) && (pfgList.size() == rpfList.size())) {
            FileChooser fc = commonFXML.createFileChooser("Save");
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text File", "*.txt"));
            String initialFilename = Files.getNameWithoutExtension("Fixed".concat(new File(rpfPathTxt.getText()).getName()));
            fc.setInitialFileName(initialFilename.concat(".").concat("txt"));

            Optional<File> fileOptional = Optional.ofNullable(fc.showSaveDialog(null));
            fileOptional.ifPresent(file -> {
                try {
                    ALWriter.useALWriter(new PrintWriter(new BufferedWriter(new FileWriter(file, false))), writeFixedRPF());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        else{
            new Alert(Alert.AlertType.ERROR, "You need to first browse for a PFG and RPF and these files " +
                    "need to contain an equal number of points before you can combine them", ButtonType.OK).showAndWait();
        }
    }

    private Consumer<ALWriter> writeFixedRPF()
    {
        return w ->
            IntStream.range(0, pfgList.size()).forEachOrdered(idx -> w.writeGCODELine(
                            "X" + pfgList.get(idx).getX() +
                            " Y" + pfgList.get(idx).getY() +
                            " Z" + rpfList.get(idx).getZ()));
    }

    @FXML
    private void onCancel()
    {
        close();
    }

    static RPFFixer newInstance()
    {
        return new RPFFixer();
    }

    void popupAndWait()
    {
        showAndWait();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        setResizable(false);
        setTitle("RPF Fixer");
    }
}
