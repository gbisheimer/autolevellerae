/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.*;
import com.cncsoftwaretools.autoleveller.reader.PlainGCodeReader;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * A controller used for the mesh tab of the tab pane
 * Used to visualise the size, position and number of points in the probe mesh
 * The mesh can be dynamically changed based on the settings
 * Created by James Hawthorne on 06/07/2015.
 */
public class MeshTabController implements Initializable
{
    @FXML private TextField startingXTxt;
    @FXML private TextField startingYTxt;
    @FXML private TextField xLengthTxt;
    @FXML private TextField yLengthTxt;
    @FXML private TextField xyFeedTxt;
    @FXML private TextField zFeedTxt;
    @FXML private TextField pDepthTxt;
    @FXML private TextField pClearanceTxt;
    @FXML private TextField pSpacingTxt;
    @FXML private TextField pSafeHeightTxt;
    @FXML private TextField pntsTtlTxt;
    @FXML private TextField colsTxt;
    @FXML private TextField rowsTxt;
    @FXML private Pane probeLayoutPnl;
    @FXML private ChoiceBox<Units> unitsChc;
    @FXML private ChoiceBox<Controller> controllerChc;
    @FXML private Button insetLnkBtn;
    @FXML private TextField insetLTxt;
    @FXML private TextField insetRTxt;
    @FXML private TextField insetTTxt;
    @FXML private TextField insetBTxt;

    private final ALModel model = ALModel.getInstance();
    private final CommonFXML commonController = new CommonFXML();
    private List<TextField> meshBoxes;
    private final ResizableCanvas canvas = new ResizableCanvas();

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        meshBoxes = ImmutableList.of(startingXTxt, startingYTxt, xLengthTxt, yLengthTxt,
                xyFeedTxt, zFeedTxt, pDepthTxt, pClearanceTxt, pSpacingTxt, pSafeHeightTxt,
                insetLTxt, insetRTxt, insetTTxt, insetBTxt);
        meshBoxes.forEach(textField -> textField.focusedProperty()
                .addListener((observable, oldValue, gainedFocus) -> meshFieldFocusEvent(gainedFocus)));

        unitsChc.setItems(FXCollections.observableArrayList(Units.values()));
        unitsChc.getSelectionModel().select(model.readUnits());
        unitsChc.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue)){clearOptionalFields();}
            model.setUnits(newValue);
            model.writeDouble(ALModel.MAX_SEG_LTH, model.readDouble(ALModel.MAX_SEG_LTH));
            setMeshPromptsForUnits(newValue);
            setMeshFromGUIFields(); //ensure mesh units is changed when unitsChc is changed
        });
        Optional.ofNullable(model.readUnits()).ifPresent(this::setMeshPromptsForUnits);
        controllerChc.setItems(FXCollections.observableArrayList(Controller.values()));
        controllerChc.valueProperty().addListener((observable, oldValue, newValue) -> {
            model.writeController(newValue);
            setMeshFromGUIFields(); //ensure mesh controller is changed when controllerChc is changed
        });
        controllerChc.getSelectionModel().select(model.readController());

        model.meshProperty().addListener(observable -> safeSwitchModel());

        model.ogfProperty().addListener((observable, oldValue, newValue) -> {
            Optional.ofNullable(newValue).ifPresent(newOGF -> {
                unitsChc.getSelectionModel().select(newOGF.getUnits());
                unitsChc.setDisable(true);
                model.setMesh(null); //make sure the mesh gets cleared when OGF is changed
                clearGUIFields();
            });
            if (!Optional.ofNullable(newValue).isPresent()){unitsChc.setDisable(false);}
            safeSwitchModel();
        });
        probeLayoutPnl.getChildren().add(canvas);
        canvas.widthProperty().bind(probeLayoutPnl.widthProperty());
        canvas.heightProperty().bind(probeLayoutPnl.heightProperty());
    }

    private void safeSwitchModel()
    {
        canvas.setDrawItems(ImmutableList.of());
        canvas.clear();
        setCanvases();
        setGUIFieldsFromMesh();
        canvas.draw();
    }

    private void setCanvases()
    {
        final ImmutableList.Builder<Drawing> canvases = ImmutableList.builder();

        Optional.ofNullable(model.getOGF()).ifPresent(ogf -> {
            try {
                canvases.add(
                        new OGFCanvas(ogf, PlainGCodeReader.newInstance(new BufferedReader(new FileReader(ogf.getFilename()))), canvas));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        Optional.ofNullable(model.getMesh()).ifPresent(mesh -> canvases.add(new MeshCanvas(mesh, canvas)));

        canvas.setDrawItems(canvases.build());
    }

    private void clearOptionalFields()
    {
        xyFeedTxt.setText("");
        zFeedTxt.setText("");
        pDepthTxt.setText("");
        pClearanceTxt.setText("");
        pSpacingTxt.setText("");
        pSafeHeightTxt.setText("");
    }

    private void meshFieldFocusEvent(Boolean gainedFocus)
    {
        if (!gainedFocus){ //if the focus is lost
            setMeshFromGUIFields();
        }
    }

    @FXML
    private void textFieldKeyPressed(KeyEvent event)
    {
        if (event.getCode().equals(KeyCode.ENTER)){
            setMeshFromGUIFields();
        }
    }

    private void setMeshFromGUIFields()
    {
        model.setMesh(createMeshFromGUI().orElse(null));
        canvas.draw();
    }

    private void setGUIFieldsFromMesh()
    {
        Optional.ofNullable(model.getMesh()).ifPresent(this::syncGUIWithMesh);
        canvas.draw();
    }

    private void clearGUIFields()
    {
        startingXTxt.clear();
        startingYTxt.clear();
        xLengthTxt.clear();
        yLengthTxt.clear();
        xyFeedTxt.clear();
        zFeedTxt.clear();
        pDepthTxt.clear();
        pClearanceTxt.clear();
        pSpacingTxt.clear();
        pSafeHeightTxt.clear();
        pntsTtlTxt.clear();
        colsTxt.clear();
        rowsTxt.clear();

        insetLTxt.setDisable(true);
        insetRTxt.setDisable(true);
        insetTTxt.setDisable(true);
        insetBTxt.setDisable(true);

        insetLTxt.clear();
        insetRTxt.clear();
        insetTTxt.clear();
        insetBTxt.clear();
    }

    private Optional<Mesh> createMeshFromGUI()
    {
        meshBoxes.forEach(textField -> {
            if (!isValidField(textField)){textField.setText("");}
        });

        if (startingXTxt.getText().equals("") || startingYTxt.getText().equals("") ||
                xLengthTxt.getText().equals("") || yLengthTxt.getText().equals(""))
            return Optional.empty();

        Mesh.Builder meshBuilder = new Mesh.Builder(model.readUnits(), model.readController(),
                new BigDecimal(startingXTxt.getText()), new BigDecimal(startingYTxt.getText()),
                new BigDecimal(xLengthTxt.getText()), new BigDecimal(yLengthTxt.getText()));
        meshBuilder.buildXYFeed(setMeshField(ALModel.XY_FEED_KEY, xyFeedTxt));
        meshBuilder.buildZFeed(setMeshField(ALModel.Z_FEED_KEY, zFeedTxt));
        meshBuilder.buildDepth(setMeshField(ALModel.PROBE_DEPTH, pDepthTxt));
        meshBuilder.buildClearance(setMeshField(ALModel.PROBE_CLEARANCE, pClearanceTxt));
        meshBuilder.buildSpacing(setMeshField(ALModel.PROBE_SPACING, pSpacingTxt));
        meshBuilder.buildSafeHeight(setMeshField(ALModel.SAFE_HEIGHT, pSafeHeightTxt));

        buildInsets(meshBuilder);

        return Optional.of(meshBuilder.build());
    }

    @FXML
    private void insetLinkClick()
    {
        if (NumberUtils.isNumber(insetLTxt.getText()))
        {
            insetRTxt.setText(insetLTxt.getText());
            insetTTxt.setText(insetLTxt.getText());
            insetBTxt.setText(insetLTxt.getText());

            setMeshFromGUIFields();
        }
    }

    private void buildInsets(Mesh.Builder meshBuilder)
    {
        try{
            double lInsetTxtToDouble = NumberUtils.isNumber(insetLTxt.getText()) ?
                    Double.parseDouble(insetLTxt.getText()) : model.readDouble(ALModel.LEFT_INSET);
            double rInsetTxtToDouble = NumberUtils.isNumber(insetRTxt.getText()) ?
                    Double.parseDouble(insetRTxt.getText()) : model.readDouble(ALModel.RIGHT_INSET);
            double tInsetTxtToDouble = NumberUtils.isNumber(insetTTxt.getText()) ?
                    Double.parseDouble(insetTTxt.getText()) : model.readDouble(ALModel.TOP_INSET);
            double bInsetTxtToDouble = NumberUtils.isNumber(insetBTxt.getText()) ?
                    Double.parseDouble(insetBTxt.getText()) : model.readDouble(ALModel.BOTTOM_INSET);

            meshBuilder.buildInsets(lInsetTxtToDouble, rInsetTxtToDouble, tInsetTxtToDouble, bInsetTxtToDouble);

            model.writeDouble(ALModel.LEFT_INSET, lInsetTxtToDouble);
            model.writeDouble(ALModel.RIGHT_INSET, rInsetTxtToDouble);
            model.writeDouble(ALModel.TOP_INSET, tInsetTxtToDouble);
            model.writeDouble(ALModel.BOTTOM_INSET, bInsetTxtToDouble);
        }
        catch (IllegalArgumentException iae){
            new Alert(Alert.AlertType.ERROR, "Cannot use current insets because: " + iae.getMessage()).showAndWait();
        }
    }

    private double setMeshField(String meshKey, TextField meshField)
    {
        if (!meshField.getText().equals("")){
            model.writeDouble(meshKey, Double.parseDouble(meshField.getText()));
            return Double.valueOf(meshField.getText());
        }
        else{
            meshField.setText(Autoleveller.DF.format(model.readDouble(meshKey)));
            return model.readDouble(meshKey);
        }
    }

    @VisibleForTesting
    boolean isValidField(TextField textField)
    {
        //if textfield value is not a number, no need to continue further
        if (!NumberUtils.isNumber(textField.getText())){return false;}

        List<String> positiveOnly = ImmutableList.of(xLengthTxt.getId(), yLengthTxt.getId(), xyFeedTxt.getId(),
                zFeedTxt.getId(), pClearanceTxt.getId(), pSpacingTxt.getId(), pSafeHeightTxt.getId());
        List<String> negativeOnly = ImmutableList.of(pDepthTxt.getId());

        if (!positiveOnly.contains(textField.getId()) && !negativeOnly.contains(textField.getId())){
            //if textfield number can be either positive or negative the the number is valid
            return true;
        }

        double textFieldNum = Double.parseDouble(textField.getText());

        if (positiveOnly.contains(textField.getId())){
            return textFieldNum >= 0;
        }
        else {//must be in negativeOnly list
            return textFieldNum < 0;
        }
    }

    private void syncGUIWithMesh(Mesh mesh)
    {
        startingXTxt.setText(Autoleveller.DF.format(mesh.getInsetlessSize().get(Mesh.Rect.X).doubleValue()));
        startingYTxt.setText(Autoleveller.DF.format(mesh.getInsetlessSize().get(Mesh.Rect.Y).doubleValue()));
        xLengthTxt.setText(Autoleveller.DF.format(mesh.getInsetlessSize().get(Mesh.Rect.WIDTH).doubleValue()));
        yLengthTxt.setText(Autoleveller.DF.format(mesh.getInsetlessSize().get(Mesh.Rect.HEIGHT).doubleValue()));
        xyFeedTxt.setText(Autoleveller.DF.format(mesh.getXyFeed()));
        zFeedTxt.setText(Autoleveller.DF.format(mesh.getzFeed()));
        pDepthTxt.setText(Autoleveller.DF.format(mesh.getProbeDepth()));
        pClearanceTxt.setText(Autoleveller.DF.format(mesh.getProbeClearance()));
        pSpacingTxt.setText(Autoleveller.DF.format(mesh.getPointSpacing()));
        pSafeHeightTxt.setText(Autoleveller.DF.format(mesh.getSafeHeight()));
        pntsTtlTxt.setText(Autoleveller.DF.format(mesh.getColCount() * mesh.getRowCount()));
        colsTxt.setText(Autoleveller.DF.format(mesh.getColCount()));
        rowsTxt.setText(Autoleveller.DF.format(mesh.getRowCount()));

        Optional<Mesh> meshOpt = Optional.ofNullable(model.getMesh());
        insetLTxt.setDisable(!meshOpt.isPresent());
        insetRTxt.setDisable(!meshOpt.isPresent());
        insetTTxt.setDisable(!meshOpt.isPresent());
        insetBTxt.setDisable(!meshOpt.isPresent());

        insetLTxt.setText(Autoleveller.DF.format(mesh.getInset(Mesh.Inset.LEFT)));
        insetRTxt.setText(Autoleveller.DF.format(mesh.getInset(Mesh.Inset.RIGHT)));
        insetTTxt.setText(Autoleveller.DF.format(mesh.getInset(Mesh.Inset.TOP)));
        insetBTxt.setText(Autoleveller.DF.format(mesh.getInset(Mesh.Inset.BOTTOM)));
    }

    private void setMeshPromptsForUnits(Units units)
    {
        DefaultMeshValues dmv = units.equals(Units.MM) ? DefaultMeshValues.MM : DefaultMeshValues.INCHES;

        xyFeedTxt.setPromptText(Autoleveller.DF.format(dmv.getXYFeed()));
        zFeedTxt.setPromptText(Autoleveller.DF.format(dmv.getZFeed()));
        pDepthTxt.setPromptText(Autoleveller.DF.format(dmv.getProbeDepth()));
        pClearanceTxt.setPromptText(Autoleveller.DF.format(dmv.getProbeClearance()));
        pSpacingTxt.setPromptText(Autoleveller.DF.format(dmv.getPointSpacing()));
        pSafeHeightTxt.setPromptText(Autoleveller.DF.format(dmv.getSafeHeight()));
    }

    @FXML
    private void callChooseOGF()
    {
        commonController.chooseOGF();
    }

    @FXML
    private void clearOGF(){model.setOGF(null);}
}
