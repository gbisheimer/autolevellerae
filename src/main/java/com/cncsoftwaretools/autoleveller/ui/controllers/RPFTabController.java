/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.Autoleveller;
import com.cncsoftwaretools.autoleveller.RPF;
import com.cncsoftwaretools.autoleveller.Units;
import com.cncsoftwaretools.autoleveller.guice.factories.RPF3DModelProvider;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.cncsoftwaretools.autoleveller.ui.MouseEventSetter;
import com.cncsoftwaretools.autoleveller.ui.drawables.Axis3dModelGroup;
import com.cncsoftwaretools.autoleveller.ui.drawables.FocusedTransformableCamera;
import com.cncsoftwaretools.autoleveller.ui.drawables.ImageCanvas2D;
import com.cncsoftwaretools.autoleveller.ui.drawables.RPFMesh3DContainer;
import com.cncsoftwaretools.autoleveller.util.StringDouble3DPConverter;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import java.net.URL;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.stream.Collectors.toCollection;

/**
 * Controller for the RPF tab. Draws a representation of an RPF and displays statistics
 * Created by James Hawthorne on 20/04/2016.
 */
public class RPFTabController implements Initializable
{
    @FXML private StackPane drawPnl;
    @FXML private Slider ampZSld;
    @FXML private TextField ampZTxt;
    @FXML private Label rpfnameLbl;
    @FXML private TableView<Point3DView> pointsTbl;
    @FXML private TableColumn<Point3DView, String> xCol;
    @FXML private TableColumn<Point3DView, String> yCol;
    @FXML private TableColumn<Point3DView, String> zCol;

    @FXML private TextField allPointTxt;
    @FXML private TextField rowPointTxt;
    @FXML private TextField colPointTxt;
    @FXML private TextField hPointXTxt;
    @FXML private TextField hPointYTxt;
    @FXML private TextField hPointZTxt;
    @FXML private TextField lPointXTxt;
    @FXML private TextField lPointYTxt;
    @FXML private TextField lPointZTxt;
    @FXML private TextField averageZTxt;
    @FXML private TextField zDiffTxt;
    @FXML private TextField rpfUnitsTxt;
    @FXML private Spinner<Double> hiThreshSpn;
    @FXML private Spinner<Double> loThreshSpn;

    private final DecimalFormat df = Autoleveller.DF;

    private final ALModel model = ALModel.getInstance();
    private static final Optional<Logger> LOGGER = Autoleveller.getLogger(RPFTabController.class.getName());
    private final RPF3DModelProvider rpfModelProvider;
    private final SpinnerValueFactory.DoubleSpinnerValueFactory hiMMSpinnerFactory =
            new SpinnerValueFactory.DoubleSpinnerValueFactory(-3, 3, 0.2, 0.05);
    private final SpinnerValueFactory.DoubleSpinnerValueFactory loMMSpinnerFactory =
            new SpinnerValueFactory.DoubleSpinnerValueFactory(-3, 3, -0.2, 0.05);
    private final SpinnerValueFactory.DoubleSpinnerValueFactory hiInchSpinnerFactory =
            new SpinnerValueFactory.DoubleSpinnerValueFactory(Units.mmToInches(hiMMSpinnerFactory.getMin()),
                    Units.mmToInches(hiMMSpinnerFactory.getMax()), Units.mmToInches(hiMMSpinnerFactory.getValue()),
                    Units.mmToInches(hiMMSpinnerFactory.getAmountToStepBy()));
    private final SpinnerValueFactory.DoubleSpinnerValueFactory loInchSpinnerFactory =
            new SpinnerValueFactory.DoubleSpinnerValueFactory(Units.mmToInches(loMMSpinnerFactory.getMin()),
                    Units.mmToInches(loMMSpinnerFactory.getMax()), Units.mmToInches(loMMSpinnerFactory.getValue()),
                    Units.mmToInches(loMMSpinnerFactory.getAmountToStepBy()));

    private Consumer<Integer> amplifyFunc = integer -> {};

    @Inject
    private RPFTabController(RPF3DModelProvider rpfModelProvider)
    {
        this.rpfModelProvider = rpfModelProvider;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        xCol.prefWidthProperty().bind(pointsTbl.widthProperty().divide(3));
        yCol.prefWidthProperty().bind(pointsTbl.widthProperty().divide(3));
        zCol.prefWidthProperty().bind(pointsTbl.widthProperty().divide(3));
        xCol.setCellValueFactory(new PropertyValueFactory<>("x"));
        yCol.setCellValueFactory(new PropertyValueFactory<>("y"));
        zCol.setCellValueFactory(new PropertyValueFactory<>("z"));
        setThresholdSpinners(model.readUnits());
        hiThreshSpn.valueProperty().addListener(observable -> threshListener());
        loThreshSpn.valueProperty().addListener(observable -> threshListener());
        model.rpfProperty().addListener(observable -> tabSelected());
        hiThreshSpn.increment();
        hiThreshSpn.decrement();
        loThreshSpn.increment();
        loThreshSpn.decrement();
    }

    private void threshListener(){Optional.ofNullable(model.getRPF()).ifPresent(this::reDrawScene);}

    private void rpfDataDisplay(RPF rpf)
    {
        Point3D highPoint = rpf.highestPoint();
        Point3D lowPoint = rpf.lowestPoint();
        double averageZ = rpf.fetchAverageZ();
        double zDiff = rpf.fetchHiLowZDiff();

        rpfnameLbl.setText(Paths.get(rpf.getFileName()).toAbsolutePath().toString());
        allPointTxt.setText(String.valueOf(rpf.getPointsInFileOrder().size()));
        rowPointTxt.setText(String.valueOf(rpf.getRowCount()));
        colPointTxt.setText(String.valueOf(rpf.getColCount()));
        hPointXTxt.setText(df.format(highPoint.getX()));
        hPointYTxt.setText(df.format(highPoint.getY()));
        hPointZTxt.setText(df.format(highPoint.getZ()));
        lPointXTxt.setText(df.format(lowPoint.getX()));
        lPointYTxt.setText(df.format(lowPoint.getY()));
        lPointZTxt.setText(df.format(lowPoint.getZ()));
        averageZTxt.setText(df.format(averageZ));
        zDiffTxt.setText(df.format(zDiff));
        rpfUnitsTxt.setText(rpf.units.toString());

        pointsTbl.setItems(rpf.getPointsInFileOrder().stream()
            .map(Point3DView::new)
            .collect(toCollection(FXCollections::observableArrayList)));
        pointsTbl.getColumns().setAll(ImmutableList.of(xCol, yCol, zCol));
    }

    private void setThresholdSpinners(Units units)
    {
        if (units == Units.MM){
            hiThreshSpn.setValueFactory(hiMMSpinnerFactory);
            loThreshSpn.setValueFactory(loMMSpinnerFactory);
        }
        else{
            hiThreshSpn.setValueFactory(hiInchSpinnerFactory);
            loThreshSpn.setValueFactory(loInchSpinnerFactory);
        }
        hiThreshSpn.getValueFactory().setConverter(new StringDouble3DPConverter());
        loThreshSpn.getValueFactory().setConverter(new StringDouble3DPConverter());
    }

    @FXML
    private void amplifyZ(int ampValue){ amplifyFunc.accept(ampValue);}

    void tabSelected()
    {
        drawPnl.getChildren().clear();
        pointsTbl.getColumns().clear();
        rpfnameLbl.setText("");
        hPointXTxt.setText("");
        hPointYTxt.setText("");
        hPointZTxt.setText("");
        lPointXTxt.setText("");
        lPointYTxt.setText("");
        lPointZTxt.setText("");
        averageZTxt.setText("");
        zDiffTxt.setText("");

        if (!Optional.ofNullable(model.getRPF()).isPresent()){
            drawPnl.setAlignment(Pos.CENTER);
            drawPnl.getChildren().add(new Label("No RPF set or RPF is invalid"));
            LOGGER.ifPresent(logger -> logger.log(Level.INFO, "No RPF set or RPF is invalid"));
            return;
        }

        Optional.ofNullable(model.getRPF()).ifPresent(rpf -> {
            rpfDataDisplay(rpf);
            reDrawScene(rpf);
        });
    }

    private void reDrawScene(RPF rpf)
    {
        rpf.createExpandedRPF(200).ifPresent(expandedRPF -> {
            Node graphicalRPFDisplay = Platform.isSupported(ConditionalFeature.SCENE3D) ?
                    create3DSceneFrom(expandedRPF) : create2DSceneFrom(expandedRPF);

            drawPnl.getChildren().add(graphicalRPFDisplay);
            setThresholdSpinners(rpf.units);
        });
    }

    private Node create2DSceneFrom(RPF rpf)
    {
        ampZSld.setDisable(true);
        ampZTxt.setDisable(true);
        ImageCanvas2D canvas = new ImageCanvas2D(createHeightMap(rpf));
        Pane wrapper = new Pane(canvas); //need canvas wrapper pane because bindings do not update with the plain draw panel
        canvas.widthProperty().bind(wrapper.widthProperty());
        canvas.heightProperty().bind(wrapper.heightProperty());
        canvas.draw();

        return wrapper;
    }

    private Image createHeightMap(RPF rpf)
    {
        return rpf.createHeightMapImage(Color.RED, loThreshSpn.getValue(), Color.GREEN, hiThreshSpn.getValue());
    }

    private Node create3DSceneFrom(RPF rpf)
    {
        ampZSld.setDisable(false);
        ampZTxt.setDisable(false);
        ampZSld.valueProperty().addListener((observable, oldValue, newValue) -> {
            int intValue = newValue.intValue();
            ampZTxt.textProperty().setValue(String.valueOf(intValue));
            amplifyZ(intValue);
        });

        //force the text box to display initial value
        ampZSld.increment();
        ampZSld.decrement();
        final Axis3dModelGroup axis3D = rpfModelProvider.createAxisGroup(rpf.units);
        final RPFMesh3DContainer rpfMesh3DContainer = rpfModelProvider.createMeshView(rpf);
        rpfMesh3DContainer.materialSetup(createHeightMap(rpf));
        final FocusedTransformableCamera rpfCamera = rpfModelProvider.createCamera(rpf.theBoundingBox);
        final MouseEventSetter mouseHandler = rpfModelProvider.createMouseHandler(rpfCamera);
        amplifyFunc = rpfMesh3DContainer::amplifyZ;

        Group group = new Group(axis3D.theAxisGroup, rpfMesh3DContainer.theMeshView, rpfCamera.theCamera);
        SubScene subScene = new SubScene(group, 640, 480, true, SceneAntialiasing.BALANCED);
        subScene.setCamera(rpfCamera.theCamera);
        subScene.setFill(Color.WHITE);
        subScene.heightProperty().bind(drawPnl.heightProperty());
        subScene.widthProperty().bind(drawPnl.widthProperty());
        subScene.setManaged(false);

        mouseHandler.setMouseEvents(subScene);

        return subScene;
    }

    /**
     * This class is purely a bunch of getters and setter for the X, Y and Z
     * of a Point3d made for displaying in a javaFX Table.
     * Also formats the X, Y and Z so we dont need a custom cell renderer
     */
    public class Point3DView
    {
        private double x;
        private double y;
        private double z;

        Point3DView(Point3D point3D)
        {
            x = point3D.getX();
            y = point3D.getY();
            z = point3D.getZ();
        }

        public String getX()
        {
            return df.format(x);
        }

        public String getY()
        {
            return df.format(y);
        }

        public String getZ()
        {
            return df.format(z);
        }

        public void setX(String x)
        {
            this.x = Double.parseDouble(x);
        }

        public void setY(String y)
        {
            this.y = Double.parseDouble(y);
        }

        public void setZ(String z)
        {
            this.z = Double.parseDouble(z);
        }
    }
}
