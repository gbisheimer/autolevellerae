/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.*;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.io.Files;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Optional.ofNullable;

/**
 * Controller (MVC) for the main GUI
 * The parent controller for the tabs
 * Created by James Hawthorne on 16/06/2015.
 */
public class ALGUIController implements Initializable
{
    @FXML private TabPane tabPane;
    @FXML private Tab meshTab;
    @FXML private Tab rpfTab;
    @SuppressWarnings("unused")
    private CommonFXML commonController = new CommonFXML();
    @FXML private Label jobLbl;
    @FXML private Label meshLbl;
    @FXML private Label optionsLbl;
    private final InitZDialog zInitDialog = InitZDialog.newInstance();
    private final MSLDialog mslDialog = MSLDialog.newInstance();
    private final CustomControllerDialog cstCntDialog = CustomControllerDialog.newInstance();
    private final RPFFixer rpfFixer = RPFFixer.newInstance();
    private ALModel model = ALModel.getInstance();

    @VisibleForTesting
    @FXML MeshTabController meshContentController;

    private static final Logger LOGGER = Autoleveller.getLogger(
            ALGUIController.class.getName()).orElse(Logger.getAnonymousLogger());

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        versionCheckAndAlert();
        model.ogfProperty().addListener((observable, oldValue, newValue) ->
                Platform.runLater(() -> {
                    Autoleveller.setTitle(ofNullable(newValue).isPresent() ? " - " + newValue.getFilename() : "");
                    jobLbl.setText(("Job/OGF Area: ").concat(ofNullable(newValue).isPresent() ?
                    newValue.displayDetails() : " Not set"));
        }));
        model.meshProperty().addListener((observable, oldValue, newValue) -> Platform.runLater(() ->
                meshLbl.setText(("Mesh/Probe Area: ").concat(ofNullable(newValue).isPresent() ? newValue.displayDetails() : " Not set"))));
        model.rpfProperty().addListener((observable, oldValue, newValue) -> setTabSelection(newValue));
        model.addChangeListener(modelProperty -> displayOptions());
        displayOptions();
        setTabSelection(null);
        Autoleveller.changeSize(model.readDouble(ALModel.WIDTH), model.readDouble(ALModel.HEIGHT));
    }

    private void displayOptions()
    {
        optionsLbl.setText("Level Options: " + "Maximum Segment Length=" + model.readDouble(ALModel.MAX_SEG_LTH) +
                ", Init Block=" + (model.readUseLvlZInit() ? "Custom" : "Default") +
                ", PFG Init Block=" + (model.readUsePFGZInit() ? "Custom" : "Default"));
    }

    void setTabSelection(RPF rpf)
    {
        if (ofNullable(rpf).isPresent()){
            rpfTab.setDisable(false);
            tabPane.getSelectionModel().select(rpfTab);
            meshTab.setDisable(true);
        }
        else{
            rpfTab.setDisable(true);
            tabPane.getSelectionModel().select(meshTab);
            meshTab.setDisable(false);
        }
    }

    private void versionCheckAndAlert()
    {
        Platform.runLater(() -> {
            try {
                Document xmlDoc = getExternalDoc();
                Version thisVersion = getInternalVersion();
                Version latestVersion = xmlToVersion(xmlDoc, "01");
                alertGreaterAvailable(latestVersion, thisVersion).ifPresent(Dialog::showAndWait);
            }
            catch (Exception e){e.printStackTrace();}
        });
    }

    private Optional<Dialog> alertGreaterAvailable(Version latestVersion, Version thisVersion)
    {
        if (latestVersion.compareVersions(thisVersion) < 1)
            return Optional.empty();

        FlowPane pane = new FlowPane();
        Label contentStr = new Label("This version is: " + thisVersion + "\nLatest version is: " + latestVersion +
                "\n\nThe latest versions of Autoleveller and AutolevellerAE are always available to members at:\n");
        Hyperlink link = new Hyperlink("http://www.autoleveller.co.uk/member-downloads/");
        link.setOnAction(event -> model.getHostServices().showDocument(link.getText()));
        pane.getChildren().addAll(contentStr, link);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Version Check");
        alert.setHeaderText("New Version Available");
        alert.getDialogPane().contentProperty().setValue(pane);

        return Optional.of(alert);
    }

    private Version getInternalVersion()
    {
        String[] verWUpdate = Autoleveller.version.split("u");
        String[] majorMinorStr = verWUpdate[0].split("\\.", 2);
        int thisMajor = Integer.parseInt(majorMinorStr[0]);
        double thisMinor = Double.parseDouble(majorMinorStr[1]);
        int upNum = Integer.parseInt(verWUpdate[1]);

        return UpdateVer.newInstance(Autoleveller.name, thisMajor, thisMinor, upNum);
    }

    private Document getExternalDoc() throws IOException, ParserConfigurationException, SAXException
    {
        URL webaddress = new URL("http://autoleveller.co.uk/Version.xml");

        URLConnection con = webaddress.openConnection();
        con.setConnectTimeout(5000); //5 second timeout
        con.setReadTimeout(10000); //10 second timeout
        InputStream stream = con.getInputStream();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document xmlDoc = dBuilder.parse(stream);
        stream.close();
        return xmlDoc;
    }

    @FXML
    public void showAbout()
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("AutoLevellerAE");
        String changeSet = Autoleveller.changeSet;
        LocalDate buildD = Autoleveller.buildDate;
        DateTimeFormatter df = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);

        FlowPane pane = new FlowPane();
        Label text = new Label("AutoLevellerAE - daedelus@autoleveller.co.uk\n" +
                "Version: " + Autoleveller.version + " Changeset: " + changeSet.substring(0, 7) +
                " Date: " + df.format(buildD) + "\n" +
                "License - GPLv2 Copyright 2013-" + LocalDate.now().getYear() + " James Hawthorne PhD\n");
        Hyperlink link = new Hyperlink("www.autoleveller.co.uk");
        link.setOnAction(event -> model.getHostServices().showDocument(link.getText()));
        pane.getChildren().addAll(text, link);
        alert.getDialogPane().contentProperty().set(pane);
        alert.showAndWait();
    }

    @FXML
    public void menuClose()
    {
        System.exit(0);
    }

    @FXML
    public void clickAutolevel()
    {
        //check if there is an OGF in the model and also contains either an RPF or Mesh
        boolean writingRequirementsMet = Optional.ofNullable(model.getOGF()).isPresent() &&
            (Optional.ofNullable(model.getRPF()).isPresent() || Optional.ofNullable(model.getMesh()).isPresent());

        if (writingRequirementsMet){
            evalValidALClick(Optional.ofNullable(model.getRPF()).isPresent() ?
                    model.getRPF() : model.getMesh());
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Set an OGF and either an RPF or Mesh before levelling");
            alert.showAndWait();
        }
    }

    private void evalValidALClick(LevellerWriter levellerWriter)
    {
        String defaultPath = model.readDirectory();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(defaultPath));
        fc.setTitle("Save Autolevelled file");
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(model.readController().getDefaultExt() + " Files",
                "*." + model.readController().getDefaultExt()),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        String initialFilename = Files.getNameWithoutExtension("AL".concat(new File(model.getOGF().getFilename()).getName()));
        initialFilename = initialFilename.concat(".").concat(model.readController().getDefaultExt());

        fc.setInitialFileName(initialFilename);

        Optional<File> fileOptional = ofNullable(fc.showSaveDialog(null));
        fileOptional.ifPresent(file -> {
            model.writeDirectory(file.getParent());
            levellerWriter.writeLevelled(file.getAbsolutePath(), model);
        });
    }

    @FXML
    public void clickPFG()
    {
        Optional<Mesh> mesh = ofNullable(model.getMesh());

        if (!mesh.isPresent()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "To generate a PFG file, you first need to generate a probe Mesh");
            alert.setHeaderText("Information");
            alert.showAndWait();
        }

        String defaultPath = model.readDirectory();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(defaultPath));
        fc.setTitle("Save PFG file");
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(model.readController().getDefaultExt() + " Files",
                "*" + model.readController().getDefaultExt()),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        int pfgWidth = (int)mesh.get().getSize().getWidth();
        int pfgHeight = (int)mesh.get().getSize().getHeight();
        String initialFilename = "PFG~".concat(pfgWidth + "x" + pfgHeight).concat(".").concat(model.readController().getDefaultExt());;
        fc.setInitialFileName(initialFilename);

        Optional<File> fileOptional = ofNullable(fc.showSaveDialog(null));
        fileOptional.ifPresent(file -> {
            model.writeDirectory(file.getParent());
            PFGWriter pfgWriter = mesh.get();
            try {
                ALWriter.useALWriter(new PrintWriter(new BufferedWriter(new FileWriter(file, false))), pfgWriter.writePFG());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "No PFG written. Failed to create a FileWriter from: " + file.getAbsolutePath());
            }
        });
    }

    /**
     * Transforms an XML document into a Version object for comparing
     * Must be of the form:
     * <APPLICATIONS>
     *      <APP id="00">
     *          <NAME>Autoleveller</NAME>
     *          <MAJOR>0</MAJOR>
     *          <MINOR>8.7</MINOR>
     *          <UPDATE>1</UPDATE>
     *      </APP>
     * </APPLICATIONS>
     * @param xmlDoc The w3c XML Document to search
     * @param appElementID The id of the app containing the version information
     * @return A Version created from the XML document or a Version which is zero;
     */
    @VisibleForTesting
    static Version xmlToVersion(Document xmlDoc, String appElementID)
    {
        try {
            NodeList appNodes = xmlDoc.getElementsByTagName("APP");

            Element appNode = IntStream.range(0, appNodes.getLength())
                    .mapToObj(appNodes::item)
                    .filter(node -> node.getAttributes().getNamedItem("id").getNodeValue().equals(appElementID))
                    .filter(node -> node.getNodeType() == Node.ELEMENT_NODE)
                    .map(node -> (Element) node)
                    .findFirst().orElseThrow(NoSuchElementException::new);

            String appName = appNode.getElementsByTagName("NAME").item(0).getTextContent();
            int major = Integer.parseInt(appNode.getElementsByTagName("MAJOR").item(0).getTextContent());
            double minor = Double.parseDouble(appNode.getElementsByTagName("MINOR").item(0).getTextContent());
            int update = Integer.parseInt(appNode.getElementsByTagName("UPDATE").item(0).getTextContent());

            return UpdateVer.newInstance(appName, major, minor, update);
        } catch (Exception e) {
            return UpdateVer.IDENTITY;
        }
    }

    @FXML
    private void showSegLengthDlg()
    {
        Optional<Double> msl = mslDialog.popupAndWait(model.readDouble(ALModel.MAX_SEG_LTH), model.readUnits());
        msl.ifPresent(aDouble -> model.writeDouble(ALModel.MAX_SEG_LTH, aDouble));
        displayOptions();
    }

    @FXML
    private void showZInitDialog()
    {
        zInitDialog.popupAndWait();
        displayOptions();
    }

    @FXML
    private void showRPFFixer()
    {
        rpfFixer.popupAndWait();
    }

    @FXML
    private void changeRes(ActionEvent mnuEvt)
    {
        MenuItem source = (MenuItem)mnuEvt.getSource();
        Pair<Double, Double> res;

        if (source.getId().equals("res1280x800Itm")){
            res = Pair.createPair(1280.0, 760.0);
        }
        else if (source.getId().equals("res1600x1200Itm")){
            res = Pair.createPair(1600.0, 1160.0);
        }
        else{
            res = Pair.createPair(1024.0, 730.0);
        }

        Autoleveller.changeSize(res.getFirst(), res.getSecond());
        model.writeDouble(ALModel.WIDTH, res.getFirst());
        model.writeDouble(ALModel.HEIGHT, res.getSecond());
    }

    @FXML
    private void showCstCntDlg()
    {
        cstCntDialog.popupAndWait();
    }

    @FXML
    private void chooseOGF(){commonController.chooseOGF();}

    @FXML
    private void chooseRPF(){commonController.chooseRPF();}

    @FXML
    private void clearOGF(){model.setOGF(null);}

    @FXML
    private void clearRPF(){model.setRPF(null);}
}
