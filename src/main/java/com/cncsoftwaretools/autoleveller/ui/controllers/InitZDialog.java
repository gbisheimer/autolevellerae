/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.ALTextBlockFunctions;
import com.cncsoftwaretools.autoleveller.Mesh;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by James Hawthorne on 26/03/2017.
 *
 */
public class InitZDialog extends Stage implements Initializable
{
    @FXML private TextArea initTxtArea;
    @FXML private TextArea pfgInitTxtArea;
    @FXML private RadioButton pfgCtmRdo;
    @FXML private RadioButton pfgDftRdo;
    @FXML private RadioButton lvlCtmRdo;
    @FXML private RadioButton lvlDftRdo;

    private ALModel model = ALModel.getInstance();
    private String defaultInit;

    private InitZDialog()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ALGUI/ZInitDlg.fxml"));
        fxmlLoader.setController(this);

        try {
            setScene(new Scene(fxmlLoader.load()));
            Stage stage = (Stage) getScene().getWindow();
            stage.getIcons().add(new Image("alicon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static InitZDialog newInstance()
    {
        return new InitZDialog();
    }

    Optional<Boolean> popupAndWait()
    {
        defaultInit = model.getMesh() != null ?
                ALTextBlockFunctions.zInitGCode(model.getMesh()).stream().collect(Collectors.joining("\n")) :
                "*No Mesh Set*";

        pfgSelector();
        lvlSelector();
        showAndWait();

        return Optional.ofNullable(true);
    }

    private void pfgSelector()
    {
        if (model.readUsePFGZInit()){
            pfgCtmRdo.setSelected(true);
            onPFGCustomClicked();
        } else {
            pfgDftRdo.setSelected(true);
            onPFGDefaultClicked();
        }
    }

    private void lvlSelector()
    {
        if (model.readUseLvlZInit()){
            lvlCtmRdo.setSelected(true);
            onLvlCustomClicked();
        } else {
            lvlDftRdo.setSelected(true);
            onLvlDefaultClicked();
        }
    }

    @FXML
    private void onPFGDefaultClicked()
    {
        pfgInitTxtArea.setEditable(false);
        pfgInitTxtArea.setText(defaultInit);
        model.writeUsePFGZInit(false);
    }

    @FXML
    private void onLvlDefaultClicked()
    {
        initTxtArea.setEditable(false);
        initTxtArea.setText(defaultInit);
        model.writeUseLvlZInit(false);
    }

    @FXML
    private void onPFGCustomClicked()
    {
        pfgInitTxtArea.setEditable(true);
        pfgInitTxtArea.setText(model.readString(ALModel.CST_PFG_INIT_BLOCK));
        model.writeUsePFGZInit(true);
    }

    @FXML
    private void onLvlCustomClicked()
    {
        initTxtArea.setEditable(true);
        initTxtArea.setText(model.readString(ALModel.CST_LVL_INIT_BLOCK));
        model.writeUseLvlZInit(true);
    }

    @FXML
    private void onCancel()
    {
        close();
    }

    @FXML
    private void onOK()
    {
        onApply();
        close();
    }

    @FXML
    private void onApply()
    {
        if (pfgCtmRdo.isSelected()) {
            model.writeString(ALModel.CST_PFG_INIT_BLOCK, pfgInitTxtArea.getText());
        }
        if (lvlCtmRdo.isSelected()) {
            model.writeString(ALModel.CST_LVL_INIT_BLOCK, initTxtArea.getText());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        setResizable(false);
        setTitle("Z Initialization");
    }
}
