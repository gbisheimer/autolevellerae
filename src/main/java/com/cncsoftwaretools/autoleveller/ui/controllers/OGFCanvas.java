/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.Drawing;
import com.cncsoftwaretools.autoleveller.OGF;
import com.cncsoftwaretools.autoleveller.TriConsumer;
import com.cncsoftwaretools.autoleveller.reader.GCodeReader;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.reader.Word;
import com.cncsoftwaretools.autoleveller.reader.WordGroup;
import com.cncsoftwaretools.autoleveller.reader.processor.GCodeProcessor;
import com.cncsoftwaretools.autoleveller.reader.processor.ReplacerPreProcessor;
import com.cncsoftwaretools.autoleveller.util.Arc;
import com.cncsoftwaretools.autoleveller.util.AutolevellerUtil;
import com.cncsoftwaretools.autoleveller.util.GraphicsContextWrapper;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

/**
 * Created by James Hawthorne on 11/06/2017.
 *
 */
public class OGFCanvas implements Drawing
{
    private final Rectangle originalSize;
    private final Canvas canvas;
    private final List<TriConsumer<GraphicsContextWrapper, Double, Point2D>> drawingInstructions;

    OGFCanvas(OGF ogf, GCodeReader reader, Canvas canvas)
    {
        this.canvas = canvas;
        originalSize = new Rectangle(ogf.getSize().getX(), ogf.getSize().getY(), ogf.getSize().getWidth(), ogf.getSize().getHeight());
        List<TriConsumer<GraphicsContextWrapper, Double, Point2D>> di = ImmutableList.of();
        OGFProcessor cp = new OGFProcessor();
        try {
            reader.readFile(cp);
            di = cp.getDrawingInstructions();
        } catch (IOException e) {
            e.printStackTrace();
        }

        drawingInstructions = di;
    }

    @Override
    public Rectangle originalSize()
    {
        return originalSize;
    }

    @Override
    public List<TriConsumer<GraphicsContextWrapper, Double, Point2D>> getDrawingInstructions()
    {
        return drawingInstructions;
    }

    private class OGFProcessor implements GCodeProcessor
    {
        private ImmutableList.Builder<TriConsumer<GraphicsContextWrapper, Double, Point2D>> drawingInstructions = ImmutableList.builder();
        private List<Line3D> lines = new ArrayList<>();
        private List<Arc> arcs = new ArrayList<>();

        @Override
        public void interpret(List<GCodeState> states)
        {
            states.forEach(this::processState);
        }

        @Override
        public List<GCodeState> processState(GCodeState gCodeState)
        {
            stateToShape(gCodeState);
            return ImmutableList.of();
        }

        private void stateToShape(GCodeState gCodeState)
        {
            Optional<Word> motionWord = gCodeState.getModalWordByGroup(WordGroup.MOTIONGROUP);
            motionWord.ifPresent(word -> {
                double wordValue = word.getWordPair().getSecond().doubleValue();
                if (wordValue == 0 || wordValue == 1){
                    arcsToInstructions();

                    addLine(gCodeState);
                }
                else if (wordValue == 2 || wordValue == 3){
                    linesToInstructions();

                    addArc(gCodeState);
                }
                else if (wordValue >= 81 && wordValue <= 89){
                    drillCycleToInstruction(gCodeState);
                }
                else {
                    linesToInstructions();
                    arcsToInstructions();
                }
            });
        }

        private void addArc(GCodeState gCodeState)
        {
            Arc.createArcFromState(gCodeState).ifPresent(arc -> {
                //make all arcs CCW for drawing purposes
                arcs.add(arc.getDirection().equals(Arc.Direction.CW) ?
                        Arc.newInstance(arc.getEndPoint(), arc.getStartPoint(), arc.getCentrePoint(), Arc.Direction.CCW) : arc);
            });
        }

        private void addLine(GCodeState gCodeState)
        {
            gCodeState.getReal3DPoint().ifPresent(currentPoint ->
                    gCodeState.getPreviousState().ifPresent(previousState ->
                            previousState.getReal3DPoint().ifPresent(prevPoint -> {
                                // a line needs to be mirrored so the image is reversed
                                lines.add(new Line3D(prevPoint.getX(), -prevPoint.getY(), prevPoint.getZ(),
                                        currentPoint.getX(), -currentPoint.getY(), currentPoint.getZ()));
                            })));
        }

        private void arcsToInstructions()
        {
            if (!arcs.isEmpty()){
                arcs.forEach(arc -> {
                    double r = arc.getRadius().doubleValue();
                    double d = r * 2;
                    double startAngle = Math.toDegrees(Arc.angleAtPoint(arc.getStartPoint(), arc.getCentrePoint()).doubleValue());
                    double extentAngle = Math.toDegrees(arc.angleBetweenStartAndGivenPoint(arc.getEndPoint()).doubleValue());
                    // the centre point is relative to the start and end points, not an absolute value?
                    double tlX = (arc.getCentrePoint().getX() - r);
                    double tlY = ((originalSize.getY() - arc.getCentrePoint().getY()) - r);

                    drawingInstructions.add((gcw, ratio, offset) -> {
                        gcw.setStroke(Color.GREEN);
                        gcw.strokeArc((tlX + offset.getX()) * ratio - originalSize.getX() * ratio,
                                (tlY - offset.getY())* ratio + canvas.getHeight(), d * ratio, d * ratio, startAngle, extentAngle, ArcType.OPEN);
                    });
                });

                arcs.clear();
            }
        }

        private void drillCycleToInstruction(GCodeState gCodeState)
        {
            gCodeState.get2DPoint().ifPresent(point2D -> {
                double radius = 2;

                drawingInstructions.add((gcw, ratio, offset) -> {
                    gcw.setStroke(Color.BLACK);
                    double cx = point2D.getX() * ratio - ((originalSize.getX() - offset.getX()) * ratio);
                    double cy = (-point2D.getY() * ratio) + (canvas.getHeight() + ((originalSize.getY() - offset.getY()) * ratio));

                    gcw.strokeline(cx - radius, cy - radius, cx + radius, cy + radius);
                    gcw.strokeline(cx - radius, cy + radius, cx + radius, cy - radius);
                });
            });
        }

        private void linesToInstructions()
        {
            Predicate<Line3D> zHeightChange = line3D -> (line3D.getStartZ() > 0 && line3D.getEndZ() <= 0) ||
                    (line3D.getStartZ() <= 0 && line3D.getEndZ() > 0);
            List<List<Line3D>> partitionedLines = AutolevellerUtil.partitionList(lines, zHeightChange);

            partitionedLines.forEach(this::subLinesToInstruction);

            lines.clear();
        }

        private void subLinesToInstruction(List<Line3D> lineList)
        {
            final double[] xArray = linesToArray(lineList, Line3D::getEndX, Line3D::getStartX);
            final double[] yArray = linesToArray(lineList, Line3D::getEndY, Line3D::getStartY);
            final int lineCount = xArray.length;
            final Color color = lineList.get(0).getStartZ() <= 0 ? Color.BLUE : Color.BURLYWOOD;
            final double[] dashLengths = lineList.get(0).getStartZ() <= 0 ? null : new double[]{5.0};

            drawingInstructions.add((gcw, ratio, offset) -> {
                double[] newXs = Arrays.stream(xArray).map(xValue -> xValue * ratio - ((originalSize.getX() - offset.getX()) * ratio)).toArray();
                double[] newYs = Arrays.stream(yArray).map(yValue -> yValue * ratio + (canvas.getHeight() + ((originalSize.getY() - offset.getY()) * ratio))).toArray();

                gcw.setStroke(color);
                gcw.setDashLengths(dashLengths);
                gcw.setLineDashOffset(0.1);
                gcw.strokePolyline(newXs, newYs, lineCount);
                gcw.setDashLengths(null);
            });
        }

        private double[] linesToArray(List<Line3D> lineList, Function<Line3D, Double> doubleMapFunc, Function<Line3D, Double> lineElementFunc)
        {
            final List<Double> listArray = lineList.stream().map(doubleMapFunc).collect(toList());
            Optional.ofNullable(lineList.get(0)).ifPresent(line -> listArray.add(0, lineElementFunc.apply(line)));

            return listArray.stream().mapToDouble(Double::doubleValue).toArray();
        }

        @Override
        public void finish()
        {
            linesToInstructions();
            arcsToInstructions();
        }

        List<TriConsumer<GraphicsContextWrapper, Double, Point2D>> getDrawingInstructions()
        {
            return drawingInstructions.build();
        }
    }

    private class Line3D
    {
        private final Line line2D;
        private final double startZ;
        private final double endZ;

        Line3D(double startX, double startY, double startZ,
                      double endX, double endY, double endZ)
        {
            line2D = new Line(startX, startY, endX, endY);
            this.startZ = startZ;
            this.endZ = endZ;
        }

        double getStartX(){return line2D.getStartX();}

        double getEndX(){return line2D.getEndX();}

        double getStartY(){return line2D.getStartY();}

        double getEndY(){return line2D.getEndY();}

        double getStartZ(){return startZ;}

        double getEndZ(){return endZ;}
    }
}
