/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.drawables;

import com.cncsoftwaretools.autoleveller.RPF;
import com.cncsoftwaretools.autoleveller.Units;
import com.cncsoftwaretools.autoleveller.ui.controllers.RPFTabController;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.sun.javafx.collections.ObservableFloatArrayImpl;
import com.sun.javafx.scene.shape.ObservableFaceArrayImpl;
import javafx.collections.ObservableFloatArray;
import javafx.geometry.Point3D;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.Material;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.ObservableFaceArray;
import javafx.scene.shape.TriangleMesh;

import java.util.stream.IntStream;

/*
 *
 * Created by James Hawthorne on 13/09/2016.
 */
public class RPFMesh3DContainer implements ZAmplifier
{
    private final RPF rpf;
    private final ObservableFloatArray originalPoints;
    public final MeshView theMeshView;

    @Inject
    private RPFMesh3DContainer(@Assisted RPF rpf, MeshView meshView, ObservableFloatArray originalPoints)
    {
        this.rpf = rpf;

        meshSetup((TriangleMesh) meshView.getMesh());
        //materialSetup((PhongMaterial) meshView.getMaterial());
        this.theMeshView = meshView;

        originalPoints.setAll(getPointsForThis());
        this.originalPoints = originalPoints;

    }

    private void meshSetup(TriangleMesh rpfMesh)
    {
        rpfMesh.getPoints().setAll(createPointsForRPFMesh());
        rpfMesh.getTexCoords().addAll(createTextureCoordsForRPFMesh());
        rpfMesh.getFaces().addAll(createFacesForRPFMesh());
    }

    public void materialSetup(Image diffuseImage)
    {
        PhongMaterial material = (PhongMaterial) theMeshView.getMaterial();

        material.setDiffuseMap(diffuseImage);
        material.setSpecularColor(Color.WHITE);
    }

    private ObservableFloatArray createPointsForRPFMesh()
    {
        return IntStream.range(0, rpf.getRowCount())
                .mapToObj(row -> IntStream.range(0, rpf.getColCount())
                        .mapToObj(col -> convertPoint3DToFloatArray(rpf.getPointAt(row, col)))
                        .reduce(new ObservableFloatArrayImpl(), this::accumulateFloatArray))
                .reduce(new ObservableFloatArrayImpl(), this::accumulateFloatArray);
    }

    private ObservableFloatArray createTextureCoordsForRPFMesh()
    {
        return IntStream.range(0, rpf.getRowCount()-1).asDoubleStream()
                .mapToObj(row -> IntStream.range(0, rpf.getColCount()-1).asDoubleStream()
                        .mapToObj(col -> createTextureCoordsFor(row, col))
                        .reduce(new ObservableFloatArrayImpl(), this::accumulateFloatArray))
                .reduce(new ObservableFloatArrayImpl(), this::accumulateFloatArray);
    }

    private ObservableFaceArray createFacesForRPFMesh()
    {
        return IntStream.range(0, rpf.getRowCount()-1)
                .mapToObj(row -> IntStream.range(0, rpf.getColCount()-1)
                        .mapToObj(col -> createFacesFor(row, col))
                        .reduce(new ObservableFaceArrayImpl(), this::accumulateFaceArray))
                .reduce(new ObservableFaceArrayImpl(), this::accumulateFaceArray);
    }

    private static ObservableFloatArray convertPoint3DToFloatArray(Point3D point3D)
    {
        final float xValue = Double.valueOf(point3D.getX()).floatValue();
        final float yValue = -(Double.valueOf(point3D.getY()).floatValue());
        final float zValue = -(Double.valueOf(point3D.getZ()).floatValue());

        return new ObservableFloatArrayImpl(xValue, yValue, zValue);
    }

    private ObservableFloatArray createTextureCoordsFor(double row, double col)
    {
        final int rowsInMesh = rpf.getRowCount();
        final int pointsPerRow = rpf.getColCount();

        final float ul = (float)(col / pointsPerRow);
        final float ur = (float)((col + 1) / pointsPerRow);
        final float vb = (float)((rowsInMesh - row) / rowsInMesh);
        final float vt = (float)(((rowsInMesh - row) - 1) / rowsInMesh);

        return new ObservableFloatArrayImpl(
                ul, vt,   //tl 0
                ur, vt,   //tr 1
                ul, vb,   //bl 2
                ur, vb    //br 3
        );
    }

    private ObservableFaceArray createFacesFor(int row, int col)
    {
        final int pointsPerRow = rpf.getColCount();

        int tlPoint = (row + 1) * pointsPerRow + col;
        int trPoint = (row + 1) * pointsPerRow + col + 1;
        int blPoint = row * pointsPerRow + col;
        int brPoint = row * pointsPerRow + col + 1;

        int tOffset = (row * (pointsPerRow - 1) + col) * 4;
        ObservableFaceArray faceArray = new ObservableFaceArrayImpl();

        faceArray.addAll(tlPoint, tOffset, blPoint, tOffset + 2, brPoint, tOffset + 3);
        faceArray.addAll(trPoint, tOffset + 1, tlPoint, tOffset, brPoint, tOffset + 3);

        return faceArray;
    }

    private ObservableFloatArray accumulateFloatArray(ObservableFloatArray accumulator, ObservableFloatArray elements)
    {
        accumulator.addAll(elements);
        return accumulator;
    }

    private ObservableFaceArray accumulateFaceArray(ObservableFaceArray accumulator, ObservableFaceArray elements)
    {
        accumulator.addAll(elements);
        return accumulator;
    }

    @Override
    public void amplifyZ(int ampValue)
    {
        final ObservableFloatArray thesePoints = getPointsForThis();

        IntStream.iterate(2, i -> i+3)
                .limit(originalPoints.size() / 3) //no. of iterations not value of i
                .forEachOrdered(i -> thesePoints.set(i, originalPoints.get(i) * ampValue));
    }

    private ObservableFloatArray getPointsForThis()
    {
        return ((TriangleMesh) theMeshView.getMesh()).getPoints();
    }
}
