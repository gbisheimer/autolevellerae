/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.drawables;

import com.cncsoftwaretools.autoleveller.Pair;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.function.Function;

/**
 *
 * Created by James Hawthorne on 04/10/2016.
 */
public class ImageCanvas2D extends Canvas
{
    private final Image image;
    private final GraphicsContext gc;
    private final double ratio;

    private final Function<Pair<Double, Double>, Double> proportionalWidth;
    private final Function<Pair<Double, Double>, Double> proportionalHeight;

    public ImageCanvas2D(Image image)
    {
        this.image = image;
        this.gc = getGraphicsContext2D();
        this.ratio = image.getWidth() / image.getHeight();
        proportionalWidth = (widthHeight) -> ratio >= 0 ? widthHeight.getFirst() :  widthHeight.getSecond() / ratio;
        proportionalHeight = (widthHeight) -> ratio >= 0 ? widthHeight.getFirst() / ratio : widthHeight.getSecond();
        widthProperty().addListener(observable -> draw());
        heightProperty().addListener(observable -> draw());
    }

    public void draw()
    {
        final Pair<Double, Double> widthHeight = Pair.createPair(this.getWidth(), this.getHeight());
        final Function<Pair<Double, Double>, Double> displayWidth = getDisplayWidth(widthHeight);
        final Function<Pair<Double, Double>, Double> displayHeight = getDisplayHeight(widthHeight);

        gc.clearRect(0, 0, this.getWidth(), this.getHeight());
        gc.drawImage(image, 0, 0, displayWidth.apply(widthHeight), displayHeight.apply(widthHeight));
    }

    private Function<Pair<Double, Double>, Double> getDisplayWidth(Pair<Double, Double> widthHeight)
    {
        final double proportionedWidth = proportionalWidth.apply(widthHeight);
        final double proportionedHeight = proportionalHeight.apply(widthHeight);

        return (Pair<Double, Double> dimensions) -> proportionedHeight > dimensions.getSecond() ?
                dimensions.getSecond() * ratio : proportionedWidth > dimensions.getFirst() ?
                dimensions.getFirst() : proportionedWidth;
    }

    private Function<Pair<Double, Double>, Double> getDisplayHeight(Pair<Double, Double> widthHeight)
    {
        final double proportionedWidth = proportionalWidth.apply(widthHeight);
        final double proportionedHeight = proportionalHeight.apply(widthHeight);

        return (Pair<Double, Double> dimensions) -> proportionedWidth > dimensions.getFirst() ?
                dimensions.getFirst() * ratio : proportionedHeight > dimensions.getSecond() ?
                dimensions.getSecond() : proportionedHeight;
    }

    @Override
    public boolean isResizable()
    {
        return true;
    }

    @Override
    public double prefWidth(double height)
    {
        return getWidth();
    }

    @Override
    public double prefHeight(double width)
    {
        return getHeight();
    }
}
