/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.google.common.base.Objects;

/**
 * Implementation of a version with a major and minor number
 * Created by James Hawthorne on 28/05/2016.
 */
public class MajorMinor implements Version
{
    private final String name;
    private final int majorNum;
    private final double minorNum;

    public final static Version IDENTITY = newInstance(0, 0);

    public MajorMinor(String name, int majorNum, double minorNum)
    {
        this.name = name;
        this.majorNum = majorNum;
        this.minorNum = minorNum;
    }

    private MajorMinor(int majorNum, double minorNum)
    {
        this("App Name", majorNum, minorNum);
    }

    public String getName() { return name; }

    @Override
    public int getMajorNum()
    {
        return majorNum;
    }

    @Override
    public double getMinorNum()
    {
        return minorNum;
    }

    public static MajorMinor newInstance(int majorNum, double minorNum)
    {
        return new MajorMinor(majorNum, minorNum);
    }

    public static MajorMinor newInstance(String name, int majorNum, double minorNum)
    {
        return new MajorMinor(name, majorNum, minorNum);
    }

    @Override
    public String toString()
    {
        return name + " " + majorNum + "." + minorNum;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MajorMinor that = (MajorMinor) o;
        return majorNum == that.majorNum &&
                Double.compare(that.minorNum, minorNum) == 0 &&
                Objects.equal(name, that.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(name, majorNum, minorNum);
    }
}
