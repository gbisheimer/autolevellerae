/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.guice.modules.ALModule;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.cncsoftwaretools.autoleveller.ui.controllers.ALGUIController;
import com.cncsoftwaretools.autoleveller.ui.controllers.MeshTabController;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.math.NumberUtils;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * The main AL class used to create a JavaFX application
 * Created by James Hawthorne on 16/06/2015.
 */
public class Autoleveller extends Application
{
    private final Optional<Properties> props = getProperties("/.properties");
    private static final Optional<Logger> LOGGER = getLogger(Autoleveller.class.getName());
    public static final DecimalFormat DF = new DecimalFormat("0.######",
            DecimalFormatSymbols.getInstance(Locale.ENGLISH)); //use this for any output
    public static String name;
    public static String version;
    public static String changeSet;
    public static LocalDate buildDate;
    private static Stage primaryStage;

    private ALGUIController rootController;

    static final String MAINFXML = "/ALGUI/MainAL.fxml";

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Sets the scene, stage and gets properties relating to this project from the properties file or sets
     * default values if the file does not exist. Shows the stage finally.
     * This method is called by the system. You never need to call this
     * @param primaryStage The JavaFX Stage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        this.primaryStage = primaryStage;
        LOGGER.ifPresent(logger -> logger.log(Level.CONFIG, "starting JavaFX application with " + MAINFXML));
        final Injector injector = Guice.createInjector(new ALModule());
        FXMLLoader loader = new FXMLLoader(getClass().getResource(MAINFXML));
        loader.setControllerFactory(injector::getInstance);
        Parent root = loader.load();
        rootController = loader.getController();
        ALModel model = ALModel.getInstance();
        model.setHostServices(getHostServices());

        primaryStage.getIcons().add(new Image("/alicon.png"));

        Scene scene = new Scene(root, 1024, 730);

        // get all version information
        props.ifPresent(properties -> {
            name = getProperty(properties, "AutolevellerAE.name").get();
            version = getProperty(properties, "AutolevellerAE.version").get();
            changeSet = getProperty(properties, "AutolevellerAE.build").get();
            String timeStamp = getProperty(properties, "AutolevellerAE.buildDate").get();
            
            if (!NumberUtils.isNumber(timeStamp)){
            	//this if/else avoids exceptions if maven not run first to get build date etc.
            	LOGGER.ifPresent(logger -> logger.log(Level.WARNING, "properties file not configured correctly. Run maven build first"));
            	buildDate = LocalDateTime.now().toLocalDate();
            	changeSet = "not set";
            }
            else{
            	Instant timeStampInstant = Instant.ofEpochMilli(Long.parseLong(timeStamp));
                LocalDateTime localDateTime = LocalDateTime.ofInstant(timeStampInstant, ZoneId.systemDefault());
                buildDate = localDateTime.toLocalDate();
            }
        });

        setTitle("");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void setTitle(String title)
    {
        //need to check thread name because title cannot be updated from main for TestFX
        if (!Thread.currentThread().getName().equalsIgnoreCase("main")){
            primaryStage.setTitle(name + " " + version + title);
        }
    }

    public static void changeSize(double width, double height)
    {
        primaryStage.setWidth(width);
        primaryStage.setHeight(height);
    }

    /**
     * Can create a Logger instance to be used by any class in this project.
     * The properties are set in /logging.properties
     * @param className The class name to use in logging
     * @return An optional logger. If there is an error creating the logger, an empty optional is returned
     */
    public static Optional<Logger> getLogger(@Nonnull String className)
    {
        try(InputStream stream = Autoleveller.class.getResourceAsStream("/logging.properties"))
        {
            LogManager.getLogManager().readConfiguration(stream);
            return Optional.ofNullable(Logger.getLogger(className));
        }
        catch (Exception e)
        {
            return Optional.empty();
        }
    }

    static Optional<Properties> getProperties(@Nonnull String propertiesFile)
    {
        LOGGER.ifPresent(logger -> logger.log(Level.FINE, "getting properties file at " + propertiesFile));

        try(InputStream stream = Autoleveller.class.getResourceAsStream(propertiesFile))
        {
            final Properties properties = new Properties();
            properties.load(stream);
            return Optional.of(properties);
        }
        catch (Exception e)
        {
            return Optional.empty();
        }
    }

    Optional<String> getProperty(@Nonnull Properties propertyObj, @Nonnull String key)
    {
        return Optional.ofNullable(propertyObj.getProperty(key));
    }

    @VisibleForTesting
    public ALGUIController getRootController()
    {
        return rootController;
    }
}
