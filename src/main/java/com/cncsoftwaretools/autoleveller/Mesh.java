/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.reader.GCodeReader;
import com.cncsoftwaretools.autoleveller.reader.PlainGCodeReader;
import com.cncsoftwaretools.autoleveller.reader.processor.*;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.shape.Rectangle;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * A Mesh is a grid of ThreeDPoints representing the area to be probed where each point is a probe point.
 * The size of the grid and value of is points are determined by the members
 * which are set during construction of an instance via a builder.
 * 
 * <p>NOTE: The first point is at the bottom left row and column. So, for a 9 * 9 Mesh, the first point is at row 8 column 0</p>
 *
 * Created by James Hawthorne on 11/06/2015.
 */
public class Mesh implements PFGWriter, LevellerWriter
{
    public static final int SCALE = ALTextBlockFunctions.SCALE;

    private final Controller controller;
    private final Units units;
    private final BigDecimal xValue;
    private final BigDecimal yValue;
    private final BigDecimal xLength;
    private final BigDecimal yLength;
    private final BigDecimal zFeed;
    private final BigDecimal xyFeed;
    private final BigDecimal probeDepth;
    private final BigDecimal probeClearance;
    private final BigDecimal pointSpacing;
    private final BigDecimal safeHeight;
    private final int rows;
    private final int cols;
    private final BigDecimal ySpaceSize;
    private final BigDecimal xSpaceSize;
    private final Map<Rect, BigDecimal> insetlessSize; // size before insets applied
    private final Rectangle size;
    private final Map<Inset, Double> insetMap;
    private final ALTextBlockFunctions alTextBlockFunctions = new ALTextBlockFunctions();
    private Canvas meshRect;

    private final Map<Integer, List<ThreeDPoint>> mesh;
    private final Logger LOGGER = Autoleveller.getLogger(
            Mesh.class.getName()).orElse(Logger.getAnonymousLogger());

    private Mesh(Builder builder)
    {
        final Predicate<BigDecimal> greaterThanZero = num -> num.compareTo(BigDecimal.ZERO) > 0;

        controller = builder.controller;
        units = builder.units;
        insetlessSize = ImmutableMap.of(Rect.X, builder.xValue, Rect.Y, builder.yValue,
                Rect.WIDTH, builder.xLength, Rect.HEIGHT, builder.yLength);
        insetMap = ImmutableMap.copyOf(builder.insetMap);
        xValue = builder.xValue.add(BigDecimal.valueOf(insetMap.get(Inset.LEFT)));
        yValue = builder.yValue.add(BigDecimal.valueOf(insetMap.get(Inset.BOTTOM)));
        xLength = builder.xLength.subtract(BigDecimal.valueOf(insetMap.get(Inset.LEFT)))
                .subtract(BigDecimal.valueOf(insetMap.get(Inset.RIGHT)));
        yLength = builder.yLength.subtract(BigDecimal.valueOf(insetMap.get(Inset.BOTTOM)))
                .subtract(BigDecimal.valueOf(insetMap.get(Inset.TOP)));
        zFeed = builder.zFeed;
        xyFeed = builder.xyFeed;
        probeDepth = builder.probeDepth;
        probeClearance = builder.probeClearance;
        pointSpacing = builder.pointSpacing;
        safeHeight = builder.safeHeight;

        if (!(greaterThanZero.test(xLength) || greaterThanZero.test(yLength) || greaterThanZero.test(zFeed) ||
                greaterThanZero.test(xyFeed) || greaterThanZero.test(pointSpacing) || greaterThanZero.test(probeClearance)))
            throw new IllegalArgumentException("Should be greater than 0");

        if (probeDepth.compareTo(BigDecimal.ZERO) > 0)
            throw new IllegalArgumentException("Should be negative");

        final int ySpaces = greaterThanZero.test(pointSpacing) ? yLength.divide(pointSpacing, RoundingMode.HALF_EVEN).intValue() :
                BigDecimal.ZERO.intValue();
        final int xSpaces = greaterThanZero.test(pointSpacing) ? xLength.divide(pointSpacing, RoundingMode.HALF_EVEN).intValue() :
                BigDecimal.ZERO.intValue();
        rows = ySpaces + 1;
        cols = xSpaces + 1;
        ySpaceSize = ySpaces != 0 ? yLength.divide(BigDecimal.valueOf(ySpaces), SCALE, RoundingMode.HALF_EVEN) :
                BigDecimal.ZERO;
        xSpaceSize = xSpaces != 0 ? xLength.divide(BigDecimal.valueOf(xSpaces), SCALE, RoundingMode.HALF_EVEN) :
                BigDecimal.ZERO;

        size = new Rectangle(xValue.doubleValue(), yValue.doubleValue(), xLength.doubleValue(), yLength.doubleValue());
        meshRect = new Canvas(size.getWidth(), size.getHeight());

        mesh = generateMesh();
    }

    public Controller getController(){return controller;}

    public Units getUnits(){return units;}

    public BigDecimal getxValue()
    {
        return xValue;
    }

    public BigDecimal getyValue()
    {
        return yValue;
    }

    public BigDecimal getxLength()
    {
        return xLength;
    }

    public BigDecimal getyLength()
    {
        return yLength;
    }

    public Map<Rect, BigDecimal> getInsetlessSize() { return insetlessSize; }

    public Rectangle getSize()
    {
        return size;
    }

    public BigDecimal getzFeed()
    {
        return zFeed;
    }

    public BigDecimal getXyFeed()
    {
        return xyFeed;
    }

    public BigDecimal getProbeDepth()
    {
        return probeDepth;
    }

    public BigDecimal getProbeClearance()
    {
        return probeClearance;
    }

    public BigDecimal getPointSpacing()
    {
        return pointSpacing;
    }

    public BigDecimal getSafeHeight()
    {
        return safeHeight;
    }

    public int getRowCount()
    {
        return rows;
    }

    public int getColCount() { return cols; }

    public double getInset(Inset inset){return insetMap.get(inset);}

    private Map<Integer, List<ThreeDPoint>> generateMesh()
    {
        Map<Integer, List<ThreeDPoint>> grid = new HashMap<>();

        for (int row = 0; row < rows; row++){
            List<ThreeDPoint> eachRow = new ArrayList<>();
            for (int col = 0; col < cols; col++){
                BigDecimal xPoint = BigDecimal.valueOf(xValue.doubleValue() + col *
                        xSpaceSize.doubleValue()).setScale(SCALE, RoundingMode.HALF_UP);
                BigDecimal yPoint = BigDecimal.valueOf(yValue.doubleValue() + row *
                        ySpaceSize.doubleValue()).setScale(SCALE, RoundingMode.HALF_UP);
                int startingParm = controller.getStartingParameter().matches("^#\\d+$") ?
                        Integer.parseInt(controller.getStartingParameter().substring(1)) : 500;
                String zPoint = "#" + (startingParm + (row * cols) + col);

                ThreeDPoint point = ThreeDPoint.createPoint(xPoint, yPoint, zPoint);
                eachRow.add(point);
            }
            grid.put(row, ImmutableList.copyOf(eachRow));
        }
        return ImmutableMap.copyOf(grid);
    }

    /**
     * Gets the ThreeDPoint at specified row and column.
     * The row and / or column will be limited to the minimum or maximum value for this Mesh.
     * If the row or column are outside the mesh bounds an empty Optional is returned
     * @param row The row of the point to return
     * @param col The column of the point to return
     * @return An Optional ThreeDPoint because the row and/or column may be out of bounds
     */
    Optional<ThreeDPoint> getPoint(int row, int col)
    {
        if (row < 0 || row >= rows) {return Optional.empty();}
        if (col < 0 || col >= cols) {return Optional.empty();}

        return Optional.of(mesh.get(row).get(col));
    }

    /**
     * Gets all points in the mesh ass a single flattened List of ThreeDPoints
     * @return the flattened List
     */
    public List<ThreeDPoint> getPointsAsList()
    {
        return mesh.keySet().stream()
                .flatMap(rowNum -> mesh.get(rowNum).stream())
                .collect(toList());
    }

    /**
     * As part of the probe routine, this method simply moves to a given point and probes that point
     * @param point The point to be probed. Only the X and Y values are used from this point
     */
    Consumer<ALWriter> moveNprobe(ThreeDPoint point)
    {
        return writer -> printMoveNProbe(writer, point);
    }

    private void printMoveNProbe(ALWriter writer, ThreeDPoint point)
    {
        writer.writeGCODELine("G0 Z" + Autoleveller.DF.format(probeClearance));
        writer.writeGCODELine("G1 X" + Autoleveller.DF.format(point.getXValue()) + " Y" + Autoleveller.DF.format(point.getYValue()) +
                " F" + Autoleveller.DF.format(xyFeed));
        writer.writeGCODELine(controller.getProbeWord() + " Z" + Autoleveller.DF.format(probeDepth) + " F" + Autoleveller.DF.format(zFeed));
        if (!controller.getProbeCommand().trim().isEmpty()){writer.writeGCODELine(controller.getProbeCommand());}
    }

    /**
     * Applies a given consumer to each probe point in this Mesh. In the order of the probe routine.
     * @param pointConsumer The Consumer to apply to each point
     * @return This method as a Consumer<ALWriter>
     */
    public Consumer<ALWriter> meshApplier(Consumer<ThreeDPoint> pointConsumer)
    {
        return writer -> {
            for (int row = 0; row < rows; row++ ){
                List<ThreeDPoint> pointsInRow = mesh.get(row);
                if (row % 2 != 0){pointsInRow = Lists.reverse(pointsInRow);}
                pointsInRow.forEach(pointConsumer);
            }
        };
    }

    /**
     * Returns a list of the 4 mesh points surrounding the input point. Each returned point
     * is bounded to the number of rows or columns within this Mesh.
     * index values: 0 = Top Left, 1 = Top Right, 2 = Bottom Left, 3 = Bottom Right
     * @param pointToFind The point to find the 4 surrounding points for
     * @return A list of 4 surrounding points
     */
    public List<ThreeDPoint> getSurroundingPoints(Point2D pointToFind)
    {
        BigDecimal xDistanceFromInitialPoint = BigDecimal.valueOf(pointToFind.getX()).subtract(xValue);
        BigDecimal yDistanceFromInitialPoint = BigDecimal.valueOf(pointToFind.getY()).subtract(yValue);

        int leftColNum = !xSpaceSize.equals(BigDecimal.ZERO) ?
                xDistanceFromInitialPoint.divide(xSpaceSize, 0, BigDecimal.ROUND_FLOOR).intValue() : 0;
        leftColNum = Math.max(0, Math.min(leftColNum, cols - 1)); //limit to grid range
        int rightColNum = !xSpaceSize.equals(BigDecimal.ZERO) ?
                xDistanceFromInitialPoint.divide(xSpaceSize, 0, BigDecimal.ROUND_CEILING).intValue() : 0;
        rightColNum = Math.max(0, Math.min(rightColNum, cols - 1)); //limit to grid range
        int bottomRowNum = !ySpaceSize.equals(BigDecimal.ZERO) ?
                yDistanceFromInitialPoint.divide(ySpaceSize, 0, BigDecimal.ROUND_FLOOR).intValue() : 0;
        bottomRowNum = Math.max(0, Math.min(bottomRowNum, rows - 1)); //limit to grid range
        int topRowNum = !ySpaceSize.equals(BigDecimal.ZERO) ?
                yDistanceFromInitialPoint.divide(ySpaceSize, 0, BigDecimal.ROUND_CEILING).intValue() : 0;
        topRowNum = Math.max(0, Math.min(topRowNum, rows - 1)); //limit to grid range

        return ImmutableList.of(getPoint(topRowNum, leftColNum).get(), getPoint(topRowNum, rightColNum).get(),
                getPoint(bottomRowNum, leftColNum).get(), getPoint(bottomRowNum, rightColNum).get());
    }

    public String displayDetails()
    {
        DecimalFormat df = Autoleveller.DF;

        return "Units=" + units + " X=" + df.format(size.getX()) + " Y=" + df.format(size.getY()) +
                " Width=" + df.format(size.getWidth()) + " Height=" + df.format(size.getHeight());
    }

    /**
     * Builder object used for constructing this Mesh <br>
     * Basic usage example: {@code Mesh mesh = new Mesh.Builder(Units.MM, Software.MACH3, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.TEN).build();}
     */
    public static class Builder
    {
        //required
        private final Units units;
        private final Controller controller;
        private final BigDecimal xValue;
        private final BigDecimal yValue;
        private final BigDecimal xLength;
        private final BigDecimal yLength;
        //optional
        private BigDecimal zFeed;
        private BigDecimal xyFeed;
        private BigDecimal probeDepth;
        private BigDecimal probeClearance;
        private BigDecimal pointSpacing;
        private BigDecimal safeHeight;
        private Map<Inset, Double> insetMap;

        /**
         * The only constructor for this builder. Its parameters are required. Optional parameters can be set via
         * the associated build method.
         * @param aUnits the Units to use.
         * @param asoftware the controller software used
         * @param aXValue the leftmost x value
         * @param aYValue the bottommost y value
         * @param aXWidth the width of the mesh
         * @param aYLength the length of the mesh
         */
        public Builder(Units aUnits, Controller asoftware, BigDecimal aXValue, BigDecimal aYValue,
                       BigDecimal aXWidth, BigDecimal aYLength)
        {
            units = aUnits;
            controller = asoftware;
            DefaultMeshValues dmv = units.equals(Units.MM) ? DefaultMeshValues.MM : DefaultMeshValues.INCHES;

            xValue = aXValue.setScale(SCALE, RoundingMode.HALF_EVEN);
            yValue = aYValue.setScale(SCALE, RoundingMode.HALF_EVEN);
            xLength = aXWidth.setScale(SCALE, RoundingMode.HALF_EVEN);
            yLength = aYLength.setScale(SCALE, RoundingMode.HALF_EVEN);
            zFeed = dmv.getZFeed();
            xyFeed = dmv.getXYFeed();
            probeDepth = dmv.getProbeDepth();
            probeClearance = dmv.getProbeClearance();
            pointSpacing = dmv.getPointSpacing();
            safeHeight = dmv.getSafeHeight();
            insetMap = new HashMap<>();
            insetMap.put(Inset.LEFT, dmv.getInset(Inset.LEFT));
            insetMap.put(Inset.RIGHT, dmv.getInset(Inset.RIGHT));
            insetMap.put(Inset.TOP, dmv.getInset(Inset.TOP));
            insetMap.put(Inset.BOTTOM, dmv.getInset(Inset.BOTTOM));
        }

        /**
         * After constructing and setting the optional fields, use this method to create a Mesh object.
         * @return a Mesh object based on this Builder
         */
        public Mesh build()
        {
            return new Mesh(this);
        }

        public Builder buildDepth(double thisProbeDepth)
        {
            probeDepth = BigDecimal.valueOf(thisProbeDepth).setScale(SCALE, RoundingMode.HALF_EVEN);
            return this;
        }

        public Builder buildSpacing(double desiredSpacing)
        {
            pointSpacing = BigDecimal.valueOf(desiredSpacing).setScale(SCALE, RoundingMode.HALF_EVEN);
            return this;
        }

        public Builder buildZFeed(double thisZFeed)
        {
            zFeed = BigDecimal.valueOf(thisZFeed).setScale(SCALE, RoundingMode.HALF_EVEN);
            return this;
        }

        public Builder buildXYFeed(double thisXYFeed)
        {
            xyFeed = BigDecimal.valueOf(thisXYFeed).setScale(SCALE, RoundingMode.HALF_EVEN);
            return this;
        }

        public Builder buildClearance(double thisClearance)
        {
            probeClearance = BigDecimal.valueOf(thisClearance).setScale(SCALE, RoundingMode.HALF_EVEN);
            return this;
        }

        public Builder buildSafeHeight(double thisSafeHeight)
        {
            safeHeight = BigDecimal.valueOf(thisSafeHeight).setScale(SCALE, RoundingMode.HALF_EVEN);
            return this;
        }

        //insert all insets in one method (not individually) so that we can calculate if the insets overlap
        public Builder buildInsets(double lInset, double rInset, double tInset, double bInset)
        {
            double insetWidth = xLength.subtract(BigDecimal.valueOf(lInset)).subtract(BigDecimal.valueOf(rInset)).doubleValue();
            double insetHeight = yLength.subtract(BigDecimal.valueOf(bInset)).subtract(BigDecimal.valueOf(tInset)).doubleValue();

            if ((insetWidth <= 0) || (insetHeight < 0)){
                throw new IllegalArgumentException("Insets would overlap each other, cannot build mesh with these insets");
            }

            insetMap.put(Inset.LEFT, lInset);
            insetMap.put(Inset.RIGHT, rInset);
            insetMap.put(Inset.TOP, tInset);
            insetMap.put(Inset.BOTTOM, bInset);
            return this;
        }
    }

    @Override
    public String toString()
    {
        return "Mesh{units=" + this.units + ", xValue=" + xValue + ", yValue=" + yValue + ", xLength=" + xLength + ", yLength=" + yLength +
                ", zFeed=" + zFeed + ", xyFeed=" + xyFeed + ", probeDepth=" + probeDepth + ", probeClearance=" + probeClearance +
                ", pointSpacing=" + pointSpacing + ", safeHeight=" + safeHeight + ", rows=" + getRowCount() +
                ", columns=" + getColCount() + System.lineSeparator() +
                Lists.reverse(IntStream.range(0, mesh.size()).boxed().collect(toList())).stream()
                        .map(mesh::get)
                        .map(Object::toString)
                        .collect(Collectors.joining(System.lineSeparator()));
    }

    private Consumer<ALWriter> writeProbeRoutine()
    {
        return writer -> {
            meshApplier(threeDPoint -> {
                moveNprobe(threeDPoint).accept(writer);
                if (!controller.getStartingParameter().isEmpty())
                {writer.writeGCODELine(threeDPoint.getZValue() + "=" + this.controller.getCurrentZparameter());}
            }).accept(writer);
            writer.writeGCODELine("G0 Z" + getProbeClearance());
            writer.writeGCODELine("");
        };
    }

    @Override
    public Consumer<ALWriter> writePFG()
    {
        DecimalFormat df = Autoleveller.DF;
        ThreeDPoint firstPoint = getPoint(0, 0).get();
        Optional<ALModel> modelOpt = Optional.ofNullable(ALModel.getInstance());
        String defaultInit = ALTextBlockFunctions.zInitGCode(this).stream().collect(Collectors.joining("\n"));
        String initStr = modelOpt.isPresent() ?
                modelOpt.get().readUsePFGZInit() ?
                        modelOpt.get().readString(ALModel.CST_PFG_INIT_BLOCK) :
                        defaultInit :
                defaultInit;

        return writer -> {
            alTextBlockFunctions.prembleWriter().accept(writer);
            writer.writeGCODELine("");
            alTextBlockFunctions.probeInstructionsWriter().accept(writer);
            writer.writeGCODELine("");
            alTextBlockFunctions.safetyBlockWriter(units).accept(writer);
            writer.writeGCODELine("");
            writer.writeGCODELine("M0 (Attach probe wires and clips that need attaching)");
            writer.writeGCODELine(initStr);
            writer.writeGCODELine("");
            writer.writeGCODELine(controller.getOpenLogCommand());
            meshApplier(threeDPoint -> moveNprobe(threeDPoint).accept(writer)).accept(writer);
            writer.writeGCODELine("G0 Z" + df.format(getProbeClearance()));
            writer.writeGCODELine(controller.getCloseLogCommand());
            writer.writeGCODELine("G0 X" + df.format(firstPoint.getXValue()) + " Y"
                    + df.format(firstPoint.getYValue()) + " Z" + df.format(getSafeHeight()));
            writer.writeGCODELine("M0 (Detach any clips used for probing)");
            writer.writeGCODELine("M30");
        };
    }

    @Override
    public void writeLevelled(String outFilename, ALModel alModel)
    {
        String initStr = alModel.readUseLvlZInit() ? alModel.readString(ALModel.CST_LVL_INIT_BLOCK) :
                ALTextBlockFunctions.zInitGCode(this).stream().collect(Collectors.joining("\n"));

        Consumer<ALWriter> preText = writer -> {
            alTextBlockFunctions.prembleWriter().accept(writer);
            writer.writeGCODELine("");
            alTextBlockFunctions.probeInstructionsWriter().accept(writer);
            writer.writeGCODELine("");
            alTextBlockFunctions.safetyBlockWriter(units).accept(writer);
            writer.writeGCODELine("");
            writer.writeGCODELine("M0 (Attach probe wires and clips that need attaching)");
            writer.writeGCODELine(initStr);
            writer.writeGCODELine("");
            writeProbeRoutine().accept(writer);
            writer.writeGCODELine("M0 (Detach any clips used for probing)");
            writer.writeGCODELine("");
        };

        try {
            ALWriter.useALWriter(new PrintWriter(new BufferedWriter(new FileWriter(outFilename, false))), preText);

            GCodeReader reader = PlainGCodeReader.newInstance(new FileReader(alModel.getOGF().getFilename()));
            GCodeProcessor writer = WriterProcessor.newInstance(Optional.empty(), outFilename, true);
            GCodeProcessor leveller = LevelerProcessor.newInstance(Optional.of(writer), this);
            GCodeProcessor arcSegmentor = ArcSegmentProcessor.newInstance(Optional.of(leveller),
                    alModel.readDouble(ALModel.MAX_SEG_LTH));
            GCodeProcessor lineSegmentor = LineSegmentProcessor.newInstance(Optional.of(arcSegmentor),
                    alModel.readDouble(ALModel.MAX_SEG_LTH));
            reader.readFile(lineSegmentor);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error writing levelled file to disk");
        }
    }

    public enum Rect{X, Y, WIDTH, HEIGHT}
    public enum Inset{LEFT, RIGHT, TOP, BOTTOM}
}
