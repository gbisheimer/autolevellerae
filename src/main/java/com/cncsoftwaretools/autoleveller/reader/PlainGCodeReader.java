/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader;

import com.cncsoftwaretools.autoleveller.reader.processor.GCodeProcessor;
import com.cncsoftwaretools.autoleveller.reader.processor.ReplacerPreProcessor;
import com.google.common.collect.ImmutableList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.cncsoftwaretools.autoleveller.Autoleveller.getLogger;

/**
 * Implementation of a GCodeReader. Reads the given file and passes each line to a gcode processor object.
 * Created by James Hawthorne on 04/09/2015.
 */
public class PlainGCodeReader implements GCodeReader
{
    private final BufferedReader fileReader;
    private static final Optional<Logger> LOGGER = getLogger(PlainGCodeReader.class.getName());

    private PlainGCodeReader (BufferedReader fileReader)
    {
        this.fileReader = fileReader;
    }

    public static PlainGCodeReader newInstance(FileReader fileReader) {return new PlainGCodeReader(new BufferedReader(fileReader));}

    public static PlainGCodeReader newInstance(BufferedReader fileReader){return new PlainGCodeReader(fileReader);}

    @Override
    public void readFile(GCodeProcessor processor) throws IOException
    {
        try (BufferedReader reader = fileReader) {
            Optional<GCodeState> previousState = Optional.empty();
            String line;

            while ((line = reader.readLine()) != null) {
                String preprocessedLine = ReplacerPreProcessor.normalizeString(line);
                GCodeState currentState = GCodeState.newInstance(previousState, preprocessedLine);
                processor.interpret(ImmutableList.of(currentState));
                previousState = Optional.of(currentState);
            }
        }

        finishProcessing(processor);
    }


    // a recursive method to call finish() and log it on each processor from the bottom up
    private void finishProcessing(GCodeProcessor processor)
    {
        if (processor.nextProcessor().isPresent()){
            finishProcessing(processor.nextProcessor().get());
        }

        LOGGER.ifPresent(logger -> logger.log(Level.INFO, processor.getClass().getSimpleName() + " finishing..."));
        processor.finish();
    }

}

