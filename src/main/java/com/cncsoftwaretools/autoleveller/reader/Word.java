/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader;

import com.cncsoftwaretools.autoleveller.Pair;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * A Word in gcode is any character followed by a number.
 * 1 or more words in a gcode lne make up a command
 * Created by James Hawthorne on 20/08/2015.
 */
public class Word
{
    public static final Pattern DECIMALPATTERN = Pattern.compile("-?[0-9]+([\\.,][0-9]+)?");
    public static final Pattern WORDPATTERN = Pattern.compile("([A-Z])\\s*(" + DECIMALPATTERN.pattern() + ")");

    private final Pair<Character, BigDecimal> word;
    private final WordGroup wordGroup;
    private final String description;

    private Word(Pair<Character, BigDecimal> wordPair)
    {
        word = wordPair;
        Pair<WordGroup, String> elements = createElements(wordPair);
        wordGroup = elements.getFirst();
        description = elements.getSecond();
    }

    private static Pair<WordGroup, String> createElements(Pair<Character, BigDecimal> wordPair)
    {
        switch (wordPair.getFirst()){
            case ('F'):
                return Pair.createPair(WordGroup.FEEDGROUP, "Feed rate");
            case ('S'):
                return Pair.createPair(WordGroup.SPEEDGROUP, "Spindle-speed");
            case ('T'):
                return Pair.createPair(WordGroup.TOOLGROUP, "Tool selection");
            case ('X'):
                return Pair.createPair(WordGroup.XAXISGROUP, "X coordinate");
            case ('Y'):
                return Pair.createPair(WordGroup.YAXISGROUP, "Y coordinate");
            case ('Z'):
                return Pair.createPair(WordGroup.ZAXISGROUP, "Z coordinate");
            case ('I'):
                return Pair.createPair(WordGroup.NOGROUP, "X offset for arcs and G87 canned cycles");
            case ('J'):
                return Pair.createPair(WordGroup.NOGROUP, "Y offset for arcs and G87 canned cycles");
            case ('K'):
                return Pair.createPair(WordGroup.NOGROUP, "Z offset for arcs and G87 canned cycles");
            case ('R'):
                return Pair.createPair(WordGroup.NOGROUP, "Arc radius or canned cycle plane");
            case ('G'):
                return createGcodeElements(wordPair.getSecond());
            case ('M'):
                return createMcodeElements(wordPair.getSecond());
            default:
                return Pair.createPair(WordGroup.NOGROUP, "no description set yet");
        }
    }

    private static Pair<WordGroup, String> createGcodeElements(BigDecimal gcodeValue)
    {
        if (gcodeValue.equals(BigDecimal.ZERO)) return Pair.createPair(WordGroup.MOTIONGROUP, "Rapid motion");
        else if (gcodeValue.equals(BigDecimal.ONE)) return Pair.createPair(WordGroup.MOTIONGROUP, "Linear move with Feed");
        else if (gcodeValue.equals(BigDecimal.valueOf(2))) return Pair.createPair(WordGroup.MOTIONGROUP, "Arc move with Feed (CW)");
        else if (gcodeValue.equals(BigDecimal.valueOf(3))) return Pair.createPair(WordGroup.MOTIONGROUP, "Arc move with Feed (CCW)");
        else if ((gcodeValue.compareTo(BigDecimal.valueOf(80)) >= 0) &&
                (gcodeValue.compareTo(BigDecimal.valueOf(89)) <= 0)) return  Pair.createPair(WordGroup.MOTIONGROUP, "Drill cycle (G80-G89)");
        else if (gcodeValue.equals(BigDecimal.valueOf(4))) return Pair.createPair(WordGroup.NOGROUP, "Dwell");
        else if (gcodeValue.equals(BigDecimal.valueOf(21))) return Pair.createPair(WordGroup.UNITSGROUP,"Input in Millimeters");
        else if (gcodeValue.equals(BigDecimal.valueOf(20))) return Pair.createPair(WordGroup.UNITSGROUP, "Input in Inches");
        else if (gcodeValue.equals(BigDecimal.valueOf(90))) return Pair.createPair(WordGroup.DISTANCEGROUP, "Absolute distance mode");
        else if (gcodeValue.equals(BigDecimal.valueOf(91))) return Pair.createPair(WordGroup.DISTANCEGROUP, "Incremental distance mode");
        else if (gcodeValue.equals(BigDecimal.valueOf(40))) return Pair.createPair(WordGroup.CUTTERCOMP, "Cancel cutter compensation");
        else if (gcodeValue.equals(BigDecimal.valueOf(41))) return Pair.createPair(WordGroup.CUTTERCOMP, "Cutter compensation left side");
        else if (gcodeValue.equals(BigDecimal.valueOf(42))) return Pair.createPair(WordGroup.CUTTERCOMP, "Cutter compensation right side");
        else if (gcodeValue.equals(BigDecimal.valueOf(49))) return Pair.createPair(WordGroup.TOOLLENGTHOFFSET, "Tool length offset compensation cancel");
        else if (gcodeValue.equals(BigDecimal.valueOf(43))) return Pair.createPair(WordGroup.TOOLLENGTHOFFSET, "Tool length offset compensation from tool table");
        else if (gcodeValue.equals(BigDecimal.valueOf(43.1))) return Pair.createPair(WordGroup.TOOLLENGTHOFFSET, "Dynamic tool length offset compensation");
        else if (gcodeValue.equals(BigDecimal.valueOf(61))) return Pair.createPair(WordGroup.PATHCONTMODE, "Exact path mode");
        else if (gcodeValue.equals(BigDecimal.valueOf(61.1))) return Pair.createPair(WordGroup.PATHCONTMODE, "Exact path mode");
        else if (gcodeValue.equals(BigDecimal.valueOf(64))) return Pair.createPair(WordGroup.PATHCONTMODE, "Path control mode with optional tolerance");
        else if (gcodeValue.equals(BigDecimal.valueOf(90.1))) return Pair.createPair(WordGroup.ARCMODE, "Absolute IJK mode. IJ specifies the absolute XY centre point");
        else if (gcodeValue.equals(BigDecimal.valueOf(91.1))) return Pair.createPair(WordGroup.ARCMODE, "Relative IJK mode. IJ specifies the relative (from start point) XY centre point");
        else return Pair.createPair(WordGroup.NOGROUP, "no description set yet");
    }

    private static Pair<WordGroup, String> createMcodeElements(BigDecimal mcodeValue)
    {
        if (mcodeValue.equals(BigDecimal.valueOf(6))) return Pair.createPair(WordGroup.NOGROUP, "Tool change");
        else return Pair.createPair(WordGroup.NOGROUP, "no description set yet");
    }

    public static Word createPair(char address, BigDecimal value)
    {
        return new Word(Pair.createPair(address, value));
    }

    public Pair<Character, BigDecimal> getWordPair()
    {
        return word;
    }

    public WordGroup getGroup()
    {
        return wordGroup;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word1 = (Word) o;

        if (!word.equals(word1.word)) return false;
        //noinspection SimplifiableIfStatement
        if (wordGroup != word1.wordGroup) return false;
        return description.equals(word1.description);

    }

    @Override
    public int hashCode()
    {
        int result = word.hashCode();
        result = 31 * result + wordGroup.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return String.format("Word{%s,  wordGroup=%s, description=%s}", word, wordGroup, description);
    }

    /**
     * @return The concatenation of the character and its value (i.e. the first and second values combined)
     */
    public String asString()
    {
        return word.getFirst() + word.getSecond().toString();
    }
}
