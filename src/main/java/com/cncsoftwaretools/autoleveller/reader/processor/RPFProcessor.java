/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.ALTextBlockFunctions;
import com.cncsoftwaretools.autoleveller.RPF;
import com.cncsoftwaretools.autoleveller.ThreeDPoint;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.util.AutolevellerUtil;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Uses an RPF and real numbers to perform levelling
 * Created by James Hawthorne on 18/05/2016.
 */
public class RPFProcessor extends GCodeInterpreter implements LevellerCommon
{
    private final RPF rpf;
    private final Optional<GCodeProcessor> nextProcessor;

    private RPFProcessor(RPF rpf, Optional<GCodeProcessor> nextProcessor)
    {
        this.rpf = rpf;
        this.nextProcessor = nextProcessor;
    }

    public static RPFProcessor newInstance(RPF rpf, Optional<GCodeProcessor> nextProcessor)
    {
        return new RPFProcessor(rpf, nextProcessor);
    }

    @Override
    public List<GCodeState> processState(GCodeState gCodeState)
    {
        return ImmutableList.of(interpolateState(gCodeState));
    }

    @Override
    public Optional<GCodeProcessor> nextProcessor()
    {
        return nextProcessor;
    }


    public GCodeState interpolateState(GCodeState originalState)
    {
        Optional<ThreeDPoint> threeDPoint = originalState.get3DPoint();

        if (!(threeDPoint.isPresent() && new BigDecimal(threeDPoint.get().getZValue()).compareTo(SURFACEHEIGHT) == -1)) {
            return originalState;
        }

        Point2D point = originalState.get2DPoint().get();
        List<Point3D> surroundingPoints = rpf.getSurroundingPoints(point);
        Point3D interpolatedPoint = AutolevellerUtil.biLinearInterpolate(surroundingPoints).apply(point);
        String replacementLine = modifyZ(originalState, originalZ -> {
            BigDecimal intPlusOriginal = new BigDecimal(interpolatedPoint.getZ()).add(originalZ);
            intPlusOriginal = intPlusOriginal.setScale(ALTextBlockFunctions.SCALE, BigDecimal.ROUND_HALF_UP);
            return "Z".concat(intPlusOriginal.toPlainString());
        });

        return GCodeState.newInstance(originalState.getPreviousState(), replacementLine);
    }
}
