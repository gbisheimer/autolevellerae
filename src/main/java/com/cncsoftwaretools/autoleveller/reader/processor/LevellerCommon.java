/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.reader.Word;
import com.cncsoftwaretools.autoleveller.reader.WordGroup;
import com.google.common.annotations.VisibleForTesting;
import com.sksamuel.diffpatch.DiffMatchPatch;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.joining;

/**
 * Contains methods common to all levellers
 * Created by James Hawthorne on 19/05/2016.
 */
public interface LevellerCommon
{
    BigDecimal SURFACEHEIGHT = BigDecimal.ZERO;

    @VisibleForTesting
    default String modifyZ(GCodeState state, Function<BigDecimal, String> zReplacementFunc)
    {
        Optional<Word> xWord = state.getModalWordByGroup(WordGroup.XAXISGROUP);
        Optional<Word> yWord = state.getModalWordByGroup(WordGroup.YAXISGROUP);
        Optional<Word> zWord = state.getModalWordByGroup(WordGroup.ZAXISGROUP);
        if (!zWord.isPresent()){return state.getRawLine();}

        String commentlessLine = GCodeState.removeComments(state.getRawLine().toUpperCase());
        LinkedList<DiffMatchPatch.Diff> diffs = new DiffMatchPatch().diff_main(state.getRawLine(), commentlessLine);
        String alteredText = commentlessLine;

        String replacementText = zReplacementFunc.apply(zWord.get().getWordPair().getSecond());

        if (alteredText.contains("Z")){ //if Z already exists just replace it with the replacement
            alteredText = alteredText.replaceAll("Z\\s{0,3}" + Word.DECIMALPATTERN.pattern(), replacementText);
        }
        else if (alteredText.contains("Y")){ //IFF Z was not replaced add the Z replacement after the Y word if it exists in the string
            alteredText = alteredText.replaceAll("Y\\s{0,3}" + Word.DECIMALPATTERN.pattern(), yWord.get().asString() + replacementText);
        }
        else if (alteredText.contains("X")){ //IFF Z was still not replaced add the Z replacement after the X word if it exists in the string
            alteredText = alteredText.replaceAll("X\\s{0,3}" + Word.DECIMALPATTERN.pattern(), xWord.get().asString() + replacementText);
        }

        //add comments back to line after making changes
        return alteredText + diffs.stream()
                .filter(diff -> diff.operation == DiffMatchPatch.Operation.DELETE)
                .map(diff -> diff.text)
                .collect(joining(""));
    }
}
