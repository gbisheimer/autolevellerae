/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeState;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

/**
 * Classes implementing this functional interface can analyse a GCodeState object make the appropriate modifications.
 * New processors need only to override processState as long as {@link GCodeInterpreter} is extended.
 * Created by James Hawthorne on 04/09/2015.
 */
public interface GCodeProcessor
{
    /**
     * Overridden and finalised in {@link GCodeInterpreter}
     */
    void interpret(List<GCodeState> states);

    /**
     * Method to use if a GCodeState is to be processed and/or divided differently
     * @param gCodeState The GCodeState to process
     * @return The processed GCodeState, which could be a list of processed GCodeStates
     */
    List<GCodeState> processState(GCodeState gCodeState);

    /**
     * If processors are to be chained, you must override this method to provide the next processor in the chain.
     * Otherwise a default empty optional will be returned
     * @return A default empty optional unless overridden
     */
    default Optional<GCodeProcessor> nextProcessor(){return Optional.empty();}

    /**
     * Informs the GCoodeProcessor to perform any cleanup needed, i.e the WriterProcessor finishes by
     * writting the remainder of its buffer to file
     */
    default void finish(){}

}
