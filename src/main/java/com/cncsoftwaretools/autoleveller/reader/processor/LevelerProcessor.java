/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.ALTextBlockFunctions;
import com.cncsoftwaretools.autoleveller.Mesh;
import com.cncsoftwaretools.autoleveller.Pair;
import com.cncsoftwaretools.autoleveller.ThreeDPoint;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.cncsoftwaretools.autoleveller.Autoleveller.getLogger;
import static java.util.stream.Collectors.toList;

/**
 * Interpolates a given GCode state based on the given Mesh and passes each new line to the given GCodeProcessor
 * Created by James Hawthorne on 29/10/2015.
 *
 * Interpolation formula used:
 *          XorY - XorY1
 * z = z1 + ------------- (z2 - z1)
 *          XorY2 - XorY1
 */
public class LevelerProcessor extends GCodeInterpreter implements LevellerCommon
{
    private Optional<GCodeState> lastProcessed = Optional.empty();

    private final Optional<GCodeProcessor> nextProcessor;
    private final Mesh mesh;
    private static final Optional<Logger> LOGGER = getLogger(LevelerProcessor.class.getName());

    private LevelerProcessor(Optional<GCodeProcessor> processor,  Mesh mesh)
    {
        this.nextProcessor = processor;
        this.mesh = mesh;
    }

    public static LevelerProcessor newInstance(Optional<GCodeProcessor> processor, Mesh probeArea)
    {
        return new LevelerProcessor(processor, probeArea);
    }

    @Override
    public List<GCodeState> processState(GCodeState currentState)
    {
        lastProcessed = currentState.getPreviousState(); //assume the previous state has already been processed

        try {
            return interpolateState(currentState).stream()
                    .map(line -> GCodeState.newInstance(lastProcessed, line))
                    .peek(gCodeState -> lastProcessed = Optional.of(gCodeState))
                    .collect(toList());
        }
        catch (Exception e)
        {
            LOGGER.ifPresent(logger -> logger.log(Level.SEVERE, "Failed to convert String to GCodeState," +
                    " possible unrecognised element of String: " + e.getMessage()));

            return ImmutableList.of();

        }
    }

    @Override
    public Optional<GCodeProcessor> nextProcessor()
    {
        return nextProcessor;
    }

    @VisibleForTesting
    String interpolateToStringFormula(BigDecimal knownXorY, Pair<BigDecimal, String> yOrXnZ, Pair<BigDecimal, String> yOrXnZ2)
    {
        BigDecimal calc = calcDivBit(knownXorY, yOrXnZ.getFirst(), yOrXnZ2.getFirst());

        return yOrXnZ.getSecond() + '+' + calc + '*' + yOrXnZ2.getSecond() + '-' + calc + '*' + yOrXnZ.getSecond();
    }

    private BigDecimal calcDivBit(BigDecimal knownXorY, BigDecimal xORy1, BigDecimal xORy2)
    {
        BigDecimal div = knownXorY.subtract(xORy1);
        BigDecimal divisor = xORy2.subtract(xORy1);

        //Cannot divide by 0 so return 0 if divisor is 0
        if (divisor.compareTo(BigDecimal.ZERO) == 0){return BigDecimal.ZERO;}

        return div.divide(divisor, ALTextBlockFunctions.SCALE, BigDecimal.ROUND_HALF_EVEN);
    }

    /**
     * @param state the original state to be levelled.
     * @return  a list of Strings representing the Gcode lines which should replace the rawline in the given state.
     * The returned lines are a maximum of 4. 3 lines for calculation + the final modified line.
     * Or just a single line if no modifications are needed.
     */
    @VisibleForTesting
    List<String> interpolateState(GCodeState state)
    {
        Optional<ThreeDPoint> threeDPoint = state.get3DPoint();

        if (threeDPoint.isPresent() && new BigDecimal(threeDPoint.get().getZValue()).compareTo(SURFACEHEIGHT) == -1){

            ThreeDPoint extractedPoint = threeDPoint.get();
            List<String> returnList = getInterpolationLines(extractedPoint, mesh.getSurroundingPoints(extractedPoint.get2DPoint()));
            returnList.add(modifyZ(state, originalZ -> "Z[#100+" + originalZ + ']'));

            return ImmutableList.copyOf(returnList);
        }

        return ImmutableList.of(state.getRawLine());
    }

    private List<String> getInterpolationLines(ThreeDPoint point, List<ThreeDPoint> surroundingPoints)
    {
        List<String> returnList = new ArrayList<>();

        String interpolateLeft = interpolateToStringFormula(point.getYValue(),
                Pair.createPair(surroundingPoints.get(0).getYValue(), surroundingPoints.get(0).getZValue()),
                Pair.createPair(surroundingPoints.get(2).getYValue(), surroundingPoints.get(2).getZValue()));
        returnList.add("#102=[" + interpolateLeft + ']');

        String interpolateRight = interpolateToStringFormula(point.getYValue(),
                Pair.createPair(surroundingPoints.get(1).getYValue(), surroundingPoints.get(1).getZValue()),
                Pair.createPair(surroundingPoints.get(3).getYValue(), surroundingPoints.get(3).getZValue()));
        returnList.add("#101=[" + interpolateRight + ']');

        String interpolateHoriz = interpolateToStringFormula(point.getXValue(),
                Pair.createPair(surroundingPoints.get(0).getXValue(), "#102"),
                Pair.createPair(surroundingPoints.get(1).getXValue(), "#101"));
        returnList.add("#100=[" + interpolateHoriz + ']');

        return returnList;
    }
}
