/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

/**
 * A generic Pair
 * Created by James Hawthorne on 08/05/2015.
 */
public class Pair <T, U>
{
    private final T first;
    private final U second;

    private Pair(T first, U second)
    {
        this.first = first;
        this.second = second;
    }

    public T getFirst()
    {
        return first;
    }

    public U getSecond()
    {
        return second;
    }

    /**
     * Factory method to create a Pair using generic types for the 2 objects
     * @param first First object of type T
     * @param second Second object of type U
     * @return A new instance of Pair consisting of the given objects
     */
    public static <T, U> Pair<T, U> createPair(@Nonnull final T first, @Nonnull final U second)
    {
        return new Pair<>(first, second);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair pair = (Pair) o;

        return first.equals(pair.first) && second.equals(pair.second);

    }

    @Override
    public int hashCode()
    {
        int result = first.hashCode();
        result = 31 * result + second.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return String.format("Pair{first=%s, second=%s}", first, second);
    }

}
