/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.google.common.collect.ImmutableList;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 *
 * Created by James Hawthorne on 26/09/2016.
 */
public class ALTextBlockFunctions
{
    public final static int SCALE = 5; //decimal places used when rounding

    /**
     * Returns a Consumer that writes a series of lines (autoleveller preamble) to a file using ALWriter.writeGCODELine
     */
    Consumer<ALWriter> prembleWriter()
    {
        return this::writePreamble;
    }

    private void writePreamble(ALWriter writer)
    {
        String fullChangeset = Autoleveller.changeSet;
        String formattedChangeset = fullChangeset != null ? "..." + fullChangeset.substring(fullChangeset.length() - 6) : "UNKMOWN";

        writer.writeGCODELine("(This GCode script was designed to adjust the Z height of a CNC machine according)");
        writer.writeGCODELine("(to the minute variations in the surface height in order to achieve a better result in the milling/etching process)");
        writer.writeGCODELine("(This script is the output of AutoLevellerAE, " + Autoleveller.version + " Changeset: " + formattedChangeset +
                " @ " + "http://autoleveller.co.uk)");
        writer.writeGCODELine("(Author: James Hawthorne PhD. File creation date: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")) + ')');
        writer.writeGCODELine("(This program and any of its output is licensed under GPLv2 and as such...)");
        writer.writeGCODELine("(AutoLevellerAE comes with ABSOLUTELY NO WARRANTY; for details, see sections 11 and 12 of the GPLv2 @ http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)");
    }

    Consumer<ALWriter> probeInitWriter(final Mesh mesh)
    {
        return alWriter -> showProbeInit(alWriter, mesh);
    }

    private void showProbeInit(ALWriter writer, Mesh meshSettings)
    {
        zInitGCode(meshSettings).forEach(writer::writeGCODELine);
    }

    public static List<String> zInitGCode(Mesh mesh)
    {
        Controller controller = mesh.getController();
        DecimalFormat df = Autoleveller.DF;
        ImmutableList.Builder<String> gcodeInitList = ImmutableList.builder();

        gcodeInitList.add("(Initialize probe routine)");
        gcodeInitList.add("G0 Z" + df.format(mesh.getSafeHeight()) + " (Move clear of the board first)");
        gcodeInitList.add("G1 X" + df.format(mesh.getxValue()) + " Y" + df.format(mesh.getyValue()) +
                " F" + df.format(mesh.getXyFeed()) + " (Move to bottom left corner)");
        gcodeInitList.add("G0 Z" + df.format(mesh.getProbeClearance()) + " (Quick move to probe clearance height)");
        controller.getProbeWordLine(mesh.getProbeDepth(), mesh.getzFeed()).ifPresent(line -> gcodeInitList.add(
                        line + " (Probe to a maximum of the specified probe height at the specified feed rate)"));
        if (!controller.getProbeCommand().trim().isEmpty()){gcodeInitList.add(controller.getProbeCommand());}
        if (!controller.getZeroWord().trim().isEmpty()){gcodeInitList.add(controller.getZeroWord() +
                " Z0 (Touch off Z to 0 once contact is made)");}
        if (!controller.getZeroCommand().trim().isEmpty()){gcodeInitList.add(controller.getZeroCommand());}
        gcodeInitList.add("G0 Z" + df.format(mesh.getProbeClearance()) + " (Move Z to above the contact point)");
        controller.getProbeWordLine(mesh.getProbeDepth(), mesh.getzFeed().divide(
                BigDecimal.valueOf(2), SCALE, RoundingMode.HALF_EVEN)).ifPresent(line -> gcodeInitList.add(
                line + " (Repeat at a more accurate slower rate)"));
        if (!controller.getProbeCommand().trim().isEmpty()){gcodeInitList.add(controller.getProbeCommand());}
        if (!controller.getZeroWord().trim().isEmpty()){gcodeInitList.add(controller.getZeroWord() + " Z0");}
        if (!controller.getZeroCommand().trim().isEmpty()){gcodeInitList.add(controller.getZeroCommand());}
        gcodeInitList.add("G0 Z" + df.format(mesh.getProbeClearance()));

        return gcodeInitList.build();
    }

    /**
     * Returns a Consumer that writes a series of lines (mesh instructions) to a file using ALWriter.writeGCODELine
     */
    Consumer<ALWriter> probeInstructionsWriter()
    {
        return this::showProbeInstructions;
    }

    private void showProbeInstructions(ALWriter writer)
    {
        writer.writeGCODELine("(The following is a checklist which can be used before the probe routine starts)");
        writer.writeGCODELine("(The checklist may not be entirely correct given your machine, therefore you should make your own adaptations as appropriate)");
        writer.writeGCODELine("(1. Make sure wires/clips are attached and the probe is working correctly)");
        writer.writeGCODELine("(Test probe connections within control software before starting)");
        writer.writeGCODELine("(2. Home all axis)");
        writer.writeGCODELine("(3. Load GCode file that contains the probe routine into your software controller)");
        writer.writeGCODELine("(4. Jog tool tip close to surface and touch off Z)");
        writer.writeGCODELine("(5. Jog Z up a fraction to make sure surface is cleared)");
        writer.writeGCODELine("(6. Jog to and touch off X and Y at the desired bottom left corner of the probe area)");
        writer.writeGCODELine("(Note: The first probe will touch off Z to 0.0 when it first touches to the surface,)");
        writer.writeGCODELine("(all other probe values are relative to this first point)");
    }

    /**
     * This block writes a series of gcodes which "reset" the machine to a safe and expected initial state
     */
    Consumer<ALWriter> safetyBlockWriter(Units units)
    {
        return writer -> writer.writeGCODELine(
                "G90 " //set absolute distance mode (not G91, incremental)
                        + units.getWord().asString() //set units used (G20 inches or G21 millimeters)
                        + " S20000" //set an initial speed in case none is set
                        + " G17");  //set default XY plane selection
    }
}
