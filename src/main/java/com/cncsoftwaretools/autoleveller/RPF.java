/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.reader.GCodeReader;
import com.cncsoftwaretools.autoleveller.reader.PlainGCodeReader;
import com.cncsoftwaretools.autoleveller.reader.processor.*;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.cncsoftwaretools.autoleveller.util.AutolevellerUtil;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.ImmutableList.of;
import static java.util.stream.Collectors.*;

/**
 * Represents the RPF, i.e. the raw points captured in the file created by the PP
 * Created by James Hawthorne on 22/04/2016.
 */
public class RPF implements LevellerWriter
{
    private static final Optional<Logger> LOGGER = Autoleveller.getLogger(RPF.class.getName());
    private static final Pattern decimalGroups = Pattern.compile("(-?[0-9]*[\\.]?[0-9]+)");
    public static final double probeSpacingUnitsLimit = 3.5;

    private final Map<Integer, List<Point3D>> rowMap;
    private final List<Point3D> pointsInFileOrder;
    private final String fileName;
    private final ALTextBlockFunctions alTextBlockFunctions = new ALTextBlockFunctions();
    public final Units units;
    public final BoundingBox theBoundingBox;

    private RPF(Map<Integer, List<Point3D>> rowMap, List<Point3D> pointsFromRPF, String fileName, Units units)
    {
        this.rowMap = rowMap;
        this.pointsInFileOrder = pointsFromRPF;
        this.fileName = fileName;
        this.units = units;
        this.theBoundingBox = setupBoundingBox();
    }

    @VisibleForTesting
    RPF(ImmutableList<Point3D> pointsFromRPF, String fileName, Units units)
    {
        this(ImmutableMap.copyOf(createRowMap(pointsFromRPF)), pointsFromRPF, fileName, units);
    }

    private BoundingBox setupBoundingBox()
    {
        double minX = getPointAt(0, 0).getX();
        double minY = getPointAt(0, 0).getY();
        double width = (getPointAt(getRowCount() -1, getColCount() -1).getX())-minX;
        double height = (getPointAt(getRowCount() -1, getColCount() -1).getY())-minY;

        return new BoundingBox(minX, minY, width, height);
    }

    /**
     * Creates an RPF instance from an RPF file if possible
     * @param filename the RPF path as a string
     * @return the RPF if possible
     */
    public static Optional<RPF> newInstance(String filename)
    {
        ImmutableList<Point3D> pointsFromRPF = createOrderedPointList(filename);
        pointsFromRPF = distinctList(pointsFromRPF);
        if (pointsFromRPF.equals(Collections.emptyList())){
            return Optional.empty();
        }

        int rowCount = countRowsInList(pointsFromRPF);
        //if no rows counted or the total points does not divide evenly by the row count
        if (rowCount == 0 || pointsFromRPF.size() % rowCount != 0){
            return Optional.empty();
        }

        Map<Integer, List<Point3D>> rowMap = createRowMap(pointsFromRPF);

        RPF tempRPF = new RPF(ImmutableMap.copyOf(rowMap), pointsFromRPF, filename, Units.INCHES);
        return Optional.of(new RPF(ImmutableMap.copyOf(rowMap), pointsFromRPF, filename, tempRPF.deriveUnits()));
    }

    private static Map<Integer, List<Point3D>> createRowMap(ImmutableList<Point3D> pointsFromFile)
    {
        int rowCount = countRowsInList(pointsFromFile);
        int pointsPerRow = pointsFromFile.size()/rowCount;
        Map<Integer, List<Point3D>> rowMap = new HashMap<>();

        for (int row = 0; row < rowCount; row++){
            ImmutableList<Point3D> pointsInRow = pointsFromFile.subList(row * pointsPerRow, (row * pointsPerRow) + pointsPerRow);
            if (row % 2 != 0){
                rowMap.put(row, pointsInRow.reverse());
            }
            else {
                rowMap.put(row, pointsInRow);
            }
        }

        return ImmutableMap.copyOf(rowMap);
    }

    public Point3D getPointAt(int row, int col){
        return rowMap.get(row).get(col);
    }

    int totalPoints()
    {
        return getRowCount() * getColCount();
    }

    /**
     * Gets a List of 3D points exactly as they appear in the plain text file version of the RPF
     */
    public List<Point3D> getPointsInFileOrder() {return pointsInFileOrder;}

    /**
     * Populates a Point3D list from a given RPF
     * @param filename the RPF
     * @return a list of points from an RPF file
     */
    @VisibleForTesting
    public static ImmutableList<Point3D> createOrderedPointList(String filename)
    {
        try (Stream<String> lineStream = Files.lines(Paths.get(filename))) {
            List<Point3D> pointsFromLine = lineStream.filter(RPF::isValidFormat)
                    .map(RPF::getDecimalNumbersInLine)
                    .map(doubles -> new Point3D(doubles.get(0), doubles.get(1), doubles.get(2)))
                    .collect(toList());

            return copyOf(pointsFromLine);
        }catch (IOException e) {
            LOGGER.ifPresent(logger -> logger.log(Level.WARNING, "Error reading RPF to a list of points"));
            return of();
        }
    }

    @VisibleForTesting
    static boolean isValidFormat(String rpfLine)
    {
        return getDecimalNumbersInLine(rpfLine).size() >= 3;
    }

    private static List<Double> getDecimalNumbersInLine(String rpfLine)
    {
        Matcher decimalMatcher = decimalGroups.matcher(rpfLine);
        List<Double> decimals = new ArrayList<>();

        while (decimalMatcher.find()){
            decimals.add(Double.parseDouble(decimalMatcher.group()));
        }

        return decimals;
    }

    /**
     * Counts the number of distinct Y values in a list of 3D points and assumes that number is
     * the number of rows
     * @param orderedPointList the list of points, usually generated from an RPF
     * @return the number of rows
     */
    @VisibleForTesting
    static int countRowsInList(List<Point3D> orderedPointList)
    {
        Long rowCount = IntStream.range(0, orderedPointList.size())
                .mapToDouble(num -> orderedPointList.get(num).getY())
                .mapToObj(yValue -> new BigDecimal(yValue).setScale(4, BigDecimal.ROUND_HALF_UP))
                .distinct()
                .count();

        return rowCount.intValue();
    }

    /**
     * Creates an RPF based on the original RPF but with as many points as desired.
     * This adds more detail to an RPF
     * @param columns the number of columns or points in each row desired.
     * Must be a larger number than the original number of columns.
     * The number of rows will be equal to the columns / ratio of the original RPF
     * @return a more detailed version of an RPF
     */
    public Optional<RPF> createExpandedRPF(int columns)
    {
        if (columns <= getColCount()){return Optional.empty();}

        double ratio = theBoundingBox.getWidth() / theBoundingBox.getHeight();
        int rows = (int)Math.ceil(columns / ratio);

        ImmutableMap.Builder<Integer, List<Point3D>> expandedRPF = ImmutableMap.builder();
        for (int row = 0; row < rows; row++){
            List<Point3D> point3DList = new ArrayList<>();
            for (int listOfPoints = 0; listOfPoints < columns; listOfPoints++){
                double firstx = getPointAt(0, 0).getX();
                double firsty = getPointAt(0, 0).getY();
                double xDiff = theBoundingBox.getWidth() / (columns - 1);
                double yDiff = theBoundingBox.getHeight() / (rows - 1);

                Point2D point2D = new Point2D(firstx + (listOfPoints * xDiff), firsty + (row * yDiff));
                List<Point3D> surroundingPoints = getSurroundingPoints(point2D);
                point3DList.add(AutolevellerUtil.biLinearInterpolate(surroundingPoints).apply(point2D));
            }
            expandedRPF.put(row, point3DList);
        }

        return Optional.of(new RPF(expandedRPF.build(), pointsInFileOrder, this.fileName, this.units));
    }

    /**
     * Returns a list of the 4 mesh points surrounding the input point. Each returned point
     * is bounded to the number of rows or columns within this Mesh.
     * index values: 0 = Top Left, 1 = Top Right, 2 = Bottom Left, 3 = Bottom Right
     * @param pointToFind The point to find the 4 surrounding points for
     * @return A list of 4 surrounding points
     */
    @VisibleForTesting
    public List<Point3D> getSurroundingPoints(Point2D pointToFind)
    {
        double firstx = getPointAt(0, 0).getX();
        double firsty = getPointAt(0, 0).getY();
        double xDiff = theBoundingBox.getWidth() / (getColCount() - 1);
        double yDiff = theBoundingBox.getHeight() / (getRowCount() - 1);

        //this is just a rearrangement of the standard arithmetic summation formula, i.e (a + (n - 1)d)
        int leftCol = (int)((pointToFind.getX() - firstx) / xDiff);
        int rightCol = leftCol + 1;
        int bottomRow = (int)((pointToFind.getY() - firsty) / yDiff);
        int topRow = bottomRow + 1;
        leftCol = Math.max(0, Math.min(leftCol, getColCount() - 1)); //limit to column range
        rightCol = Math.max(0, Math.min(rightCol, getColCount() - 1)); //limit to column range
        bottomRow = Math.max(0, Math.min(bottomRow, getRowCount() - 1)); //limit to row range
        topRow = Math.max(0, Math.min(topRow, getRowCount() - 1)); //limit to row range

        return of(getPointAt(topRow, leftCol), getPointAt(topRow, rightCol),
                getPointAt(bottomRow, leftCol), getPointAt(bottomRow, rightCol));
    }

    public Point3D highestPoint()
    {
        return pointsInFileOrder.stream()
                .max((p1, p2) -> Double.compare(p1.getZ(), p2.getZ()))
                .get();
    }

    public Point3D lowestPoint()
    {
        return pointsInFileOrder.stream()
                .min((p1, p2) -> Double.compare(p1.getZ(), p2.getZ()))
                .get();
    }

    public double fetchAverageZ()
    {
        return pointsInFileOrder.stream()
                .mapToDouble(Point3D::getZ)
                .average()
                .getAsDouble();
    }

    public double fetchHiLowZDiff()
    {
        return highestPoint().getZ() - lowestPoint().getZ();
    }

    public String getFileName(){return fileName;}

    static ImmutableList<Point3D> distinctList(List<Point3D> points)
    {
        //compare X and Y and return 0 if equal but keep the order
        Comparator<Point3D> byXandY = (Point3D p1, Point3D p2) -> {
            BigDecimal p1X = new BigDecimal(p1.getX());
            BigDecimal roundedP1Y = new BigDecimal(p1.getY()).setScale(4, BigDecimal.ROUND_HALF_UP);
            BigDecimal p2X = new BigDecimal(p2.getX());
            BigDecimal roundedP2Y = new BigDecimal(p2.getY()).setScale(4, BigDecimal.ROUND_HALF_UP);
            int xCompare = p1X.compareTo(p2X);
            int yCompare = roundedP1Y.compareTo(roundedP2Y);

            if (yCompare != 0){
                return 1;
            }

            return xCompare == 0 ? 0 : 1;
        };

        List<Point3D> distinctList = points.stream()
                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(byXandY)), ArrayList::new));

        return ImmutableList.copyOf(distinctList);
    }

    /**
     * Creates a height map image, where an interpolation of 2 colours represents the height
     * at any particular point. The image can be aplied to a 2D or 3D model
     * @param lowColor The color to represent a low point
     * @param lowHeight The value, below which the low color is at its most intense
     * @param hiColor The color to represent a high point
     * @param hiHeight The value, above which the high color is at its most intense
     * @return A 2D image which is a height map of the RPF (The more points in the RPF, the smoother the height map will be)
     */
    public Image createHeightMapImage(Color lowColor, double lowHeight, Color hiColor, double hiHeight)
    {
        WritableImage image = new WritableImage(getColCount(), getRowCount());
        PixelWriter pw = image.getPixelWriter();
        for (int row = 0; row < getRowCount(); row++){
            for (int col = 0; col < getColCount(); col++){
                double zHeight = getPointAt(row, col).getZ();
                double nomalizedHeight = AutolevellerUtil.normalize(zHeight, lowHeight, hiHeight);
                Color interpolatedColor = lowColor.interpolate(hiColor, nomalizedHeight);

                pw.setColor(col, ((getRowCount() - 1) - row), interpolatedColor);
            }
        }

        return image;
    }

    public int getRowCount() { return rowMap.size(); }

    public int getColCount() { return rowMap.get(0).size();}

    @Override
    public void writeLevelled(String outputFilename, ALModel alModel)
    {
        Mesh rpfMesh = new Mesh.Builder(this.units, alModel.readController(),
                new BigDecimal(this.theBoundingBox.getMinX()),
                new BigDecimal(this.theBoundingBox.getMinY()),
                new BigDecimal(this.theBoundingBox.getWidth()),
                new BigDecimal(this.theBoundingBox.getHeight()))
                .build();

        String initStr = alModel.readUseLvlZInit() ? alModel.readString(ALModel.CST_LVL_INIT_BLOCK) :
                ALTextBlockFunctions.zInitGCode(rpfMesh).stream().collect(Collectors.joining("\n"));

        Consumer<ALWriter> preText = writer -> {
            alTextBlockFunctions.prembleWriter().accept(writer);
            writer.writeGCODELine("");
            alTextBlockFunctions.safetyBlockWriter(this.units).accept(writer);
            writer.writeGCODELine("");
            writer.writeGCODELine("M0 (Attach probe wires and clips that need attaching)");
            writer.writeGCODELine(initStr);
            writer.writeGCODELine("M0 (Detach any clips used for probing)");
            writer.writeGCODELine("");
        };

        try {
            ALWriter.useALWriter(new PrintWriter(new BufferedWriter(new FileWriter(outputFilename, false))), preText);

            GCodeReader reader = PlainGCodeReader.newInstance(new FileReader(alModel.getOGF().getFilename()));
            GCodeProcessor writer = WriterProcessor.newInstance(Optional.empty(), outputFilename, true);
            GCodeProcessor leveller = RPFProcessor.newInstance(this, Optional.of(writer));
            GCodeProcessor arcSegmentor = ArcSegmentProcessor.newInstance(Optional.of(leveller),
                    alModel.readDouble(ALModel.MAX_SEG_LTH));
            GCodeProcessor lineSegmentor = LineSegmentProcessor.newInstance(Optional.of(arcSegmentor),
                    alModel.readDouble(ALModel.MAX_SEG_LTH));
            reader.readFile(lineSegmentor);
        } catch (IOException e) {
            LOGGER.ifPresent(logger -> logger.log(Level.SEVERE, "Error writing levelled file to disk"));
        }
    }

    @VisibleForTesting
    Units deriveUnits()
    {
        final double probeSpacing = getPointAt(0, 0).distance(getPointAt(0, 1));

        return probeSpacing <= probeSpacingUnitsLimit ? Units.INCHES : Units.MM;
    }
}
