/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import javafx.geometry.Point2D;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class SurroundingPointfromPlusMinusMeshTest
{
    private Mesh mesh;

    @Before
    public void setUp() throws Exception
    {
        mesh = new Mesh.Builder(Units.INCHES, Controller.TURBOCNC, new BigDecimal("-65.396"), new BigDecimal("-58.695"),
                new BigDecimal("125.562"), new BigDecimal("100.541"))
                .buildSpacing(14.5) // ySpaceSize = 16.75683, xSpaceSize = 15.69525
                .build();
    }

    @Test
    public void getPointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("-34.00550"), new BigDecimal("8.33232"), "#538");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("-18.31025"), new BigDecimal("8.33232"), "#539");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("-34.00550"), new BigDecimal("-8.42451"), "#529");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("-18.31025"), new BigDecimal("-8.42451"), "#530");

        Point2D pointToFind = new Point2D(-22.326, 4.32);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }

    @Test
    public void getLowerRightPointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("13.08025"), new BigDecimal("-41.93817"), "#514");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("28.77550"), new BigDecimal("-41.93817"), "#515");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("13.08025"), new BigDecimal("-58.69500"), "#505");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("28.77550"), new BigDecimal("-58.69500"), "#506");

        Point2D pointToFind = new Point2D(22.5487, -54);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3


        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }
}
