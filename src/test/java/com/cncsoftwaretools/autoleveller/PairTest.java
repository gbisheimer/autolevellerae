package com.cncsoftwaretools.autoleveller;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class PairTest
{
    @Test
    public void allEquals()
    {
        Pair<BigDecimal, BigDecimal> pair1 = Pair.createPair(BigDecimal.ONE, BigDecimal.valueOf(2.0));
        Pair<BigDecimal, BigDecimal> pair2 = Pair.createPair(BigDecimal.ONE, BigDecimal.valueOf(2.0));

        assertThat(pair1, equalTo(pair2));
        assertThat(pair2, equalTo(pair1));
        assertThat(pair1.hashCode(), equalTo(pair2.hashCode()));
        assertThat(pair2.hashCode(), equalTo(pair1.hashCode()));
    }

    @Test
    public void notAllEquals()
    {
        Pair<BigDecimal, BigDecimal> pair1 = Pair.createPair(BigDecimal.ONE, BigDecimal.valueOf(2.0));
        Pair<BigDecimal, BigDecimal> pair2 = Pair.createPair(BigDecimal.TEN, BigDecimal.valueOf(2.0));

        assertThat(pair1, not(equalTo(pair2)));
        assertThat(pair2, not(equalTo(pair1)));
        assertThat(pair1.hashCode(), not(equalTo(pair2.hashCode())));
        assertThat(pair2.hashCode(), not(equalTo(pair1.hashCode())));
    }

    @Test
    public void testToString()
    {
        Pair<BigDecimal, BigDecimal> pair1 = Pair.createPair(BigDecimal.ONE, BigDecimal.valueOf(2.0));
        Pair<BigDecimal, BigDecimal> pair2 = Pair.createPair(BigDecimal.TEN, BigDecimal.valueOf(2.0));

        assertThat(pair1.toString(), equalTo("Pair{first=1, second=2.0}"));
        assertThat(pair2.toString(), equalTo("Pair{first=10, second=2.0}"));
    }
}