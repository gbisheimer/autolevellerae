package com.cncsoftwaretools.autoleveller;


import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class VersionTest
{
    @Test
    public void compareVersions_sameVersionDifferentName()
    {
        Version aVersion = MajorMinor.newInstance("anApp", 1, 1.1);

        Version sameVersion = MajorMinor.newInstance("anotherApp", 1, 1.1);

        assertThat(aVersion.compareVersions(sameVersion), equalTo(0));
        assertThat(aVersion.equals(sameVersion), is(false));
    }

    @Test
    public void compareVersions_argSmallerMajor()
    {
        Version aVersion = MajorMinor.newInstance(2, 1.1);

        Version otherVersion = MajorMinor.newInstance(1, 1.1);

        assertThat(aVersion.compareVersions(otherVersion), equalTo(1));

    }

    @Test
    public void compareVersions_argSmallerMinor()
    {
        Version aVersion = MajorMinor.newInstance(2, 1.12);

        Version otherVersion = MajorMinor.newInstance(2, 1.1);

        assertThat(aVersion.compareVersions(otherVersion), equalTo(1));

    }

    @Test
    public void compareVersions_argBiggerMinor()
    {
        Version aVersion = MajorMinor.newInstance(3, 1.5);

        Version otherVersion = MajorMinor.newInstance(3, 1.6);

        assertThat(aVersion.compareVersions(otherVersion), equalTo(-1));

    }

    @Test
    public void compareUpdateVersionsWhereOtherIsHigherUpdateNum_ShouldReturnMinus1()
    {
        Version aVersion = UpdateVer.newInstance("AE", 1, 1.1, 1);

        Version otherVersion = UpdateVer.newInstance("AE", 1, 1.1, 2);

        assertThat(aVersion.compareVersions(otherVersion), equalTo(-1));

    }

    @Test
    public void compareUpdateVersionsWhereOtherIsLowerUpdateNum_ShouldReturnPlus1()
    {
        Version aVersion = UpdateVer.newInstance("AE", 1, 1.1, 5);

        Version otherVersion = UpdateVer.newInstance("AE", 1, 1.1, 4);

        assertThat(aVersion.compareVersions(otherVersion), equalTo(1));

    }
}
