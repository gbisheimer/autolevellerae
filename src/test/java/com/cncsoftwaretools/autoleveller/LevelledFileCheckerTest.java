package com.cncsoftwaretools.autoleveller;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class LevelledFileCheckerTest
{
    private OGF ogf;
    private RPF rpf;
    private Mesh mesh;

    @Before
    public void setUp() throws Exception
    {
        ogf = null;
        rpf = null;
        mesh = null;
    }

    @Test
    public void incompleteModel()
    {
        assertThat(isReadyForWriting(), is(false));
    }

    @Test
    public void completeModel()
    {
        Optional<OGF> ogf = OGF.newInstance("src/test/spindle.top.etchemcmm.ngc");
        Mesh mesh = new Mesh.Builder(ogf.get().getUnits(), Controller.LINUXCNC,
                BigDecimal.valueOf(ogf.get().getSize().getX()), BigDecimal.valueOf(ogf.get().getSize().getY()),
                BigDecimal.valueOf(ogf.get().getSize().getWidth()), BigDecimal.valueOf(ogf.get().getSize().getHeight()))
                .build();

        this.ogf = ogf.get();
        this.mesh = mesh;

        assertThat( isReadyForWriting(), is(true));
    }

    private boolean isReadyForWriting()
    {
        return Optional.ofNullable(ogf).isPresent() &&
                (Optional.ofNullable(rpf).isPresent() || Optional.ofNullable(mesh).isPresent());
    }
}