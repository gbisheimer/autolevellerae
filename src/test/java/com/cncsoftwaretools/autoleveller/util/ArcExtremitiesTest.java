package com.cncsoftwaretools.autoleveller.util;

import javafx.geometry.Point2D;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class ArcExtremitiesTest
{
    @Test
    public void givenStartEndPointDoNotCrossCircleLimits_ExtremitiesShouldBeSameAsStartEndPoints()
    {
        Arc arc = Arc.newInstance(new Point2D(-7.22402, 33.25102),
                new Point2D(8.61904, 44.81582), new Point2D(17, 16.7), Arc.Direction.CW);
        Map<Arc.Coords, BigDecimal> arcMinMaxCoords = arc.getExtremities();

        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("8.61904")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-7.22402")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("44.81582")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("33.25102")));
    }

    @Test
    public void givenStartEndPointDoCrossLeftmostCircleLimits_MinXShouldBeSameAsLeftmostLimits()
    {
        Arc arc = Arc.newInstance(new Point2D(-4.18431, -3.5969),
                new Point2D(-7.22402, 33.25102), new Point2D(17, 16.7), Arc.Direction.CW);
        Map<Arc.Coords, BigDecimal> arcMinMaxCoords = arc.getExtremities();

        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-4.18431")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-12.33836")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("33.25102")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-3.59690")));
    }

    @Test
    public void givenArcIsAlmostFullCircle_shouldInclude3LimitPointsInMinMaxCoords()
    {
        Arc arc = Arc.newInstance(new Point2D(-4.18431, -3.5969),
                new Point2D(32.4999, -8.20968), new Point2D(17, 16.7), Arc.Direction.CW);
        Map<Arc.Coords, BigDecimal> arcMinMaxCoords = arc.getExtremities();

        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("46.33836")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-12.33836")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("46.03836")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-8.20968")));
    }

    @Test
    public void givenArcIsCounterClockwise_ArcMinMaxShouldCoverASmallArc()
    {
        Arc arc = Arc.newInstance(new Point2D(-4.18431, -3.5969),
                new Point2D(32.4999, -8.20968), new Point2D(17, 16.7), Arc.Direction.CCW);
        Map<Arc.Coords, BigDecimal> arcMinMaxCoords = arc.getExtremities();

        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("32.49990")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINX).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-4.18431")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MAXY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-3.59690")));
        assertThat(arcMinMaxCoords.get(Arc.Coords.MINY).setScale(5, BigDecimal.ROUND_HALF_UP),
                equalTo(new BigDecimal("-12.63836")));
    }

    @Test
    public void getAngularDistancesBetweenStartAndCircleLimitPoints()
    {
        Arc arc = Arc.newInstance(new Point2D(-4.18431, -3.5969),
                new Point2D(-7.22402, 33.25102), new Point2D(17, 16.7), Arc.Direction.CW);
        Map<Point2D, BigDecimal> angularDistances = arc.getAngularDistanceToLimitPointsFromStart();

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(arc.getCentrePoint().getX() - arc.getRadius().doubleValue(), 16.7))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(44L));

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(17, arc.getCentrePoint().getY() + arc.getRadius().doubleValue()))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(134L));

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(arc.getCentrePoint().getX() + arc.getRadius().doubleValue(), 16.7))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(224L));

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(17, arc.getCentrePoint().getY() - arc.getRadius().doubleValue()))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(314L));
    }

    @Test
    public void getAngularDistancesBetweenStartAndCircleLimitPoints_counterClockwise()
    {
        Arc arc = Arc.newInstance(new Point2D(-4.18431, -3.5969),
                new Point2D(-7.22402, 33.25102), new Point2D(17, 16.7), Arc.Direction.CCW);
        Map<Point2D, BigDecimal> angularDistances = arc.getAngularDistanceToLimitPointsFromStart();

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(arc.getCentrePoint().getX() - arc.getRadius().doubleValue(), 16.7))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(316L));

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(17, arc.getCentrePoint().getY() + arc.getRadius().doubleValue()))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(226L));

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(arc.getCentrePoint().getX() + arc.getRadius().doubleValue(), 16.7))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(136L));

        assertThat(Math.round(Math.toDegrees(angularDistances.get(
                new Point2D(17, arc.getCentrePoint().getY() - arc.getRadius().doubleValue()))
                .setScale(5, BigDecimal.ROUND_HALF_UP).doubleValue())), equalTo(46L));
    }
}
