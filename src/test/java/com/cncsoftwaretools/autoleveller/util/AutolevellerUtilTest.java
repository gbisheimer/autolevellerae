package com.cncsoftwaretools.autoleveller.util;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AutolevellerUtilTest
{
    @Test
    public void normalize_scaleBetween1and0()
    {
        assertThat(AutolevellerUtil.normalize(0.7, -2, 2), equalTo(0.675));
        assertThat(AutolevellerUtil.normalize(2.2, -2, 2), equalTo(1.0));
        assertThat(AutolevellerUtil.normalize(-5.567, -2, 2), equalTo(0.0));
        assertThat(AutolevellerUtil.normalize(0, -2, 2), equalTo(0.5));
    }

    @Test
    public void partitionEmptyList_shouldreturnEmpty()
    {
        assertThat(AutolevellerUtil.partitionList(new ArrayList<Integer>(), i -> i % 2 == 0),
                equalTo(ImmutableList.of()));
    }

    @Test
    public void partitionListWhereNothingMatchedThePredicate_shouldReturnInputList()
    {
        assertThat(AutolevellerUtil.partitionList(ImmutableList.of(1, 2, 3, 4, 5, 6, 7, 8), i -> i > 10),
                equalTo(ImmutableList.of(
                        ImmutableList.of(1, 2, 3, 4, 5, 6, 7, 8))));
    }

    @Test
    public void partitionIntListByPredicate_shouldReturnListPartitionedInOrder()
    {
        assertThat(AutolevellerUtil.partitionList(ImmutableList.of(1, 2, 3, 4, 5, 6, 7, 8), i -> i % 2 == 0),
                equalTo(ImmutableList.of(
                        ImmutableList.of(1, 2),
                        ImmutableList.of(3, 4),
                        ImmutableList.of(5, 6),
                        ImmutableList.of(7, 8))));
    }

    @Test
    public void partitionStringListWherePartitionIsIfStringIsLength2_shouldIncludeAllRemainingStrings()
    {
        ImmutableList<String> sentence = ImmutableList.of("a", "aa", "and", "another", "thing", "is", "everything", "and",
                "nothing", "so", "endeth", "the", "lesson");

        assertThat(AutolevellerUtil.partitionList(sentence, s -> s.length() == 2), equalTo(ImmutableList.of(
                ImmutableList.of("a", "aa"),
                ImmutableList.of("and", "another", "thing", "is"),
                ImmutableList.of("everything", "and", "nothing", "so"),
                ImmutableList.of("endeth", "the", "lesson"))));
    }


}
