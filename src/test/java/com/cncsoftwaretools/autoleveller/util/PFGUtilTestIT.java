package com.cncsoftwaretools.autoleveller.util;

import com.cncsoftwaretools.autoleveller.reader.Word;
import javafx.geometry.Point2D;
import org.junit.Test;

import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

/**
 * Created by James Hawthorne on 23/11/2017.
 */
public class PFGUtilTestIT
{
    private final String filename = "src/test/PFG~36x22.gcd";
    private final String regexStr = ".*X(" + Word.DECIMALPATTERN.pattern() + ")\\s{0,3}Y(" + Word.DECIMALPATTERN.pattern() + ").*";
    private final List<String> subLines = AutolevellerUtil.getFileLinesFromTo(filename, "M90", "M91");
    private final Function<String, Point2D> mapTextToPoint2D = line -> {
        Matcher m = Pattern.compile(regexStr).matcher(line);
        //noinspection ResultOfMethodCallIgnored
        m.find();

        return new Point2D(Double.valueOf(m.group(1)), Double.valueOf(m.group(3)));
    };

    @Test
    public void getLinesFromFileBeforeAndAfter_shouldReturnAllLinesFromFileBetweenFromAndTo()
    {
        assertThat(subLines.get(0), equalTo("M90"));
        assertThat(subLines.get(subLines.size()-1), equalTo("G0 Z2"));
    }

    @Test
    public void filterFileForLinesThatMatchRegex_shouldResultInAListOfValidLines()
    {
        List<String> lineMatches = AutolevellerUtil.getLinesMatchingRegex(subLines, regexStr);

        assertThat(lineMatches.size(), equalTo(40));
    }

    @Test
    public void convertStringXYTo2DPoints()
    {
        List<String> lineMatches = AutolevellerUtil.getLinesMatchingRegex(subLines, regexStr);
        List<Point2D> points = lineMatches.stream().map(mapTextToPoint2D).collect(toList());

        assertThat(points, hasItem(new Point2D(5.19227, 16.89432)));
    }

    @Test
    public void getDistinctXYs()
    {
        List<String> lineMatches = AutolevellerUtil.getLinesMatchingRegex(subLines, regexStr);
        List<Point2D> points = lineMatches.stream().map(mapTextToPoint2D).collect(toList());
        long distinctXs = points.stream().map(Point2D::getX).distinct().count();
        long distinctYs = points.stream().map(Point2D::getY).distinct().count();

        assertThat(distinctXs, equalTo(8L));
        assertThat(distinctYs, equalTo(5L));
    }
}
