package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.ui.ALModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.function.Consumer;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MockFileWriterTest
{
    @Mock
    private PrintWriter writer;

    @Test
    public void writeComment() throws Exception
    {
        ALWriter.useALWriter(writer, alwriter -> alwriter.writeGCODELine("(aComment)"));

        verify(writer, times(1)).println("(aComment)");
        verify(writer, times(1)).close();
    }

    @Test
    public void writeFourComments() throws Exception
    {
        Consumer<ALWriter> aCommentConsumer = alwriter -> {
            alwriter.writeGCODELine("(aComment)");
            alwriter.writeGCODELine("(aComment2)");
            alwriter.writeGCODELine("(aComment3)");
            alwriter.writeGCODELine("(aComment4)");
        };

        ALWriter.useALWriter(writer, aCommentConsumer);

        verify(writer, times(4)).println(anyString());
        verify(writer, times(1)).close();
    }

    @Test
    public void moveNProbeIndividualPoint()
    {
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.LINUXCNC, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.TEN)
                .build();

        ALWriter.useALWriter(writer, mesh.moveNprobe(ThreeDPoint.createPoint(BigDecimal.valueOf(3.56), BigDecimal.valueOf(5.76), "#45")));
        
        verify(writer).println("G0 Z2");
        verify(writer).println("G1 X" + Autoleveller.DF.format(3.56) + " Y" + Autoleveller.DF.format(5.76) + " F600");
        verify(writer).println("G38.2 Z-1 F100");
        verify(writer, times(1)).close();
    }

    @Test
    public void writePFG_SmallMesh()
    {
        PFGWriter pfgWriter = new Mesh.Builder(Units.MM, Controller.LINUXCNC, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(75.25), BigDecimal.valueOf(53.5)).build();

        Consumer<ALWriter> pfgContent = pfgWriter.writePFG();

        ALWriter.useALWriter(writer, pfgContent);

        verify(writer, times(1)).println("M0 (Attach probe wires and clips that need attaching)");
        verify(writer, times(1)).println("M0 (Detach any clips used for probing)");
        verify(writer, times(1)).close();
    }

    @Test
    public void writingPFGWithCustomController_shouldProbeWithCustomWordsAndCommands()
    {
        ALModel model = ALModel.getInstance();
        Mesh pfgWriter = new Mesh.Builder(Units.MM, Controller.CUSTOM, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(75.25), BigDecimal.valueOf(53.5)).build();

        final int totalPoints = pfgWriter.getRowCount() * pfgWriter.getColCount();
        model.writeString(ALModel.CST_PROBE_WORD, "G76");
        model.writeString(ALModel.CST_PROBE_CMD, "M4000");
        model.writeString(ALModel.CST_OPEN_LOG, "M90");
        model.writeString(ALModel.CST_CLOSE_LOG, "M91");
        model.writeString(ALModel.USE_CST_PFG_INIT, "false");

        Consumer<ALWriter> pfgContent = pfgWriter.writePFG();

        ALWriter.useALWriter(writer, pfgContent);

        verify(writer, times(totalPoints)).println("G76 Z-1 F100"); //the extra comments in this probe line mean the init part is ignored
        verify(writer, times(totalPoints)).println("M4000");
        verify(writer, times(1)).println("M90");
        verify(writer, times(1)).println("M91");
        verify(writer, times(1)).close();
    }
}
