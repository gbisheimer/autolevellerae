package com.cncsoftwaretools.autoleveller;

import javafx.geometry.Point2D;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class MeshOffsetPointTest
{
    private Mesh mesh;

    @Before
    public void setUp() throws Exception
    {
        mesh = new Mesh.Builder(Units.INCHES, Controller.TURBOCNC, new BigDecimal("0.56"), new BigDecimal("0.345"),
                new BigDecimal("6.567"), new BigDecimal("4.564"))
                .buildSpacing(0.678) // ySpaceSize = 0.76067, xSpaceSize = 0.72967
                .build();
    }

    @Test
    public void getPointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("5.66769"), new BigDecimal("2.62701"), "#537");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("6.39736"), new BigDecimal("2.62701"), "#538");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("5.66769"), new BigDecimal("1.86634"), "#527");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("6.39736"), new BigDecimal("1.86634"), "#528");

        Point2D pointToFind = new Point2D(5.8, 2.2678);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }

    @Test
    public void getUpperPointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("1.28967"), new BigDecimal("4.14835"), "#551");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("2.01934"), new BigDecimal("4.14835"), "#552");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("1.28967"), new BigDecimal("3.38768"), "#541");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("2.01934"), new BigDecimal("3.38768"), "#542");

        Point2D pointToFind = new Point2D(1.754, 3.9864);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }

    @Test
    public void getPointsOnLineFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("1.28967"), new BigDecimal("3.38768"), "#541");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("2.01934"), new BigDecimal("3.38768"), "#542");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("1.28967"), new BigDecimal("3.38768"), "#541");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("2.01934"), new BigDecimal("3.38768"), "#542");

        Point2D pointToFind = new Point2D(1.45, 3.38768);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }
}
