/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.util.AutolevellerUtil;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class OutputFilenameTest
{
    @Test
    public void generateNameNoExt()
    {
        String generatedFilename = AutolevellerUtil.generateOutputFilename("TestFile", Controller.MACH3);

        assertThat(generatedFilename, equalTo("ALTestFile.nc"));
    }

    @Test
    public void generateNameFromExt()
    {
        String generatedFilename = AutolevellerUtil.generateOutputFilename("TestFile.nc", Controller.LINUXCNC);

        assertThat(generatedFilename, equalTo("ALTestFile.ngc"));
    }

    @Test
    public void generateNameFromExtTurbo()
    {
        String generatedFilename = AutolevellerUtil.generateOutputFilename("turboFile.ext", Controller.TURBOCNC);

        assertThat(generatedFilename, equalTo("ALturboFile.txt"));
    }
}