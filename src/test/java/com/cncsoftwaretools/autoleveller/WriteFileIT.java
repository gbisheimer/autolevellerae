package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.ui.ALModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.function.Consumer;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WriteFileIT
{
    private final String testFolder = "src/test/";
    private final ALTextBlockFunctions alText = new ALTextBlockFunctions();
    private static final long PREAMBLELINES = 6L;
    private static final long MESHCHECKLISTLINES = 11L;

    @Mock
    private ALModel mockModel;

    @Before
    public void setUp() throws Exception
    {
        when(mockModel.readController()).thenReturn(Controller.LINUXCNC);
        when(mockModel.readDouble(ALModel.MAX_SEG_LTH)).thenReturn(5.0);
    }

    @Test
    public void writePhysicalComment() throws Exception
    {
        Consumer<ALWriter> aCommentConsumer = alwriter -> alwriter.writeGCODELine("(aComment)");
        ALWriter.useALWriter(getPrintWriter(testFolder + "commentTestFile.txt"), aCommentConsumer);

        BufferedReader reader = new BufferedReader(new FileReader(testFolder + "commentTestFile.txt"));
        String lineOne = reader.readLine();
        reader.close();

        assertThat(lineOne, equalTo("(aComment)"));
    }

    @Test
    public void write3PhysicalComment() throws Exception
    {
        String fileName = testFolder + "commentTestFile.txt";

        Consumer<ALWriter> aCommentConsumer = alwriter -> {
            alwriter.writeGCODELine("(aComment)");
            alwriter.writeGCODELine("(aComment2)");
            alwriter.writeGCODELine("(aComment3)");
        };
        ALWriter.useALWriter(getPrintWriter(fileName), aCommentConsumer);

        try(BufferedReader reader = new BufferedReader(new FileReader(testFolder + "commentTestFile.txt"))){

	        String line = reader.readLine();
	        assertThat(line, equalTo("(aComment)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("(aComment2)"));
	
	        line = reader.readLine();
	        reader.close();
	        assertThat(line, equalTo("(aComment3)"));
        }
    }

    @Test
    public void writePreamble() throws IOException
    {
        ALWriter.useALWriter(getPrintWriter(testFolder + "preAmbleTestFile.txt"), alText.prembleWriter());

        Path location = Paths.get(testFolder, "preAmbleTestFile.txt");
        try(Stream<String> preambleFile = Files.lines(location))
        {
            long lines = preambleFile.count();
            assertThat(lines, equalTo(PREAMBLELINES));
        }
    }

    @Test
    public void writeProbeChecklist() throws IOException
    {
        ALWriter.useALWriter(getPrintWriter(testFolder + "meshTestFile.txt"), alText.probeInstructionsWriter());

        Path location = Paths.get(testFolder, "meshTestFile.txt");
        try(Stream<String> probeFile = Files.lines(location))
        {
            long lines = probeFile.count();
            assertThat(lines, equalTo(MESHCHECKLISTLINES));
        }
    }

    @Test
    public void writemultiFunction() throws IOException
    {
        Consumer<ALWriter> comboWriter = writer -> {
            alText.prembleWriter().accept(writer);
            writer.writeGCODELine("");
            alText.probeInstructionsWriter().accept(writer);
        };

        ALWriter.useALWriter(getPrintWriter(testFolder + "meshTestFile.txt"), comboWriter);

        Path location = Paths.get(testFolder, "meshTestFile.txt");
        try(Stream<String> probeFile = Files.lines(location))
        {
            long lines = probeFile.count();
            assertThat(lines, equalTo(PREAMBLELINES + MESHCHECKLISTLINES + 1));
        }
    }

    @Test
    public void writeProbeInit() throws IOException
    {
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.LINUXCNC, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.TEN)
                .build();
        ALWriter.useALWriter(getPrintWriter(testFolder + "meshTestFile.txt"), alText.probeInitWriter(mesh));

        try(BufferedReader reader = new BufferedReader(new FileReader(testFolder + "meshTestFile.txt"))){

	        String line = reader.readLine();
	        assertThat(line, equalTo("(Initialize probe routine)"));
            
	        line = reader.readLine();
	        assertThat(line, equalTo("G0 Z25 (Move clear of the board first)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G1 X0 Y0 F600 (Move to bottom left corner)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G0 Z2 (Quick move to probe clearance height)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G38.2 Z-1 F100 (Probe to a maximum of the specified probe height at the specified feed rate)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G92 Z0 (Touch off Z to 0 once contact is made)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G0 Z2 (Move Z to above the contact point)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G38.2 Z-1 F50 (Repeat at a more accurate slower rate)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G92 Z0"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G0 Z2"));
        }
    }

    @Test
    public void writeProbeInitCustomController() throws IOException
    {
        ALModel model = ALModel.getInstance();
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.CUSTOM, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.TEN)
                .build();
        model.writeString(ALModel.CST_PROBE_WORD, "G89.7");
        model.writeString(ALModel.CST_PROBE_CMD, "M5000");
        model.writeString(ALModel.CST_ZERO_WORD, "G100");
        model.writeString(ALModel.CST_ZERO_CMD, "M5001");
        ALWriter.useALWriter(getPrintWriter(testFolder + "meshTestFile.txt"), alText.probeInitWriter(mesh));

        try(BufferedReader reader = new BufferedReader(new FileReader(testFolder + "meshTestFile.txt"))){

            String line = reader.readLine();
            assertThat(line, equalTo("(Initialize probe routine)"));

            line = reader.readLine();
            assertThat(line, equalTo("G0 Z25 (Move clear of the board first)"));

            line = reader.readLine();
            assertThat(line, equalTo("G1 X0 Y0 F600 (Move to bottom left corner)"));

            line = reader.readLine();
            assertThat(line, equalTo("G0 Z2 (Quick move to probe clearance height)"));

            line = reader.readLine();
            assertThat(line, equalTo("G89.7 Z-1 F100 (Probe to a maximum of the specified probe height at the specified feed rate)"));

            line = reader.readLine();
            assertThat(line, equalTo("M5000"));

            line = reader.readLine();
            assertThat(line, equalTo("G100 Z0 (Touch off Z to 0 once contact is made)"));

            line = reader.readLine();
            assertThat(line, equalTo("M5001"));

            line = reader.readLine();
            assertThat(line, equalTo("G0 Z2 (Move Z to above the contact point)"));

            line = reader.readLine();
            assertThat(line, equalTo("G89.7 Z-1 F50 (Repeat at a more accurate slower rate)"));

            line = reader.readLine();
            assertThat(line, equalTo("M5000"));

            line = reader.readLine();
            assertThat(line, equalTo("G100 Z0"));

            line = reader.readLine();
            assertThat(line, equalTo("M5001"));

            line = reader.readLine();
            assertThat(line, equalTo("G0 Z2"));
        }
    }

    @Test
    public void writeProbeInitNegativeMach3() throws IOException
    {
        Mesh mesh = new Mesh.Builder(Units.INCHES, Controller.MACH3, BigDecimal.valueOf(-3.56), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.valueOf(4.56755))
                .build();
        ALWriter.useALWriter(getPrintWriter(testFolder + "meshTestFile.txt"), alText.probeInitWriter(mesh));

        DecimalFormat df = Autoleveller.DF;

        try(BufferedReader reader = new BufferedReader(new FileReader(testFolder + "meshTestFile.txt"))){
            String line = reader.readLine();
            assertThat(line, equalTo("(Initialize probe routine)"));

            line = reader.readLine();
            assertThat(line, equalTo("G0 Z" + df.format(1.5) + " (Move clear of the board first)"));

	        line = reader.readLine();
	        assertThat(line, equalTo("G1 X" + df.format(-3.56) + " Y0 F35 (Move to bottom left corner)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G0 Z" + df.format(0.0787) + " (Quick move to probe clearance height)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G31 Z" + df.format(-0.0625) + " F5 (Probe to a maximum of the specified probe height at the specified feed rate)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G92 Z0 (Touch off Z to 0 once contact is made)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G0 Z" + df.format(0.0787) + " (Move Z to above the contact point)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G31 Z" + df.format(-0.0625) + " F" + df.format(2.5) + " (Repeat at a more accurate slower rate)"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G92 Z0"));
	
	        line = reader.readLine();
	        assertThat(line, equalTo("G0 Z" + df.format(0.0787)));
        }
    }

    @Test
    public void writeProbeRoutine() throws IOException
    {
        final Mesh mesh = new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.valueOf(100), BigDecimal.valueOf(80))
                .buildSpacing(10)
                .build();

        Consumer<ALWriter> fileWriter = writer -> mesh.meshApplier(point ->  mesh.moveNprobe(point).accept(writer)).accept(writer);

        ALWriter.useALWriter(getPrintWriter(testFolder + "meshTestFile.txt"), fileWriter);

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/meshTestFile.txt"))){
            assertThat(br.lines().count(), equalTo((long)(11 * 9 * 3))); //8+1 rows including X0 * 10+1 columns including Y0 * 3 probe lines per point
        }
    }

    @Test
    public void writeProbeRoutineWithLogAndParams() throws IOException
    {
        final Mesh mesh = new Mesh.Builder(Units.INCHES, Controller.LINUXCNC, BigDecimal.valueOf(-4.455), BigDecimal.ZERO, BigDecimal.valueOf(4.455), BigDecimal.valueOf(3.6789))
                .buildSpacing(0.375)
                .build();

        Consumer<ALWriter> fileWriter = writer -> {
            writer.writeGCODELine(mesh.getController().getOpenLogCommand());
            mesh.meshApplier(threeDPoint -> {
                mesh.moveNprobe(threeDPoint).accept(writer);
                writer.writeGCODELine(threeDPoint.getZValue() + "=" + mesh.getController().getCurrentZparameter());
            }).accept(writer);
            writer.writeGCODELine("G0 Z" + mesh.getProbeClearance());
            writer.writeGCODELine(mesh.getController().getCloseLogCommand());
        };

        ALWriter.useALWriter(getPrintWriter(testFolder + "meshTestFile.txt"), fileWriter);

        try(BufferedReader br=new BufferedReader(new FileReader(testFolder.concat("meshTestFile.txt")))){
            assertThat(br.lines().findFirst().get(), equalTo("(PROBEOPEN RawProbeLog.txt)"));
        }

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/meshTestFile.txt"))){
            // 9+1 rows including X0 * 11+1 columns including Y0 * 3 probe lines per point + 120 parameter lines +
            // 1 final clearance command + 2 open / close log file
            assertThat(br.lines().count(), equalTo((long)(12 * 10 * 3 + 120 + 1 + 2)));
        }

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/meshTestFile.txt"))){
            assertThat(br.lines().reduce((previous, current) -> current).get(), equalTo("(PROBECLOSE)"));
        }
    }

    @Test
    public void writeLevelled_smallRPF() throws IOException
    {
        OGF ogf = OGF.newInstance("src/test/SerialNoText.ngc").get();
        mockModel.setOGF(ogf);
        when(mockModel.getOGF()).thenReturn(ogf);
        LevellerWriter levellerWriter = RPF.newInstance("src/test/ALProbeEMCSmall.test").get();

        levellerWriter.writeLevelled("src/test/outFile.txt", mockModel);

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/outFile.txt"))){
            assertThat(br.lines()
                            .filter(line -> line.equals("M0 (Attach probe wires and clips that need attaching)") ||
                                    line.equals("M0 (Detach any clips used for probing)"))
                            .collect(toList())
                            .size(),
                    equalTo(2));
        }
    }

    @Test
    public void writeLevelled_smallMesh() throws IOException
    {
        OGF ogf = OGF.newInstance("src/test/SerialNoText.ngc").get();
        mockModel.setOGF(ogf);
        when(mockModel.getOGF()).thenReturn(ogf);
        LevellerWriter levellerWriter = new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.valueOf(100), BigDecimal.valueOf(80))
            .buildSpacing(10)
            .build();

        levellerWriter.writeLevelled("src/test/outFile.txt", mockModel);

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/outFile.txt"))){
            assertThat(br.lines()
                    .filter(line -> line.equals("M0 (Attach probe wires and clips that need attaching)") ||
                            line.equals("M0 (Detach any clips used for probing)"))
                    .collect(toList())
                    .size(),
            equalTo(2));
        }
    }

    private PrintWriter getPrintWriter(String file) throws IOException
    {
        return new PrintWriter(new BufferedWriter(new FileWriter(file, false)));
    }
}
