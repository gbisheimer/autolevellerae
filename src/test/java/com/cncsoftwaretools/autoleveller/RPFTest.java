/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.google.common.collect.ImmutableList;
import javafx.geometry.Point3D;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 *
 * Created by James Hawthorne on 28/09/2016.
 */
public class RPFTest
{
    @Test
    public void rpfBoundingBoxWidthAndHeightShouldBeMeasuredFromStartingPoint()
    {
        ImmutableList<Point3D> probedPoints = ImmutableList.of(
                new Point3D(-10, -20, 0),
                new Point3D(0, -20, 0),
                new Point3D(10, -20, 0),
                new Point3D(10, -10, 0),
                new Point3D(0, -10, 0),
                new Point3D(-10, -10, 0),
                new Point3D(-10, 0, 0),
                new Point3D(0, 0, 0),
                new Point3D(10, 0, 0),
                new Point3D(10, 10, 0),
                new Point3D(0, 10, 0),
                new Point3D(-10, 10, 0));

        RPF rpfWithCustomPoints = new RPF(probedPoints, "none", Units.MM);

        assertThat(rpfWithCustomPoints.theBoundingBox.getMinX(), equalTo(-10.0));
        assertThat(rpfWithCustomPoints.theBoundingBox.getMinY(), equalTo(-20.0));
        assertThat(rpfWithCustomPoints.theBoundingBox.getWidth(), equalTo(20.0));
        assertThat(rpfWithCustomPoints.theBoundingBox.getHeight(), equalTo(30.0));
    }
}
