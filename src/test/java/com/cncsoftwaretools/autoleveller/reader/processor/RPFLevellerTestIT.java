package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.RPF;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RPFLevellerTestIT
{
    private RPF rpf = RPF.newInstance("src/test/ALProbeEMCSmall.test").get();

    @Test
    public void rpfProcessor_create()
    {
        GCodeInterpreter rpfLeveller = RPFProcessor.newInstance(rpf, Optional.empty());

        assertThat(rpfLeveller.nextProcessor(), equalTo(Optional.empty()));
    }

    @Test
    public void interpolateState_noX()
    {
        RPFProcessor rpfLeveller = RPFProcessor.newInstance(rpf, Optional.empty());
        GCodeState state = GCodeState.newInstance(Optional.empty(), "N310G1Y0.0648 Z-0.05825F40");

        GCodeState modifiedState = rpfLeveller.interpolateState(state);

        assertThat(modifiedState, equalTo(state));
    }

    @Test
    public void interpolateState_realZ()
    {
        RPFProcessor rpfLeveller = RPFProcessor.newInstance(rpf, Optional.empty());
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N200G0X32.9Y-0.3092Z0.2");
        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "N210G1Y49.07654 Z-0.05825F40");
        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "N220G1Z-0.02F40");

        GCodeState modifiedState = rpfLeveller.interpolateState(state3);

        assertThat(modifiedState, equalTo(GCodeState.newInstance(Optional.of(state2), "N220G1Z0.08084F40")));
    }
}
