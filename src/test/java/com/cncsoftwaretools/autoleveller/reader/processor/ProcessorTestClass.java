package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class ProcessorTestClass extends GCodeInterpreter
{
    private int callCount = 0;
    private GCodeState lastState;
    private Optional<GCodeProcessor> nextProcessor = Optional.empty();
    private final Function<GCodeState, List<GCodeState>> additionalProcess;

    public ProcessorTestClass(){additionalProcess = gCodeState -> ImmutableList.of();}

    public ProcessorTestClass(GCodeProcessor nextProcessor, Function<GCodeState, List<GCodeState>> additionalProcess)
    {
        this.nextProcessor = Optional.of(nextProcessor);
        this.additionalProcess = additionalProcess;
    }

    @Override
    public List<GCodeState> processState(GCodeState currentState)
    {
        callCount++;
        lastState = currentState;

        return additionalProcess.apply(currentState);
    }

    @Override
    public Optional<GCodeProcessor> nextProcessor()
    {
        return nextProcessor;
    }

    public int processStateCount()
    {
        return callCount;
    }

    public GCodeState getLastState()
    {
        return lastState;
    }
}
