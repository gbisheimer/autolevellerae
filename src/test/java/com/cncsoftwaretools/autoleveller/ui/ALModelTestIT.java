package com.cncsoftwaretools.autoleveller.ui;

import com.cncsoftwaretools.autoleveller.Controller;
import com.cncsoftwaretools.autoleveller.Units;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ALModelTestIT
{
    private final Path prefsPath = Paths.get(System.getProperty("user.home"), ".AE", "settings.xml");
    private final Path mmPrefsPath = Paths.get(System.getProperty("user.home"), ".AE", "mmOptions.xml");
    private final Path inPrefsPath = Paths.get(System.getProperty("user.home"), ".AE", "inOptions.xml");
    private ALModel model;

    @Before
    public void setUp() throws Exception
    {
        // rename existing settings files for backup
        if (Files.exists(prefsPath)){Files.move(prefsPath, prefsPath.resolveSibling("settingsBak.xml"), REPLACE_EXISTING);}
        if (Files.exists(mmPrefsPath)){Files.move(mmPrefsPath, mmPrefsPath.resolveSibling("mmOptionsBak.xml"), REPLACE_EXISTING);}
        if (Files.exists(inPrefsPath)){Files.move(inPrefsPath, inPrefsPath.resolveSibling("inOptionsBak.xml"), REPLACE_EXISTING);}

        //ensure we have an empty model before tests
        ALModel.clearModel();
        model = ALModel.getInstance();
    }

    @After
    public void tearDown() throws Exception
    {
        //restore backups
        if (Files.exists(prefsPath.resolveSibling("settingsBak.xml"))){
            Files.move(prefsPath.resolveSibling("settingsBak.xml"), prefsPath, REPLACE_EXISTING);
        }
        if (Files.exists(mmPrefsPath.resolveSibling("mmOptionsBak.xml"))){
            Files.move(mmPrefsPath.resolveSibling("mmOptionsBak.xml"), mmPrefsPath, REPLACE_EXISTING);
        }
        if (Files.exists(inPrefsPath.resolveSibling("inOptionsBak.xml"))){
            Files.move(inPrefsPath.resolveSibling("inOptionsBak.xml"), inPrefsPath, REPLACE_EXISTING);
        }
    }

    @Test
    public void newSettingsXMLFiles_shouldHaveDefaultValues() throws IOException
    {
        assertThat(model.readUnits(), equalTo(Units.MM));
        assertThat(model.readDirectory(), equalTo(System.getProperty("user.home")));
        assertThat(model.readController(), equalTo(Controller.LINUXCNC));

        model.setUnits(Units.MM);
        assertThat(model.readDouble(ALModel.MAX_SEG_LTH), equalTo(5.0));
        assertThat(model.readDouble(ALModel.XY_FEED_KEY), equalTo(600.0));
        assertThat(model.readDouble(ALModel.Z_FEED_KEY), equalTo(100.0));
        assertThat(model.readDouble(ALModel.PROBE_DEPTH), equalTo(-1.0));
        assertThat(model.readDouble(ALModel.PROBE_CLEARANCE), equalTo(2.0));
        assertThat(model.readDouble(ALModel.PROBE_SPACING), equalTo(10.0));
        assertThat(model.readDouble(ALModel.SAFE_HEIGHT), equalTo(25.0));
        assertThat(model.readDouble(ALModel.LEFT_INSET), equalTo(0.0));
        assertThat(model.readDouble(ALModel.RIGHT_INSET), equalTo(0.0));
        assertThat(model.readDouble(ALModel.TOP_INSET), equalTo(0.0));
        assertThat(model.readDouble(ALModel.BOTTOM_INSET), equalTo(0.0));

        model.setUnits(Units.INCHES);
        assertThat(model.readDouble(ALModel.MAX_SEG_LTH), equalTo(0.187));
        assertThat(model.readDouble(ALModel.XY_FEED_KEY), equalTo(35.0));
        assertThat(model.readDouble(ALModel.Z_FEED_KEY), equalTo(5.0));
        assertThat(model.readDouble(ALModel.PROBE_DEPTH), equalTo(-0.0625));
        assertThat(model.readDouble(ALModel.PROBE_CLEARANCE), equalTo(0.0787));
        assertThat(model.readDouble(ALModel.PROBE_SPACING), equalTo(0.375));
        assertThat(model.readDouble(ALModel.SAFE_HEIGHT), equalTo(1.5));
        assertThat(model.readDouble(ALModel.LEFT_INSET), equalTo(0.0));
        assertThat(model.readDouble(ALModel.RIGHT_INSET), equalTo(0.0));
        assertThat(model.readDouble(ALModel.TOP_INSET), equalTo(0.0));
        assertThat(model.readDouble(ALModel.BOTTOM_INSET), equalTo(0.0));
    }

    @Test
    public void storingValuesInModelAfterChoosingUnits_shouldBeWrittenToTheCorrectSettingsFile()
    {
        model.setUnits(Units.MM);
        model.writeController(Controller.MACH3); //controller is non unit specific and should be read as the last written controller each time
        model.writeDouble(ALModel.Z_FEED_KEY, 100);
        model.writeDouble(ALModel.PROBE_SPACING, 15);

        model.setUnits(Units.INCHES);
        model.writeController(Controller.LINUXCNC); //controller is non unit specific and should be read as the last written controller each time
        model.writeDouble(ALModel.Z_FEED_KEY, 4);
        model.writeDouble(ALModel.PROBE_SPACING, 0.415);

        model.setUnits(Units.MM);

        assertThat(model.readUnits(), equalTo(Units.MM));
        assertThat(model.readController(), equalTo(Controller.LINUXCNC));
        assertThat(model.readDouble(ALModel.Z_FEED_KEY), equalTo(100.0));
        assertThat(model.readDouble(ALModel.PROBE_SPACING), equalTo(15.0));

        model.setUnits(Units.INCHES);

        assertThat(model.readUnits(), equalTo(Units.INCHES));
        assertThat(model.readController(), equalTo(Controller.LINUXCNC));
        assertThat(model.readDouble(ALModel.Z_FEED_KEY), equalTo(4.0));
        assertThat(model.readDouble(ALModel.PROBE_SPACING), equalTo(0.415));
    }

    @Test
    public void writingInsetsToSpecificUnits_ShouldBeAbleToReadSameInsets()
    {
        model.setUnits(Units.MM);
        model.writeDouble(ALModel.LEFT_INSET, 10);
        model.writeDouble(ALModel.RIGHT_INSET, 10);
        model.writeDouble(ALModel.TOP_INSET, 10);
        model.writeDouble(ALModel.BOTTOM_INSET, 10);

        model.setUnits(Units.INCHES);
        model.writeDouble(ALModel.LEFT_INSET, 0.25);
        model.writeDouble(ALModel.RIGHT_INSET, -1.2);
        model.writeDouble(ALModel.TOP_INSET, 0.187);
        model.writeDouble(ALModel.BOTTOM_INSET, 0.21);

        model.setUnits(Units.MM);

        assertThat(model.readDouble(ALModel.LEFT_INSET), equalTo(10.0));
        assertThat(model.readDouble(ALModel.RIGHT_INSET), equalTo(10.0));
        assertThat(model.readDouble(ALModel.TOP_INSET), equalTo(10.0));
        assertThat(model.readDouble(ALModel.BOTTOM_INSET), equalTo(10.0));

        model.setUnits(Units.INCHES);

        assertThat(model.readDouble(ALModel.LEFT_INSET), equalTo(0.25));
        assertThat(model.readDouble(ALModel.RIGHT_INSET), equalTo(-1.2));
        assertThat(model.readDouble(ALModel.TOP_INSET), equalTo(0.187));
        assertThat(model.readDouble(ALModel.BOTTOM_INSET), equalTo(0.21));
    }
}