package com.cncsoftwaretools.autoleveller;

import javafx.geometry.Point2D;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class MeshSurroundingNegativePointsTest
{
    /*
     (0,2: x-6.567, y4.79546 z#506), (1,2: x-3.2835, y4.79546 z#507), (2,2: x0 y4.79546 z#508)
     (0,1: x-6.567, y2.39773 z#503), (1,1: x-3.2835, y2.39773 z#504), (2,1: x0, y2.39773 z#505)
     (0,0: x-6.567, y0 z#500), (1,0: x-3.2835, y0 z#501), (2,0: x0, y0 z#502)
     */
    private Mesh mesh;

    @Before
    public void setUp() throws Exception
    {
        mesh = new Mesh.Builder(Units.INCHES, Controller.LINUXCNC, new BigDecimal("-6.567"), BigDecimal.ZERO,
                new BigDecimal("6.567"), new BigDecimal("4.79546"))
                .buildSpacing(2.369)
                .build();
    }

    @Test
    public void getNegativePointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("-6.56700"), new BigDecimal("4.79546"), "#506");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("-3.28350"), new BigDecimal("4.79546"), "#507");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("-6.56700"), new BigDecimal("2.39773"), "#503");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("-3.28350"), new BigDecimal("2.39773"), "#504");

        Point2D pointToFind = new Point2D(-5.7654, 4.5);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }

    @Test
    public void getNegativeLowerPointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("-3.28350"), new BigDecimal("2.39773"), "#504");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("0.00000"), new BigDecimal("2.39773"), "#505");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("-3.28350"), new BigDecimal("0.00000"), "#501");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("0.00000"), new BigDecimal("0.00000"), "#502");

        Point2D pointToFind = new Point2D(-1.7654, 1);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }

    @Test
    public void getNegativePointsOutsideMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("-6.56700"), new BigDecimal("4.79546"), "#506");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("-6.56700"), new BigDecimal("4.79546"), "#506");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("-6.56700"), new BigDecimal("2.39773"), "#503");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("-6.56700"), new BigDecimal("2.39773"), "#503");

        Point2D pointToFind = new Point2D(-9.12345, 4.28888);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }
}
