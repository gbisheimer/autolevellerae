package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.ui.ALModel;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ALControllerTest
{
    private ALModel model = ALModel.getInstance();
    private Controller cstCnt = Controller.CUSTOM;

    @Test
    public void fromString()
    {
        assertThat(Controller.fromString("LinuxCNC"), equalTo(Optional.of(Controller.LINUXCNC)));
        assertThat(Controller.fromString("Mach3"), equalTo(Optional.of(Controller.MACH3)));
        assertThat(Controller.fromString("TurboCNC"), equalTo(Optional.of(Controller.TURBOCNC)));
        assertThat(Controller.fromString("AcmeCNC"), equalTo(Optional.empty()));
    }

    @Test
    public void getDefaultExt()
    {
        assertThat(Controller.LINUXCNC.getDefaultExt(), equalTo("ngc"));
        assertThat(Controller.MACH3.getDefaultExt(), equalTo("nc"));
        assertThat(Controller.TURBOCNC.getDefaultExt(), equalTo("txt"));
    }

    @Test
    public void getCustomControllerProbeWord_shouldBeG52AfterChangingInModel()
    {
        model.writeString(ALModel.CST_PROBE_WORD, "");

        assertThat(cstCnt.getProbeWord(), equalTo(""));
        assertThat(Controller.LINUXCNC.getProbeWord(), equalTo("G38.2"));

        model.writeString(ALModel.CST_PROBE_WORD, "G52");

        assertThat(cstCnt.getProbeWord(), equalTo("G52"));
        assertThat(Controller.LINUXCNC.getProbeWord(), equalTo("G38.2"));
    }

    @Test
    public void getAllCustomControllerOptions()
    {
        model.writeString(ALModel.CST_PROBE_WORD, "G11");
        model.writeString(ALModel.CST_PROBE_CMD, "M1000");
        model.writeString(ALModel.CST_ZERO_WORD, "G70");
        model.writeString(ALModel.CST_ZERO_CMD, "M70");
        model.writeString(ALModel.CST_CURRENT_Z, "#4000");
        model.writeString(ALModel.CST_STARTING_PARM, "#550");
        model.writeString(ALModel.CST_OPEN_LOG, "M100");
        model.writeString(ALModel.CST_CLOSE_LOG, "M101");
        model.writeString(ALModel.CST_FILE_EXT, "gcd");

        assertThat(cstCnt.getProbeWord(), equalTo("G11"));
        assertThat(cstCnt.getProbeCommand(), equalTo("M1000"));
        assertThat(cstCnt.getZeroWord(), equalTo("G70"));
        assertThat(cstCnt.getZeroCommand(), equalTo("M70"));
        assertThat(cstCnt.getCurrentZparameter(), equalTo("#4000"));
        assertThat(cstCnt.getStartingParameter(), equalTo("#550"));
        assertThat(cstCnt.getOpenLogCommand(), equalTo("M100"));
        assertThat(cstCnt.getCloseLogCommand(), equalTo("M101"));
        assertThat(cstCnt.getDefaultExt(), equalTo("gcd"));
    }
}