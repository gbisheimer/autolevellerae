AutoLevellerAE (Advanced Edition)
=========

The Java based controller for 'levelling' GCode file for use on a CNC machine

Probes the surface to be etched then uses this information to adjust the Z height using bilinear interpolation

  - Can be used on multiple operating systems
  - Helps reduce air cuts when etching
  - Can equally be used for etching other projects such as a metallic project box
  - Compatible with Mach3, LinuxCNC and TurboCNC


![AutoLevellerAE Basic Tab](http://www.autoleveller.co.uk/wp-content/uploads/2016/03/ALAEbasicv062scaled-e1457629910199.jpg "AutolevellerAE is operation [Basic Tab v.0.6.2]") | ![AutoLevellerAE Mesh Tab](http://www.autoleveller.co.uk/wp-content/uploads/2016/03/ALAEmeshv062scaled-e1457629940278.jpg "AutolevellerAE is operation [Mesh Tab v.0.6.2]")
:----------------:|:-----------------:
AutolevellerAE is operation (Basic Tab v.0.6.2) | AutolevellerAE is operation (Mesh Tab v.0.6.2)

![w/o AutoLeveller](http://www.autoleveller.co.uk/wp-content/uploads/2016/03/badcircuitscaled-e1457627198253.jpg "Circuit milled without AutoLeveller") | ![with AutoLeveller](http://www.autoleveller.co.uk/wp-content/uploads/2016/03/goodcircuitscaled-e1457627361623.jpg "Circuit milled with AutoLeveller")
:----------------:|:-----------------:
Circuit milled without AutoLeveller | Circuit milled with AutoLeveller

AutolevellerAE accepts a GCode file as input and outputs a modified GCode file. Please visit the [AutoLeveller website](http://www.autoleveller.co.uk) for more.

Downloading
--------------

The source code can be retrieved by typing at a git command prompt...

    git clone https://daedelus1982@bitbucket.org/daedelus1982/autolevellerae.git

Using
--------------

Make sure Java8 SDK is intalled
AutolevellerAE is a Maven project so it can be easily imported into most IDE's
The quickest and easiest way to compile and run all tests including integration tests is by typing at a Maven installed prompt...

    mvn clean install verify

To run only unit test type...

    mvn clean test

Brief overview of important classes and methods
-

Many of the classes in this project are important and you should investigate them, and any test code which uses them whether you want to create a new project based on this one or want to make changes to the existing code. Having said that, here are a few of the more "prominent" ones...

**GCodeState**

This class is constructed from a GCode String and the previous GCodeState, where the first State is passed an Optional.empty() GCodeState. Each String is parsed into usable objects such as Words which make up the String. The modal state of the code is stored and obtained via a call to getModalWords() or getModalWordByGroup(WordGroup wordGroup) if the WordGroup is known. You can chain together multiple lines of GCode by passing the previous GCodeState to the new one. For example...

    @Test
    public void stateEquals()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N10 G00 G1 G2 G3");
        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "#3 = 10");
        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "F3000");
        GCodeState state4 = GCodeState.newInstance(Optional.of(state3), "N50 S20000");

        assertThat(state4.getModalWords(), containsInAnyOrder(Word.createPair('G', BigDecimal.valueOf(3)),
                Word.createPair('F', BigDecimal.valueOf(3000)),
                Word.createPair('S', BigDecimal.valueOf(20000))));

        assertThat(state4.getPreviousState().get(), equalTo(state3));
    }

GCodeState's store 1 and only 1 previous state which prevents a single state from becoming too large as more and more states are chained. For most operations we only ever need the most recent previous state whilst the modal state is known for an unlimited number of chained states. This makes GCodeState's extremely robust.

**GCodeProcessor**

Each line of Gcode may need different processing and need to be rewritten after levelling for example. This is what a GCodeProcessor does. If you need to process the GCode differently, you only need to implement this interface to process states how you want.

    public interface GCodeProcessor
    {
        void interpret(List<GCodeState> states);

        List<GCodeState> processState(GCodeState gCodeState);

        default Optional<GCodeProcessor> nextProcessor(){return Optional.empty();}

        default void finish(){}
    }

The GCodeInterpeter is an abstract class which implements GCodeProcessor and makes final the interpret() method. New Processor classes should extend GCodeInterpeter rather than implement GCodeProcessor directly. What interpret() does is processes all given states then recursively calls interpret() on any nested GCodeProcessor passing the newly processed states. In this way GCodeProcessor can be chained.

    public abstract class GCodeInterpreter implements GCodeProcessor
    {
        @Override
        public final void interpret(List<GCodeState> states)
        {
            List<GCodeState> returnedStates = states
                .stream()
                .flatMap(gCodeState -> processState(gCodeState).stream())
                .collect(toList());

            nextProcessor().ifPresent(gCodeProcessor -> gCodeProcessor.interpret(returnedStates));
        }
    }

This chaining of processors is shown here...

    public static void writeFullLevelledFile(ALModel model) throws IOException
    {
        if (model.isReadyForWriting())
        {
            writeProbeInit(model);
            writeProbeRoutine(model);

            GCodeReader reader = PlainGCodeReader.newInstance(new FileReader(model.getOGF().get().getFilename()));
            GCodeProcessor writer = WriterProcessor.newInstance(Optional.<GCodeProcessor>empty(), model.getOutputFilename().get(), true);
            GCodeProcessor leveller = LevelerProcessor.newInstance(Optional.of(writer), model.getMesh().get());
            GCodeProcessor segmentor = SegmentorProcessor.newInstance(Optional.of(leveller), model.getSegLen().doubleValue());
            GCodeProcessor replacer = ReplacerProcessor.newInstance(Optional.of(segmentor));
            reader.readFile(replacer);
        }
    }

**Mesh**

A Mesh is a grid of ThreeDPoints representing the area to be probed where each point is a probe point. The size of the grid and value of is points are determined by the members which are set during construction of an instance via a builder. These few required inputs on construction result in a mesh object with probing points pre-calculated and is one of the most useful classes in the project. The test code below shows how to construct a Mesh instance and obtain the value of any pre-calculated point...

    @Test
    public void builderInputs()
    {
        Mesh mesh = new Mesh.Builder(Units.INCHES, Software.LINUXCNC, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(5), BigDecimal.valueOf(3))
                .buildDepth(-0.05)
                .buildSpacing(0.5)
                .buildZFeed(7)
                .buildXYFeed(30)
                .buildClearance(0.8)
                .buildSafeHeight(3)
                .build();

        assertThat(mesh.getUnits(), equalTo(Units.INCHES));
        assertThat(mesh.getxValue(), equalTo(BigDecimal.ZERO.setScale(Mesh.SCALE)));
        assertThat(mesh.getyValue(), equalTo(BigDecimal.ZERO.setScale(Mesh.SCALE)));
        assertThat(mesh.getxLength(), equalTo(BigDecimal.valueOf(5).setScale(Mesh.SCALE)));
        assertThat(mesh.getyLength(), equalTo(BigDecimal.valueOf(3).setScale(Mesh.SCALE)));
        assertThat(mesh.getProbeDepth(), equalTo(BigDecimal.valueOf(-0.05).setScale(Mesh.SCALE)));
        assertThat(mesh.getPointSpacing(), equalTo(BigDecimal.valueOf(0.5).setScale(Mesh.SCALE)));
        assertThat(mesh.getzFeed(), equalTo(BigDecimal.valueOf(7.0).setScale(Mesh.SCALE)));
        assertThat(mesh.getXyFeed(), equalTo(BigDecimal.valueOf(30.0).setScale(Mesh.SCALE)));
        assertThat(mesh.getProbeClearance(), equalTo(BigDecimal.valueOf(0.8).setScale(Mesh.SCALE)));
        assertThat(mesh.getSafeHeight(), equalTo(BigDecimal.valueOf(3.0).setScale(Mesh.SCALE)));

        assertThat(mesh.getPoint(0, 0).get(), equalTo(ThreeDPoint.createPoint(BigDecimal.valueOf(
                0.00000).setScale(Mesh.SCALE), BigDecimal.valueOf(0.00000).setScale(Mesh.SCALE), "#500")));
        assertThat(mesh.getPoint(6, 10).get(), equalTo(ThreeDPoint.createPoint(BigDecimal.valueOf(
                5.00000).setScale(Mesh.SCALE), BigDecimal.valueOf(3.00000).setScale(Mesh.SCALE), "#576")));
    }

All the above code is taken from the source project code but can and will change over time. Please see the current code for the latest versions

Credits
-
<div>Segment Length Dialog icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

License
-

[GPLv2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)